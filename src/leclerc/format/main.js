"use strict";
//console.log('test');
try {
    var sto = window.__sto,
        $ = window.jQuery,
        settings = require("./../../settings.json"),
        placeholder = settings.name,
        format = settings.format,
        style = require("./main.css"),
        html = require("./main.html"),
        gutters = require("./gutter.html"),
        gutters_style = require("./gutter.css"),
        product_box = require("./prodbox.html"),
        prods_hightech = require('./prods-hightech.html'),
        prods_maison = require('./prods-maison.html'),
        prods_parapharmacie = require('./prods-parapharmacie.html'),
        prods_culture = require('./prods-culture.html'),
        prods_mode = require('./prods-mode.html'),
        prods_vins = require('./prods-hightech.html'),
        prods_alimentaire = require('./prods-alimentaire.html'),
        prods_bijoux = require('./prods-bijoux.html'),
        container = $(html),
        paginate = require('./pagination.min.js'),
        pagination_container,
        deferred = sto.utils.deferred,
        sorting = null,
        subcategory = null,
        windowSize = window.innerWidth,
        paginationLimit = 20,
        paginationBijoux = 19,
        hpProductLimit = 4,
        pageNumber = null,
        crawlPortail;
    var cosa = [];

    /*var verga = ["194441170290", "3016661150500", "3401360147508", "805278385072500", "0190781396475", "3760165464969", "376007380363802", "8013298204922", "325869133156001", "3401528520846", "842776102379", "3221614003158", "8015244200071", "8806090173752", "4008789050137", "3282779185912", "190781396475", "3760165464969", "5051889488682", "328277932706", "5030945122531", "3700538222734", "6941059620648", "3760226392033", "3700538218874", "4242002853321", "5053083123444", "841011300281500", "366143400296", "190781396475", "3760256554227", "4242002853321", "3700691412065"];
    var counterverga = 0;
    var vergafinal = {};*/

    var fontRoboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
    $('head').first().append(fontRoboto);

    var trim = function(string) {
            return string.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
        },
        cleanStringBlackFriday = function(string) {
            try {
                var rule_slash = [/[^a-z0-9_]/g, "-"],
                    replacements = [
                        [/[éèëê]/g, "e"],
                        [/[àáâãäå]/g, "a"],
                        [/[ìíîï]/g, "i"],
                        [/[òóôõö]/g, "o"],
                        [/[ùúûü]/g, "u"],
                        [/ñ/g, "n"],
                        [/ç/g, "c"],
                        [/æ/g, "ae"],
                        [/œ/g, "oe"],
                        [/\n\r\t/g, "-"],
                        [/_/g, "-"],
                        [/\s/g, "_"],
                        rule_slash //replace all non ascii characters by an underscore
                    ],
                    i = -1,
                    l = replacements.length;
                string = string.toLowerCase();
                string = trim(string);

                while (++i < l) {
                    string = string.replace(replacements[i][0], replacements[i][1])
                }
                return string;
            } catch (e) {
                console.log("cleanStringBlackFriday error : " + e)
            }
        };

    var getParams = function(url) {
        var params = {};
        var parser = document.createElement('a');
        parser.href = url;
        var query = parser.search.substring(1);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            params[pair[0]] = decodeURIComponent(pair[1]);
        }
        return params;
    };

    module.exports = {
        init: _init_(),
        styleGutter: style
    }

    function _init_() {
        sto.load(format, function(tracker) {
            var container = $(html),
                helper_methods = sto.utils.retailerMethod,
                url_crawling = window.location.host.indexOf("rec.e-leclerc.com") > -1 ? settings.crawlTest : settings.crawl,
                //url_crawling = settings.crawl,
                prodsHightech = $(prods_hightech),
                prodsMaison = $(prods_maison),
                prodsParapharmacie = $(prods_parapharmacie),
                prodsCulture = $(prods_culture),
                prodsMode = $(prods_mode),
                prodsVins = $(prods_vins),
                prodsAlimentaire = $(prods_alimentaire),
                prodsBijoux = $(prods_bijoux);

            $('.page-container .main-content').append(gutters);
            $('.row[layout\\:fragment="pagecontent"]').empty();
            $('.row[layout\\:fragment="pagecontent"]').append(container);

            style.use();
            gutters_style.use();
            resize_gutters();

            helper_methods.crawl(url_crawling).promise.then(function(d) {
                crawlPortail = d;

                var objHightech = {};
                var objMaison = {};
                var objParapharmacie = {};
                var objCulture = {};
                var objMode = {};
                var objVins = {};

                $.each(d, function(i, v) {
                    var productBox = v.bloc,
                        productData = (productBox.find('#btn-ajout-liste'), v.data),
                        productUrl = productData.urlMarchand,
                        productId = productData.techid,
                        productName = productData.libelle,
                        productPrice = productData.prixFinal,
                        productCategory = productData.codeUnivers,
                        productSubcategory = productData.codeSousUnivers;

                    if (productPrice && "" !== productPrice) {
                        productPrice = productPrice.toString();
                        var g = productPrice.slice(-2),
                            y = productPrice.slice(0, -2);
                        productPrice = y + "-" + g
                    }

                    v.bloc.attr('data-trackurl', productUrl);
                    v.bloc.attr('data-trackid', productId);
                    v.bloc.attr('data-trackname', cleanStringBlackFriday(productName));
                    v.bloc.attr('data-trackprice', productPrice);
                    v.bloc.attr('data-trackcategory', cleanStringBlackFriday(productCategory));
                    v.bloc.attr('data-tracksubcategory', cleanStringBlackFriday(productSubcategory));

                    if (v.univers === "Maison, jardin & Electroménager" || v.univers === "Maison, Jardin & électroménager" || v.data.codeUnivers === "maison" || v.data.codeUnivers === "brico") {
                        objMaison[i] = v;
                    } else if (v.univers === "Culture & Jouets" || v.data.codeUnivers === "culture") {
                        objCulture[i] = v;
                    } else if (v.univers === "High Tech" || v.data.codeUnivers === "multimedia") {
                        objHightech[i] = v;
                    } else if (v.univers === "Parapharmacie") {
                        objParapharmacie[i] = v;
                    } else if (v.univers === "Mode & Sport" || v.univers === "Mode & sport") {
                        objMode[i] = v;
                    } else if (v.univers === "Vins & alcools") {
                        objVins[i] = v;
                    }
                });

                try {
                    helper_methods.createFormat(prodsHightech, tracker.copy().update({
                        "trackZone": "HighTech"
                    }), Object.keys(objHightech), objHightech, 1000);
                } catch (e) {}
                try {
                    helper_methods.createFormat(prodsMaison, tracker.copy().update({
                            "trackZone": "Maison"
                        }),
                        Object.keys(objMaison), objMaison, 1000);
                } catch (e) {}
                try {
                    helper_methods.createFormat(prodsCulture, tracker.copy().update({
                        "trackZone": "Culture"
                    }), Object.keys(objCulture), objCulture, 1000);
                } catch (e) {}
                try {
                    helper_methods.createFormat(prodsParapharmacie, tracker.copy().update({
                        "trackZone": "Parapharmacie"
                    }), Object.keys(objParapharmacie), objParapharmacie, 1000);
                } catch (e) {}

                try {
                    helper_methods.createFormat(prodsVins, tracker.copy().update({
                        "trackZone": "Vins"
                    }), Object.keys(objVins), objVins, 1000);
                } catch (e) {}
                try {
                    helper_methods.createFormat(prodsMode, tracker.copy().update({
                        "trackZone": "Mode"
                    }), Object.keys(objMode), objMode, 1000);
                } catch (e) {}

                var promises_crawl = [];
                var crawls_data = [{
                        "tz": "HighTech",
                        "shop": prodsHightech,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[High-Tech]=BLACK_FRIDAY&format=json"
                    },
                    {
                        "tz": "HighTech",
                        "shop": prodsHightech,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[High-Tech]=BLACK_FRIDAY&format=json"
                    },
                    {
                        "tz": "HighTech",
                        "shop": prodsHightech,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[High-Tech]=BLACK_FRIDAY&format=json"
                    },
                    {
                        "tz": "HighTech",
                        "shop": prodsHightech,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[High-Tech]=BLACK_FRIDAY&format=json"
                    },
                    {
                        "tz": "HighTech",
                        "shop": prodsHightech,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[High-Tech]=BLACK_FRIDAY&format=json"
                    },
                    {
                        "tz": "HighTech",
                        "shop": prodsHightech,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[High-Tech]=BLACK_FRIDAY&format=json"
                    },
                    {
                        "tz": "Maison",
                        "shop": prodsMaison,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[test]=blackfridaymaisonetloisirs&format=json"
                    },
                    {
                        "tz": "Maison",
                        "shop": prodsMaison,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[test]=blackfridaymaisonetloisirs&format=json"
                    },
                    {
                        "tz": "Maison",
                        "shop": prodsMaison,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[test]=blackfridaymaisonetloisirs&format=json"
                    },
                    {
                        "tz": "Maison",
                        "shop": prodsMaison,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[test]=blackfridaymaisonetloisirs&format=json"
                    },
                    {
                        "tz": "Maison",
                        "shop": prodsMaison,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[test]=blackfridaymaisonetloisirs&format=json"
                    },
                    {
                        "tz": "Maison",
                        "shop": prodsMaison,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[test]=blackfridaymaisonetloisirs&format=json"
                    },
                    {
                        "tz": "Maison",
                        "shop": prodsMaison,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[High-Tech]=BLACK_FRIDAY_PEM&format=json"
                    },
                    {
                        "tz": "Maison",
                        "shop": prodsMaison,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[High-Tech]=BLACK_FRIDAY_PEM&format=json"
                    },
                    {
                        "tz": "Maison",
                        "shop": prodsMaison,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[High-Tech]=BLACK_FRIDAY_PEM&format=json"
                    },
                    {
                        "tz": "Maison",
                        "shop": prodsMaison,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[High-Tech]=BLACK_FRIDAY_PEM&format=json"
                    },
                    {
                        "tz": "Maison",
                        "shop": prodsMaison,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[High-Tech]=BLACK_FRIDAY_PEM&format=json"
                    },
                    {
                        "tz": "Maison",
                        "shop": prodsMaison,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[High-Tech]=BLACK_FRIDAY_PEM&format=json"
                    },
                    {
                        "tz": "Culture",
                        "shop": prodsCulture,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[Tous]=promotion_black_friday&format=json"
                    },
                    {
                        "tz": "Culture",
                        "shop": prodsCulture,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[Tous]=promotion_black_friday&format=json"
                    },
                    {
                        "tz": "Culture",
                        "shop": prodsCulture,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[Tous]=promotion_black_friday&format=json"
                    },
                    {
                        "tz": "Culture",
                        "shop": prodsCulture,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[Tous]=promotion_black_friday&format=json"
                    },
                    {
                        "tz": "Culture",
                        "shop": prodsCulture,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[Tous]=promotion_black_friday&format=json"
                    },
                    {
                        "tz": "Culture",
                        "shop": prodsCulture,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[Tous]=promotion_black_friday&format=json"
                    },
                    {
                        "tz": "Culture",
                        "shop": prodsCulture,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=600&contexts[Tous]=promotion_black_friday&format=json"
                    },
                    {
                        "tz": "Culture",
                        "shop": prodsCulture,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=700&contexts[Tous]=promotion_black_friday&format=json"
                    },
                    {
                        "tz": "Parapharmacie",
                        "shop": prodsParapharmacie,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[test]=boutique_para_blackfriday&format=json"
                    },
                    {
                        "tz": "Parapharmacie",
                        "shop": prodsParapharmacie,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[test]=boutique_para_blackfriday&format=json"
                    },
                    {
                        "tz": "Parapharmacie",
                        "shop": prodsParapharmacie,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[test]=boutique_para_blackfriday&format=json"
                    },
                    {
                        "tz": "Parapharmacie",
                        "shop": prodsParapharmacie,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[test]=boutique_para_blackfriday&format=json"
                    },
                    {
                        "tz": "Parapharmacie",
                        "shop": prodsParapharmacie,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[test]=boutique_para_blackfriday&format=json"
                    },
                    {
                        "tz": "Parapharmacie",
                        "shop": prodsParapharmacie,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[test]=boutique_para_blackfriday&format=json"
                    },
                    {
                        "tz": "Mode",
                        "shop": prodsMode,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[test]=boutique_sport_blackfriday&format=json"
                    },
                    {
                        "tz": "Mode",
                        "shop": prodsMode,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[test]=boutique_sport_blackfriday&format=json"
                    },
                    {
                        "tz": "Mode",
                        "shop": prodsMode,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[test]=boutique_sport_blackfriday&format=json"
                    },
                    {
                        "tz": "Mode",
                        "shop": prodsMode,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[test]=boutique_sport_blackfriday&format=json"
                    },
                    {
                        "tz": "Mode",
                        "shop": prodsMode,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[test]=boutique_sport_blackfriday&format=json"
                    },
                    {
                        "tz": "Mode",
                        "shop": prodsMode,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[test]=boutique_sport_blackfriday&format=json"
                    },
                    {
                        "tz": "Vins",
                        "shop": prodsVins,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[test]=boutique_macave_blackfriday&format=json"
                    },
                    {
                        "tz": "Vins",
                        "shop": prodsVins,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[test]=boutique_macave_blackfriday&format=json"
                    },
                    {
                        "tz": "Vins",
                        "shop": prodsVins,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[test]=boutique_macave_blackfriday&format=json"
                    },
                    {
                        "tz": "Vins",
                        "shop": prodsVins,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[test]=boutique_macave_blackfriday&format=json"
                    },
                    {
                        "tz": "Vins",
                        "shop": prodsVins,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[test]=boutique_macave_blackfriday&format=json"
                    },
                    {
                        "tz": "Vins",
                        "shop": prodsVins,
                        "qty_prod": "1000",
                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[test]=boutique_macave_blackfriday&format=json"
                    }
                ];

                crawls_data.forEach(function(crawl_data) {
                    var def = deferred();

                    helper_methods.crawlAPI(crawl_data.crawl).promise.then(function(d) {

                        /*console.log(crawl_data.tz, d);
                        $.each(d, function(i, v) {
                            //if (crawl_data.tz == "Parapharmacie") {
                                cosa.push(i);
                            //}
                        });
                        console.log(cosa+"");*/

                        //Remove repated supprimer doublons
                        /*$.each(d, function(i, v) {
                            if (crawlPortail[i]) {
                                delete d[i];
                            }
                        });*/

                        $.each(crawlPortail, function(i, v) {
                            //console.log('i',i);
                            //console.log('crawlPortail[i]["data"]["ean"]', crawlPortail[i]["data"]["ean"]);
                            //console.log(d.hasOwnProperty(crawlPortail[i]["data"]["ean"]));
                            if (d.hasOwnProperty(crawlPortail[i]["data"]["ean"])) {
                                delete d[crawlPortail[i]["data"]["ean"]];
                            }
                        });

                        if (crawl_data.tz == "HighTech") {
                            $.each(d, function(i, v) {
                                var c = JSON.parse(v.category_1);
                                var catNb = 0;
                                $.each(settings.hightech_real_subcat, function(i, value) {
                                    c = c.map(sto.utils.fn.cleanString);
                                    if (c.indexOf(value) !== -1) {
                                        catNb++;
                                    }
                                });
                                if (catNb === 0) {
                                    delete d[i];
                                }
                            });
                        }
                        def.resolve({
                            "result": d,
                            "data": crawl_data
                        });
                    });
                    promises_crawl.push(def.promise);
                });

                deferred.all(promises_crawl).then(function(crawls_promise) {

                    crawls_promise.forEach(function(crawl_promise) {
                        try {
                            helper_methods.createFormatAPI(crawl_promise.data.shop, tracker.copy().update({
                                "trackZone": crawl_promise.data.tz
                            }), Object.keys(crawl_promise.result), crawl_promise.result, crawl_promise.data.qty_prod);
                        } catch (e) {}
                    });

                    // exception for Mode & Sport products, append portal product after API products
                    try {
                        helper_methods.createFormat(prodsMode, tracker.copy().update({
                            "trackZone": "Mode"
                        }), Object.keys(objMode), objMode, 1000);
                    } catch (e) {}
                    try {
                        helper_methods.createFormat(prodsMaison, tracker.copy().update({
                            "trackZone": "Maison"
                        }), Object.keys(objMaison), objMaison, 1000);
                    } catch (e) {}

                    $('.sto-' + placeholder + '-loader').hide();

                    $('.page-container .main-content').append(gutters);
                    $('.row[layout\\:fragment="pagecontent"]').empty();
                    $('.row[layout\\:fragment="pagecontent"]').append(container);

                    style.use();
                    gutters_style.use();

                    var today = new Date(),
                        date1 = new Date(2019, 11, 2),
                        date2 = new Date(2019, 11, 3);

                    //TEST
                    /*var today = new Date(),
                        date1 = new Date(2019, 10, 20, 14),
                        date2 = new Date(2019, 10, 20, 18),
                        date3 = new Date(2019, 10, 21, 6);*/


                    $('.sto-' + placeholder + '-global').attr('date', today.getFullYear() + '-' + (today.getMonth() + 1) + "-" + (today.getDate()));

                    if (today < date1) {
                        $('.sto-' + placeholder + '-nuit-blanche').attr("bg", "0");
                    } else if (today >= date1) {
                        $('.sto-' + placeholder + '-nuit-blanche').attr("bg", "1");
                    } else if (today >= date2) {
                        $('.sto-' + placeholder + '-nuit-blanche').attr("bg", "0");
                    }

                    if (today >= date1 && today < date2) {
                        $('#link-nuitblanche').attr('target', '_blank');
                        $('#link-nuitblanche').attr('href', "https://www.e-leclerc.com/catalogue/evenements/black-friday/nuit-blanche");
                        $('#link-nuitblanche').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': 'cyber_monday', 'urlsortie': 'http---www-e-leclerc-com-catalogue-evenements-nuit-blanche', 'event': 'blackfriday_traffickingexterne_visuels'});")
                        $('#link-nuitblanche').removeAttr('style');
                    }

                    $('#sto-prod-storage-hightech').append(prodsHightech);
                    $('#sto-prod-storage-maison').append(prodsMaison);
                    $('#sto-prod-storage-parapharmacie').append(prodsParapharmacie);
                    $('#sto-prod-storage-culture').append(prodsCulture);
                    $('#sto-prod-storage-mode').append(prodsMode);
                    $('#sto-prod-storage-vin').append(prodsVins);

                    $('.sto-product-container>.box-item.box-item--achat-ligne').each(function(i, v) {
                        var elem = $(v);
                        tracking_products_portal(elem);
                    });

                    $('.sto-product-container>.ng-scope').each(function(i, v) {
                        var elem = $(v);
                        tracking_products_api(elem);
                    });

                    // btn memos + buy
                    $('.box-item.box-item-rayon').each(function() {
                        var elem = $(this);

                        if (elem.find('.btn.btn-memo').length) {
                            if (elem.find('.btn.btn-achat-ligne').length === 0)
                                elem.addClass('sto-en-magasin');
                        } else {
                            elem.addClass('sto-exclu-web');
                        }
                    });

                    $.each(settings.btn_products, function(i, v) {
                        var item = $('div[data-id="' + i + '"]');

                        if (item.find('.box-item').length) {
                            item.find('.box-item').removeClass('sto-exclu-web');
                        } else {
                            item.removeClass('sto-exclu-web');
                        }
                    });

                    $('#sto-prod-storage-parapharmacie').find('.sto-exclu-web').removeClass('sto-exclu-web');

                    $('.box-item').find('a:not(.btn)').attr('target', "_blank");

                    checkWindowSize();
                    goUpBtn();

                    // get first 4 products for each universe
                    $('#sto-prod-storage-hightech').find('.box-item.box-item-rayon').each(function(i, v) {
                        if ($('#sto-section-hightech .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
                            if (i == 0) {
                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
                            } else {
                                $(v).attr('data-ind', (i + 1));
                            }
                            $('#sto-section-hightech .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
                        } else {
                            return false;
                        }
                    });
                    $('#sto-prod-storage-maison').find('.box-item.box-item-rayon').each(function(i, v) {
                        if ($('#sto-section-maison .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
                            if (i == 0) {
                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
                            } else {
                                $(v).attr('data-ind', (i + 1));
                            }
                            $('#sto-section-maison .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
                        } else {
                            return false;
                        }
                    });
                    $('#sto-prod-storage-culture').find('.box-item.box-item-rayon').each(function(i, v) {
                        if ($('#sto-section-culture .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
                            if (i == 0) {
                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
                            } else {
                                $(v).attr('data-ind', (i + 1));
                            }
                            $('#sto-section-culture .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
                        } else {
                            return false;
                        }
                    });
                    $('#sto-prod-storage-parapharmacie').find('.box-item.box-item-rayon').each(function(i, v) {
                        if ($('#sto-section-parapharmacie .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
                            if (i == 0) {
                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
                            } else {
                                $(v).attr('data-ind', (i + 1));
                            }
                            $('#sto-section-parapharmacie .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
                        } else {
                            return false;
                        }
                    });
                    $('#sto-prod-storage-mode').find('.box-item.box-item-rayon').each(function(i, v) {
                        if ($('#sto-section-mode .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
                            if (i == 0) {
                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
                            } else {
                                $(v).attr('data-ind', (i + 1));
                            }
                            $('#sto-section-mode .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
                        } else {
                            return false;
                        }
                    });
                    $('#sto-prod-storage-vin').find('.box-item.box-item-rayon').each(function(i, v) {
                        if ($('#sto-section-vin .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
                            if (i == 0) {
                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
                            } else {
                                $(v).attr('data-ind', (i + 1));
                            }
                            $('#sto-section-vin .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
                        } else {
                            return false;
                        }
                    });
                    $('#sto-prod-storage-alimentaire').find('.box-item.box-item-rayon').each(function(i, v) {
                        if ($('#sto-section-alimentaire .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
                            if (i == 0) {
                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
                            } else {
                                $(v).attr('data-ind', (i + 1));
                            }
                            $('#sto-section-alimentaire .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
                        } else {
                            return false;
                        }
                    });
                    $('#sto-prod-storage-bijoux').find('.box-item.box-item-rayon').each(function(i, v) {
                        if ($('#sto-section-bijoux .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
                            if (i == 0) {
                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
                            } else {
                                $(v).attr('data-ind', (i + 1));
                            }
                            $('#sto-section-bijoux .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
                        } else {
                            return false;
                        }
                    });

                    // fill header slider with products
                    var slideDate1 = new Date(2019, 10, 25),
                        slideDate2 = new Date(2019, 10, 26),
                        slideDate3 = new Date(2019, 10, 27),
                        slideDate4 = new Date(2019, 10, 28),
                        slideDate5 = new Date(2019, 10, 29),
                        slideDate6 = new Date(2019, 10, 30),
                        slideDate7 = new Date(2019, 11, 1),
                        slideDate8 = new Date(2019, 11, 2);


                    if (today >= slideDate8) { //2-12
                        //console.log('slider date 8');
                        settings.banners.bijoux = settings.banners.bijoux3;
                        var header1 = $('.sto-product-container').find('div[data-id="193638727392"]').first().clone(true).attr('slider-nav', "PC PORTABLE LENOVO");
                        if (header1.find('.box-item').length) {
                            header1 = $('.sto-product-container').find('div[data-id="193638727392"] .box-item').first().clone(true).attr('slider-nav', "PC PORTABLE LENOVO");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header1);

                        var banner2 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='Consoles Xbox One'>Consoles Xbox One</div>");
                        banner2.attr('data-index', '2');
                        banner2.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('Consoles Xbox One') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.culture.leclerc/%20jeux-video-u/xbox-one-u/consoles-u/selection-consoles-xbox-one-black-Friday-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-12-02-black-friday&utm_content=slider-hp-consoles-xbox-one") + '", "event":"https://www.culture.leclerc/%20jeux-video-u/xbox-one-u/consoles-u/selection-consoles-xbox-one-black-Friday-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-12-02-black-friday&utm_content=slider-hp-consoles-xbox-one")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner2);

                        var header3 = $('.sto-product-container').find('div[data-id="8718692617841"]').first().clone(true).attr('slider-nav', "MACHINE A GAZEIFIER SODASTREAM");
                        if (header3.find('.box-item').length) {
                            header3 = $('.sto-product-container').find('div[data-id="8718692617841"] .box-item').first().clone(true).attr('slider-nav', "MACHINE A GAZEIFIER SODASTREAM");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);

                        var header4 = $('.sto-product-container').find('div[data-id="1168500600"]').first().clone(true).attr('slider-nav', "Trampoline Kangui");
                        if (header4.find('.box-item').length) {
                            header4 = $('.sto-product-container').find('div[data-id="1168500600"] .box-item').first().clone(true).attr('slider-nav', "Trampoline Kangui");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);

                        var header5 = $('.sto-product-container').find('div[data-id="0734646679862"]').first().clone(true).attr('slider-nav', "PC PORTABLE");
                        if (header5.find('.box-item').length) {
                            header5 = $('.sto-product-container').find('div[data-id="0734646679862"] .box-item').first().clone(true).attr('slider-nav', "PC PORTABLE");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);

                        var header6 = $('.sto-product-container').find('div[data-id="6970244526823"]').first().clone(true).attr('slider-nav', "Trottinette électrique");
                        if (header6.find('.box-item').length) {
                            header6 = $('.sto-product-container').find('div[data-id="6970244526823"] .box-item').first().clone(true).attr('slider-nav', "Trottinette électrique");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);

                        fetch_backup_products();

                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
                            var item = $(v);
                            if (i < 4) {
                                if (item.hasClass('box-item-banner')) {
                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
                                }
                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
                            } else {
                                return false;
                            }
                        });
                    } else if (today >= slideDate7) { //1-12
                        //console.log('slider date 7');
                        settings.banners.bijoux = settings.banners.bijoux3;

                        var banner1 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-40% sur tout le site'>-40% sur tout le site</div>");
                        banner1.attr('data-index', '6');
                        banner1.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-40% sur tout le site') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner1);

                        var banner2 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='Vinyles à prix choc !'>Vinyles à prix choc !</div>");
                        banner2.attr('data-index', '1');
                        banner2.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('Vinyles à prix choc !') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.culture.leclerc/musique-u/black-friday-musique-selection-de-vinyles-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-12-01-black-friday&utm_content=slider-hp-vinyles") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.culture.leclerc/musique-u/black-friday-musique-selection-de-vinyles-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-12-01-black-friday&utm_content=slider-hp-vinyles")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner2);

                        var header3 = $('.sto-product-container').find('div[data-id="359589544483300"]').first().clone(true).attr('slider-nav', "Xls mon action minceur");
                        if (header3.find('.box-item').length) {
                            header3 = $('.sto-product-container').find('div[data-id="359589544483300"] .box-item').first().clone(true).attr('slider-nav', "Xls mon action minceur");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);

                        var header4 = $('.sto-product-container').find('div[data-id="6970244526823"]').first().clone(true).attr('slider-nav', "Trottinette électrique");
                        if (header4.find('.box-item').length) {
                            header4 = $('.sto-product-container').find('div[data-id="6970244526823"] .box-item').first().clone(true).attr('slider-nav', "Trottinette électrique");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);

                        var header5 = $('.sto-product-container').find('div[data-id="3760256554227"]').first().clone(true).attr('slider-nav', "Trotinette électrique");
                        if (header5.find('.box-item').length) {
                            header5 = $('.sto-product-container').find('div[data-id="3760256554227"] .box-item').first().clone(true).attr('slider-nav', "Trotinette électrique");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);
                        
                        var header6 = $('.sto-product-container').find('div[data-id="4242002853321"]').first().clone(true).attr('slider-nav', "Robot multifonction");
                        if (header6.find('.box-item').length) {
                            header6 = $('.sto-product-container').find('div[data-id="4242002853321"] .box-item').first().clone(true).attr('slider-nav', "Robot multifonction");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);

                        fetch_backup_products();
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
                            var item = $(v);
                            if (i < 4) {
                                if (item.hasClass('box-item-banner')) {
                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
                                }
                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
                            } else {
                                return false;
                            }
                        });
                    } else if (today >= slideDate6) { //30-11
                        //console.log('slider date 6');
                        settings.banners.bijoux = settings.banners.bijoux2;
                        var header1 = $('.sto-product-container').find('div[data-id="1168500240"]').first().clone(true).attr('slider-nav', "SMARTPHONE XIAOMI 32Go");
                        if (header1.find('.box-item').length) {
                            header1 = $('.sto-product-container').find('div[data-id="1168500240"] .box-item').first().clone(true).attr('slider-nav', "SMARTPHONE XIAOMI 32Go");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header1);

                        var header2 = $('.sto-product-container').find('div[data-id="3760226392033"]').first().clone(true).attr('slider-nav', "Tick & Box - 100% foot");
                        if (header2.find('.box-item').length) {
                            header2 = $('.sto-product-container').find('div[data-id="3760226392033"] .box-item').first().clone(true).attr('slider-nav', "Tick & Box - 100% foot");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header2);

                        var header3 = $('.sto-product-container').find('div[data-id="1168500550"]').first().clone(true).attr('slider-nav', "Nettoyeur électrique");
                        if (header3.find('.box-item').length) {
                            header3 = $('.sto-product-container').find('div[data-id="1168500550"] .box-item').first().clone(true).attr('slider-nav', "Nettoyeur électrique");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);

                        var header4 = $('.sto-product-container').find('div[data-id="4242002853321"]').first().clone(true).attr('slider-nav', "Robot multifonction");
                        if (header4.find('.box-item').length) {
                            header4 = $('.sto-product-container').find('div[data-id="4242002853321"] .box-item').first().clone(true).attr('slider-nav', "Robot multifonction");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);

                        var header5 = $('.sto-product-container').find('div[data-id="5053083123444"]').first().clone(true).attr('slider-nav', "FAST AND FURIOUS - DVD");
                        if (header5.find('.box-item').length) {
                            header5 = $('.sto-product-container').find('div[data-id="5053083123444"] .box-item').first().clone(true).attr('slider-nav', "FAST AND FURIOUS - DVD");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);

                        var banner6 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-40% sur tout le site'>-40% sur tout le site</div>");
                        banner6.attr('data-index', '6');
                        banner6.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-40% sur tout le site') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner6);

                        fetch_backup_products();
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
                            var item = $(v);
                            if (i < 4) {
                                if (item.hasClass('box-item-banner')) {
                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
                                }
                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
                            } else {
                                return false;
                            }
                        });
                    } else if (today >= slideDate5) { //29-11
                        //console.log('slider date 5');
                        settings.banners.bijoux = settings.banners.bijoux2;
                        var banner1 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='30% TEL sélection TV'>30% TEL sélection TV</div>");
                        banner1.attr('data-index', '1');
                        banner1.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('30% TEL sélection TV') + '", "urlsortie": "' + cleanStringBlackFriday("https://gti3.hightech.leclerc/dynclick/multimedia-eleclerc-com/?ept-publisher=E-Leclerc_portail&ept-name=portail-S47&eurl=https%3A%2F%2Fwww.hightech.leclerc%2Fblack-friday-la-journee-star-television-b%3Futm_source%3DE-Leclerc_portail%26utm_campaign%3DS47%26utm_medium%3Dleclerc%26utm_content%3DBLACKFRIDAYSPECIALTV") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://gti3.hightech.leclerc/dynclick/multimedia-eleclerc-com/?ept-publisher=E-Leclerc_portail&ept-name=portail-S47&eurl=https%3A%2F%2Fwww.hightech.leclerc%2Fblack-friday-la-journee-star-television-b%3Futm_source%3DE-Leclerc_portail%26utm_campaign%3DS47%26utm_medium%3Dleclerc%26utm_content%3DBLACKFRIDAYSPECIALTV")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner1);

                        var banner2 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-40% sur tout le site'>-40% sur tout le site</div>");
                        banner2.attr('data-index', '2');
                        banner2.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-40% sur tout le site') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner2);

                        var header3 = $('.sto-product-container').find('div[data-id="5051889488682"]').first().clone(true).attr('slider-nav', "Coffret Harry Potter");
                        if (header3.find('.box-item').length) {
                            header3 = $('.sto-product-container').find('div[data-id="5051889488682"] .box-item').first().clone(true).attr('slider-nav', "Coffret Harry Potter");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);

                        var header4 = $('.sto-product-container').find('div[data-id="328277932706000"]').first().clone(true).attr('slider-nav', "Crème bébé Klorane");
                        if (header4.find('.box-item').length) {
                            header4 = $('.sto-product-container').find('div[data-id="328277932706000"] .box-item').first().clone(true).attr('slider-nav', "Crème bébé Klorane");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);

                        var header5 = $('.sto-product-container').find('div[data-id="5030945122531"]').first().clone(true).attr('slider-nav', "Fifa 20 PS4");
                        if (header5.find('.box-item').length) {
                            header5 = $('.sto-product-container').find('div[data-id="5030945122531"] .box-item').first().clone(true).attr('slider-nav', "Fifa 20 PS4");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);

                        var header6 = $('.sto-product-container').find('div[data-id="3700538222734"]').first().clone(true).attr('slider-nav', "Matelas mousse NATURA");
                        if (header6.find('.box-item').length) {
                            header6 = $('.sto-product-container').find('div[data-id="3700538222734"] .box-item').first().clone(true).attr('slider-nav', "Matelas mousse NATURA");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);

                        fetch_backup_products();
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
                            var item = $(v);
                            if (i < 4) {
                                if (item.hasClass('box-item-banner')) {
                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
                                }
                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
                            } else {
                                return false;
                            }
                        });
                    } else if (today >= slideDate4) { //28-11
                        //console.log('slider date 4');
                        var header1 = $('.sto-product-container').find('div[data-id="1168500160"]').first().clone(true).attr('slider-nav', "TV LED SAMSUNG");
                        if (header1.find('.box-item').length) {
                            header1 = $('.sto-product-container').find('div[data-id="1168500160"] .box-item').first().clone(true).attr('slider-nav', "TV LED LG");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header1);

                        /*var header2 = $('.sto-product-container').find('div[data-id="4008789050137"]').first().clone(true).attr('slider-nav', "Commissariat Playmobil");
                        if (header2.find('.box-item').length) {
                            header2 = $('.sto-product-container').find('div[data-id="4008789050137"] .box-item').first().clone(true).attr('slider-nav', "Commissariat Playmobil");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header2);*/

                        var banner3 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-40% sur tout le site'>-40% sur tout le site</div>");
                        banner3.attr('data-index', '3');
                        banner3.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-40% sur tout le site') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner3);

                        /*var header4 = $('.sto-product-container').find('div[data-id="328277918591200"]').first().clone(true).attr('slider-nav', "Huile sublimatrice");
                        if (header4.find('.box-item').length) {
                            header4 = $('.sto-product-container').find('div[data-id="328277918591200"] .box-item').first().clone(true).attr('slider-nav', "Huile sublimatrice");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);*/

                        var header5 = $('.sto-product-container').find('div[data-id="0190781396475"]').first().clone(true).attr('slider-nav', "Moniteur LCD GAMING HP");
                        if (header5.find('.box-item').length) {
                            header5 = $('.sto-product-container').find('div[data-id="0190781396475"] .box-item').first().clone(true).attr('slider-nav', "Moniteur LCD GAMING HP");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);

                        var header6 = $('.sto-product-container').find('div[data-id="1168500600"]').first().clone(true).attr('slider-nav', "Trampoline Kangui");
                        if (header6.find('.box-item').length) {
                            header6 = $('.sto-product-container').find('div[data-id="1168500600"] .box-item').first().clone(true).attr('slider-nav', "Trampoline Kangui");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);

                        fetch_backup_products();
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
                            var item = $(v);
                            if (i < 4) {
                                if (item.hasClass('box-item-banner')) {
                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
                                }
                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
                            } else {
                                return false;
                            }
                        });
                    } else if (today >= slideDate3) { //27-11
                        //console.log('slider date 3');
                        var banner1 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='30% TEL Lego'>30% TEL Lego</div>");
                        banner1.attr('data-index', '1');
                        banner1.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('30% TEL Lego') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.culture.leclerc/jouets-u/jeux-de-construction-u/lego-u?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-27-black-friday&utm_content=slider-hp-journee-star-lego") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.culture.leclerc/jouets-u/jeux-de-construction-u/lego-u?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-27-black-friday&utm_content=slider-hp-journee-star-lego")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner1);

                        var header2 = $('.sto-product-container').find('div[data-id="325869133156001"]').first().clone(true).attr('slider-nav', "Dourthe - Grande cuvée");
                        if (header2.find('.box-item').length) {
                            header2 = $('.sto-product-container').find('div[data-id="325869133156001"] .box-item').first().clone(true).attr('slider-nav', "Dourthe - Grande cuvée");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header2);

                        var header3 = $('.sto-product-container').find('div[data-id="340152852084600"]').first().clone(true).attr('slider-nav', "Atoderm Huile Lavante");
                        if (header3.find('.box-item').length) {
                            header3 = $('.sto-product-container').find('div[data-id="340152852084600"] .box-item').first().clone(true).attr('slider-nav', "Atoderm Huile Lavante");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);

                        /*var header4 = $('.sto-product-container').find('div[data-id="328277010867500"]').first().clone(true).attr('slider-nav', "Gel douche A-DERMA");
                        if (header4.find('.box-item').length) {
                            header4 = $('.sto-product-container').find('div[data-id="328277010867500"] .box-item').first().clone(true).attr('slider-nav', "Gel douche A-DERMA");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);

                        var header5 = $('.sto-product-container').find('div[data-id="326468000515200"]').first().clone(true).attr('slider-nav', "NUXE eau nettoyant");
                        if (header5.find('.box-item').length) {
                            header5 = $('.sto-product-container').find('div[data-id="326468000515200"] .box-item').first().clone(true).attr('slider-nav', "Gel douche A-DERMA");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);*/
                        /*var header3 = $('.sto-product-container').find('div[data-id="805278385072500"]').first().clone(true).attr('slider-nav', "Prosecco Signore Giuseppe ");
                        if (header3.find('.box-item').length) {
                            header3 = $('.sto-product-container').find('div[data-id="805278385072500"] .box-item').first().clone(true).attr('slider-nav', "Prosecco Signore Giuseppe ");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);*/

                        var header6 = $('.sto-product-container').find('div[data-id="0842776102379"]').first().clone(true).attr('slider-nav', "Enceinte Google Mini");
                        if (header6.find('.box-item').length) {
                            header6 = $('.sto-product-container').find('div[data-id="0842776102379"] .box-item').first().clone(true).attr('slider-nav', "Enceinte Google Mini");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);

                        var header7 = $('.sto-product-container').find('div[data-id="3221614003158"]').first().clone(true).attr('slider-nav', "Aspirateur balai");
                        if (header7.find('.box-item').length) {
                            header7 = $('.sto-product-container').find('div[data-id="3221614003158"] .box-item').first().clone(true).attr('slider-nav', "Aspirateur balai");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header7);

                        var header8 = $('.sto-product-container').find('div[data-id="8015244200071"]').first().clone(true).attr('slider-nav', "Vélo pliant électrique");
                        if (header8.find('.box-item').length) {
                            header8 = $('.sto-product-container').find('div[data-id="8015244200071"] .box-item').first().clone(true).attr('slider-nav', "Vélo pliant électrique");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header8);

                        fetch_backup_products();
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
                            var item = $(v);
                            if (i < 4) {
                                if (item.hasClass('box-item-banner')) {
                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
                                }
                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
                            } else {
                                return false;
                            }
                        });
                    } else if (today >= slideDate2) { //26-11
                        //console.log('slider date 2');
                        var header1 = $('.sto-product-container').find('div[data-id="6970244526823"]').first().clone(true).attr('slider-nav', "Trottinette électrique");
                        if (header1.find('.box-item').length) {
                            header1 = $('.sto-product-container').find('div[data-id="6970244526823"] .box-item').first().clone(true).attr('slider-nav', "Trottinette électrique");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header1);

                        var banner2 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='2 albums achetés = 1 offert'>2 albums achetés = 1 offert</div>");
                        banner2.attr('data-index', '2');
                        banner2.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('2 albums achetés = 1 offert') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.culture.leclerc/2000000193649-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-26-black-friday&utm_content=slider-hp-2-albums-achetes-1-offert") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.culture.leclerc/2000000193649-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-26-black-friday&utm_content=slider-hp-2-albums-achetes-1-offert")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner2);

                        var header3 = $('.sto-product-container').find('div[data-id="3760165464969"]').first().clone(true).attr('slider-nav', "Trampoline Kangui");
                        if (header3.find('.box-item').length) {
                            header3 = $('.sto-product-container').find('div[data-id="3760165464969"] .box-item').first().clone(true).attr('slider-nav', "Trampoline Kangui");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);

                        var header4 = $('.sto-product-container').find('div[data-id="376007380363802"]').first().clone(true).attr('slider-nav', "Premières Grives 2018");
                        if (header4.find('.box-item').length) {
                            header4 = $('.sto-product-container').find('div[data-id="376007380363802"] .box-item').first().clone(true).attr('slider-nav', "Premières Grives 2018");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);

                        var banner5 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-15€ sur tous les pneus Dunlop'>-15€ sur tous les pneus Dunlop</div>");
                        banner5.attr('data-index', '5');
                        banner5.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-15€ sur tous les pneus Dunlop') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.auto.leclerc/pneus-dunlop-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.auto.leclerc/pneus-dunlop-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner5);

                        var header6 = $('.sto-product-container').find('div[data-id="8013298204922"]').first().clone(true).attr('slider-nav', "Nettoyeur pression ");
                        if (header6.find('.box-item').length) {
                            header6 = $('.sto-product-container').find('div[data-id="8013298204922"] .box-item').first().clone(true).attr('slider-nav', "Nettoyeur pression ");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);
                        fetch_backup_products();
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
                            var item = $(v);
                            if (i < 4) {
                                if (item.hasClass('box-item-banner')) {
                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
                                }
                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
                            } else {
                                return false;
                            }
                        });
                    } else if (today >= slideDate1) { //25-11
                        //console.log('slider date 1');

                        var header1 = $('.sto-product-container').find('div[data-id="0194441170290"]').first().clone(true).attr('slider-nav', "PC Portable HP 14\"");
                        if (header1.find('.box-item').length) {
                            header1 = $('.sto-product-container').find('div[data-id="0194441170290"] .box-item').first().clone(true).attr('slider-nav', "PC Portable HP 14\"");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header1);

                        /*var header2 = $('.sto-product-container').find('div[data-id="3401360147508"]').first().clone(true).attr('slider-nav', "Bioderma SEBIUM GLOBAL");
                        if (header2.find('.box-item').length) {
                            header2 = $('.sto-product-container').find('div[data-id="3401360147508"] .box-item').first().clone(true).attr('slider-nav', "Bioderma SEBIUM GLOBAL");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header2);*/

                        var banner2 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='Console Nintendo Switch + FIFA'>Console Nintendo Switch + FIFA</div>");
                        banner2.attr('data-index', '2');
                        banner2.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('Console Nintendo Switch + FIFA') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.culture.leclerc/2000000194141-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp-console-nintendo-switch-fifa-20") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.culture.leclerc/2000000194141-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp-console-nintendo-switch-fifa-20")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner2);

                        var banner3 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-15€ sur tous les pneus Dunlop'>-15€ sur tous les pneus Dunlop</div>");
                        banner3.attr('data-index', '3');
                        banner3.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-15€ sur tous les pneus Dunlop') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.auto.leclerc/pneus-dunlop-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.auto.leclerc/pneus-dunlop-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner3);

                        var header4 = $('.sto-product-container').find('div[data-id="3016661150500"]').first().clone(true).attr('slider-nav', "Blender chauffant");
                        if (header4.find('.box-item').length) {
                            header4 = $('.sto-product-container').find('div[data-id="3016661150500"] .box-item').first().clone(true).attr('slider-nav', "Blender chauffant");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);

                        /*var header5 = $('.sto-product-container').find('div[data-id="3401360147508"]').first().clone(true).attr('slider-nav', "Bioderma SEBIUM GLOBAL");
                        if (header5.find('.box-item').length) {
                            header5 = $('.sto-product-container').find('div[data-id="3401360147508"] .box-item').first().clone(true).attr('slider-nav', "Bioderma SEBIUM GLOBAL");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);*/

                        var header6 = $('.sto-product-container').find('div[data-id="805278385072500"]').first().clone(true).attr('slider-nav', "Prosecco Signore Giuseppe ");
                        if (header6.find('.box-item').length) {
                            header6 = $('.sto-product-container').find('div[data-id="805278385072500"] .box-item').first().clone(true).attr('slider-nav', "Prosecco Signore Giuseppe ");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);

                        fetch_backup_products();
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
                            var item = $(v);
                            if (i < 4) {
                                if (item.hasClass('box-item-banner')) {
                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
                                }
                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
                            } else {
                                return false;
                            }
                        });
                    } else {
                        //console.log('slider other date');
                        var header1 = $('.sto-product-container').find('div[data-id="194441170290"]').first().clone(true).attr('slider-nav', "PC Portable HP 14");
                        if (header1.find('.box-item').length) {
                            header1 = $('.sto-product-container').find('div[data-id="194441170290"] .box-item').first().clone(true).attr('slider-nav', "PC Portable HP 14");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header1);

                        var banner2 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='Console Nintendo Switch + FIFA 20'>Console Nintendo Switch + FIFA 20</div>");
                        banner2.attr('data-index', '2');
                        banner2.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('Console Nintendo Switch + FIFA 20') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.culture.leclerc/2000000194141-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp-console-nintendo-switch-fifa-20") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.culture.leclerc/2000000194141-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp-console-nintendo-switch-fifa-20")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner2);

                        var banner3 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-15€ sur tous les pneus Dunlop'>-15€ sur tous les pneus Dunlop</div>");
                        banner3.attr('data-index', '3');
                        banner3.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-15€ sur tous les pneus Dunlop') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.auto.leclerc/pneus-dunlop-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.auto.leclerc/pneus-dunlop-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner3);

                        var header4 = $('.sto-product-container').find('div[data-id="3016661150500"]').first().clone(true).attr('slider-nav', "Blender chauffant");
                        if (header4.find('.box-item').length) {
                            header4 = $('.sto-product-container').find('div[data-id="3016661150500"] .box-item').first().clone(true).attr('slider-nav', "Blender chauffant");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);

                        var header5 = $('.sto-product-container').find('div[data-id="3401360147508"]').first().clone(true).attr('slider-nav', "Bioderma SEBIUM GLOBAL");
                        if (header5.find('.box-item').length) {
                            header5 = $('.sto-product-container').find('div[data-id="3401360147508"] .box-item').first().clone(true).attr('slider-nav', "Bioderma SEBIUM GLOBAL");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);

                        var header6 = $('.sto-product-container').find('div[data-id="805278385072500"]').first().clone(true).attr('slider-nav', "Prosecco Signore Giuseppe ");
                        if (header6.find('.box-item').length) {
                            header6 = $('.sto-product-container').find('div[data-id="805278385072500"] .box-item').first().clone(true).attr('slider-nav', "Prosecco Signore Giuseppe ");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);

                        var banner4 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='CODE PROMO SPORT'>CODE PROMO SPORT</div>");
                        banner4.attr('data-index', '4');
                        banner4.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('CODE PROMO SPORT') + '", "urlsortie": "' + cleanStringBlackFriday("https://gti3.sport.leclerc/dynclick/sport-leclerc/?ept-publisher=E-Leclerc_portail&ept-name=portail-offreblackfriday&eurl=https%3A%2F%2Fwww.sport.leclerc%2Foperation-moins-30%3Futm_source%3DE-Leclerc_portail%26utm_campaign%3Doffreblackfriday%26utm_medium%3Dleclerc%26utm_content%3Doffretextile") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://gti3.sport.leclerc/dynclick/sport-leclerc/?ept-publisher=E-Leclerc_portail&ept-name=portail-offreblackfriday&eurl=https%3A%2F%2Fwww.sport.leclerc%2Foperation-moins-30%3Futm_source%3DE-Leclerc_portail%26utm_campaign%3Doffreblackfriday%26utm_medium%3Dleclerc%26utm_content%3Doffretextile")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner4);
                        var header5 = $('.sto-product-container').find('div[data-id="8801643066956"]').first().clone(true).attr('slider-nav', "SMARTPHONE");
                        if (header5.find('.box-item').length) {
                            header5 = $('.sto-product-container').find('div[data-id="8801643066956"] .box-item').first().clone(true).attr('slider-nav', "SMARTPHONE");
                        }
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);
                        var banner6 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='VENTES PRIVEES'>VENTES PRIVEES</div>");
                        banner6.attr('data-index', '6');
                        banner6.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('BrandAlley') + '", "urlsortie": "' + cleanStringBlackFriday("https://eulerian.brandalley.fr/dynclick/leclerc-brandalley-fr/?ept-publisher=Drive&ept-name=Drive_Tunnel_web_1189x176&eurl=https://leclerc.brandalley.fr/event/black-Friday") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://eulerian.brandalley.fr/dynclick/leclerc-brandalley-fr/?ept-publisher=Drive&ept-name=Drive_Tunnel_web_1189x176&eurl=https://leclerc.brandalley.fr/event/black-Friday")');
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner6);
                        fetch_backup_products();
                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
                            var item = $(v);
                            if (i < 4) {
                                if (item.hasClass('box-item-banner')) {
                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
                                }
                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
                            } else {
                                return false;
                            }
                        });
                    }


                    /* Apply tracking for homepage slider */
                    pagination_container = $('.sto-' + placeholder + '-category-page .sto-category-page-pagination');

                    if (/hightech/.test(window.location.href)) {
                        // URL High-Tech detected
                        show_category("hightech", container, tracker);
                    } else if (/jardin\/maison/.test(window.location.href)) {
                        // URL Jardin, Maison & Electroménager detected
                        show_category("maison", container, tracker);
                    } else if (/culture\/jouets/.test(window.location.href)) {
                        // URL Culture & Jouets detected
                        show_category("culture", container, tracker);
                    } else if (/parapharmacie/.test(window.location.href)) {
                        // URL Parapharmacie detected
                        show_category("parapharmacie", container, tracker);
                    } else if (/mode\/sport/.test(window.location.href)) {
                        // URL Mode & Sport detected
                        show_category("mode", container, tracker);
                    } else if (/vins\/alcools/.test(window.location.href)) {
                        // URL Vins & Alcools detected
                        show_category("vin", container, tracker);
                    } else if (/epicerie/.test(window.location.href)) {
                        // URL Epicerie fine detected
                        show_category("alimentaire", container, tracker);
                    } else if (/bijoux/.test(window.location.href)) {
                        // URL Bijoux detected
                        show_category("bijoux", container, tracker);
                    } else if (/all\/products/.test(window.location.href)) {
                        // URL Tous les produits detected
                        show_category("all", container, tracker);
                    }

                    $('.box-item').each(function(i, v) {
                        if ($(this).find('.btn-memo').length == 0 && $(this).find('.btn-achat-ligne').length > 0) {
                            $(this).addClass('sto-exclu-web');
                        } else if ($(this).find('.btn-memo').length > 0 && $(this).find('.btn-achat-ligne').length <= 0) {
                            $(this).addClass('sto-en-magasin');
                        } else {
                            $(this).removeClass('sto-en-magasin').removeClass('sto-exclu-web');
                        }
                    });

                    $('.sto-' + placeholder + '-content-section').find('.sto-' + placeholder + '-prods-slides-wrapper').each(function(i, v) {
                        var prodsWrapper = $(this);
                        if (prodsWrapper.children().length === 0) {
                            prodsWrapper.parents('.sto-' + placeholder + '-content-section-products').children('.sto-' + placeholder + '-prods-slider-prev').remove();
                            prodsWrapper.parents('.sto-' + placeholder + '-content-section-products').children('.sto-' + placeholder + '-prods-slider-next').remove();
                            prodsWrapper.parents('.sto-' + placeholder + '-prods-slider').hide();
                        }
                    });

                    //check for empy sections
                    var tgSections = $('.sto-black-friday-content-section');
                    for (var i = 0; i < 8; i++) {
                        if (tgSections.eq(i).find('.box-item').length <= 0) {
                            tgSections.eq(i).addClass('sectionEmpty');

                        }
                    }

                    /*
                     *      EVENT HANDLERS
                     */

                    /* ------- Homepage header slider event handlers ------- */
                    container.find('.sto-' + placeholder + '-header-slider-next').on('click', function() {
                        var slideWidth = $('.sto-' + placeholder + '-header-slide').outerWidth();
                        var sliderPosLeft = $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').position().left;
                        var oldSlideIndex = $('.sto-header-slide-active').attr('data-ind');

                        if (oldSlideIndex == '4') {
                            var newSlideIndex = '1';
                        } else {
                            var newSlideIndex = (parseFloat(oldSlideIndex) + 1) + '';
                        }

                        $('.sto-' + placeholder + '-header-slide, .sto-' + placeholder + '-header-slider-nav-btn').removeClass('sto-header-slide-active');
                        $('.sto-' + placeholder + '-header-slide[data-ind="' + newSlideIndex + '"], .sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + newSlideIndex + '"]').addClass('sto-header-slide-active');

                        if (oldSlideIndex == '4' && !$('.sto-' + placeholder + '-header-slides-wrapper').is(':animated')) {
                            $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').stop().animate({
                                "left": 0
                            }, 'slow');
                        } else if (!$('.sto-' + placeholder + '-header-slides-wrapper').is(':animated')) {
                            $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').stop().animate({
                                "left": "-" + (parseInt(oldSlideIndex) * 100) + "%"
                            }, 'slow');
                        }
                    });
                    container.find('.sto-' + placeholder + '-header-slider-prev').on('click', function() {
                        var slideWidth = $('.sto-' + placeholder + '-header-slide').outerWidth();
                        var sliderPosLeft = $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').position().left;
                        var oldSlideIndex = $('.sto-header-slide-active').attr('data-ind');

                        if (oldSlideIndex == '1') {
                            var newSlideIndex = '4';
                        } else {
                            var newSlideIndex = (parseFloat(oldSlideIndex) - 1) + '';
                        }

                        $('.sto-' + placeholder + '-header-slide, .sto-' + placeholder + '-header-slider-nav-btn').removeClass('sto-header-slide-active');
                        $('.sto-' + placeholder + '-header-slide[data-ind="' + newSlideIndex + '"], .sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + newSlideIndex + '"]').addClass('sto-header-slide-active');

                        if (oldSlideIndex == '1' && !$('.sto-' + placeholder + '-header-slides-wrapper').is(':animated')) {
                            $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').stop().animate({
                                "left": '-300%'
                            }, 'slow');
                        } else if (!$('.sto-' + placeholder + '-header-slides-wrapper').is(':animated')) {
                            $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').stop().animate({
                                "left": "-" + ((parseInt(newSlideIndex) - 1) * 100) + "%"
                            }, 'slow');
                        }
                    });
                    container.find('.sto-' + placeholder + '-header-slider-nav-btn').on('click', function() {
                        var thisCat = $(this).attr('data-cat');
                        var slideWidth = $('.sto-' + placeholder + '-header-slide').outerWidth();
                        var thisSlideIndex = parseFloat($(this).attr('data-ind'));

                        $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').stop().animate({
                            "left": (thisSlideIndex - 1) * -slideWidth
                        }, 'slow');

                        $('.sto-' + placeholder + '-header-slide, .sto-' + placeholder + '-header-slider-nav-btn').removeClass('sto-header-slide-active');
                        $('.sto-' + placeholder + '-header-slide[data-ind="' + thisSlideIndex + '"], .sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + thisSlideIndex + '"]').addClass('sto-header-slide-active');
                    });

                    /* ------- Homepage header MOBILE slider event handlers NEW SLIDER MOBILE HEADER ------- */
                    container.find('.sto-' + placeholder + '-header-slider-mob-next').on('click', function() {
                        $('.sto-black-friday-header-slide').addClass('smooth-slider');
                        var oldSlideIndex = parseInt($('.sto-header-slide-active').attr('data-ind'));
                        var newSlideIndex = oldSlideIndex == 4 ? 1 : oldSlideIndex + 1;
                        $('.sto-black-friday-header-slides-wrapper').animate({
                            scrollLeft: $('.sto-black-friday-header-slide').width() * (newSlideIndex - 1)
                        }, 500);
                        $('.sto-' + placeholder + '-header-slide, .sto-' + placeholder + '-header-slider-nav-btn').removeClass('sto-header-slide-active');
                        $('.sto-' + placeholder + '-header-slide[data-ind="' + newSlideIndex + '"], .sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + newSlideIndex + '"]').addClass('sto-header-slide-active');
                        setTimeout(function() {
                            $('.sto-black-friday-header-slide').removeClass('smooth-slider');
                        }, 500);
                    });
                    container.find('.sto-' + placeholder + '-header-slider-mob-prev').on('click', function() {
                        $('.sto-black-friday-header-slide').addClass('smooth-slider');
                        var oldSlideIndex = parseInt($('.sto-header-slide-active').attr('data-ind'));
                        var newSlideIndex = oldSlideIndex == 1 ? 4 : oldSlideIndex - 1;
                        $('.sto-black-friday-header-slides-wrapper').animate({
                            scrollLeft: $('.sto-black-friday-header-slide').width() * (newSlideIndex - 1)
                        }, 500);
                        $('.sto-' + placeholder + '-header-slide, .sto-' + placeholder + '-header-slider-nav-btn').removeClass('sto-header-slide-active');
                        $('.sto-' + placeholder + '-header-slide[data-ind="' + newSlideIndex + '"], .sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + newSlideIndex + '"]').addClass('sto-header-slide-active');
                        setTimeout(function() {
                            $('.sto-black-friday-header-slide').removeClass('smooth-slider');
                        }, 500);
                    });

                    /* Homepage products slider mobile event handlers NEW SLIDER MOBILE SECCION */
                    container.find('.sto-' + placeholder + '-prods-slider-next').on('click', function() {
                        var parent = $(this).parents('.sto-' + placeholder + '-content-section-products');
                        var oldSlideIndex = parseInt(parent.find('.sto-prods-slide-active').attr('data-ind'));
                        var newSlideIndex = oldSlideIndex == 4 ? 1 : oldSlideIndex + 1;
                        parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').addClass('smooth-slider');
                        parent.find('.sto-black-friday-prods-slides-wrapper').animate({
                            scrollLeft: parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').width() * (newSlideIndex - 1)
                        }, 600);
                        parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').removeClass('sto-prods-slide-active');
                        parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item[data-ind="' + newSlideIndex + '"]').addClass('sto-prods-slide-active');
                        setTimeout(function() {
                            parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').removeClass('smooth-slider');
                        }, 500);
                    });
                    container.find('.sto-' + placeholder + '-prods-slider-prev').on('click', function() {
                        var parent = $(this).parents('.sto-' + placeholder + '-content-section-products');
                        var oldSlideIndex = parseInt(parent.find('.sto-prods-slide-active').attr('data-ind'));
                        var newSlideIndex = oldSlideIndex == 1 ? 4 : oldSlideIndex - 1;
                        parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').addClass('smooth-slider');
                        parent.find('.sto-black-friday-prods-slides-wrapper').animate({
                            scrollLeft: parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').width() * (newSlideIndex - 1)
                        }, 600);
                        parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').removeClass('sto-prods-slide-active');
                        parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item[data-ind="' + newSlideIndex + '"]').addClass('sto-prods-slide-active');
                        setTimeout(function() {
                            parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').removeClass('smooth-slider');
                        }, 500);
                    });

                    /* Homepage banner tile mobile event handler */
                    container.find('.sto-' + placeholder + '-banner-slider-next').on('click', function() {
                        var parent = $(this).parents('.sto-' + placeholder + '-content-section-products');
                        var slideWidth = parent.find('.sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile').outerWidth();
                        var sliderPosLeft = parent.find('.sto-' + placeholder + '-banner-slides-wrapper').position().left;
                        var oldSlideIndex = parent.find('.sto-banner-slide-active').attr('data-ind');

                        if (oldSlideIndex == '4') {
                            var newSlideIndex = '1';
                        } else {
                            var newSlideIndex = (parseFloat(oldSlideIndex) + 1) + '';
                        }

                        parent.find('.sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile').removeClass('sto-banner-slide-active');
                        parent.find('.sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile[data-ind="' + newSlideIndex + '"]').addClass('sto-banner-slide-active');

                        if (oldSlideIndex == '4' && !$('.sto-' + placeholder + '-banner-slides-wrapper').is(':animated')) {
                            parent.find('.sto-' + placeholder + '-banner-slides-wrapper').stop().animate({
                                "left": 0
                            }, 'slow');
                        } else if (!$('.sto-' + placeholder + '-banner-slides-wrapper').is(':animated')) {
                            parent.find('.sto-' + placeholder + '-banner-slides-wrapper').stop().animate({
                                "left": "-" + (parseInt(oldSlideIndex) * 100) + "%"
                            }, 'slow');
                        }
                    });
                    container.find('.sto-' + placeholder + '-banner-slider-prev').on('click', function() {
                        var parent = $(this).parents('.sto-' + placeholder + '-content-section-products');
                        var slideWidth = parent.find('.sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile').outerWidth();
                        var sliderPosLeft = parent.find('.sto-' + placeholder + '-banner-slides-wrapper').position().left;
                        var oldSlideIndex = parent.find('.sto-banner-slide-active').attr('data-ind');

                        if (oldSlideIndex == '1') {
                            var newSlideIndex = '4';
                        } else {
                            var newSlideIndex = (parseFloat(oldSlideIndex) - 1) + '';
                        }

                        parent.find('.sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile').removeClass('sto-banner-slide-active');
                        parent.find('.sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile[data-ind="' + newSlideIndex + '"]').addClass('sto-banner-slide-active');

                        if (oldSlideIndex == '1' && !$('.sto-' + placeholder + '-banner-slides-wrapper').is(':animated')) {
                            parent.find('.sto-' + placeholder + '-banner-slides-wrapper').stop().animate({
                                "left": '-300%'
                            }, 'slow');
                        } else if (!$('.sto-' + placeholder + '-banner-slides-wrapper').is(':animated')) {
                            parent.find('.sto-' + placeholder + '-banner-slides-wrapper').stop().animate({
                                "left": "-" + ((parseInt(newSlideIndex) - 1) * 100) + "%"
                            }, 'slow');
                        }
                    });

                    /* Homepage mobile menu event handler */
                    container.find('.sto-' + placeholder + '-content-main-claim').on('click', function() {
                        if ($(this).parents('.sto-' + placeholder + '-content-wrapper').hasClass('menuopen')) {
                            $(this).parents('.sto-' + placeholder + '-content-wrapper').removeClass('menuopen');
                        } else {
                            $(this).parents('.sto-' + placeholder + '-content-wrapper').addClass('menuopen');
                        }
                    });

                    /* ------- Homepage universe CTA event handlers ------- */
                    container.find('.sto-' + placeholder + '-nav-slides-wrapper > a').on('click', function(e) {
                        //e.preventDefault();
                        //e.stopPropagation();

                        var self = $(this);
                        var category = self.data('cat');
                        show_category(category, container, tracker);
                    });
                    container.find('.sto-' + placeholder + '-content-section .sto-' + placeholder + '-content-section-seemore > span').on('click', function(e) {
                        var self = $(this);
                        var category = self.parents('.sto-' + placeholder + '-content-section').data('cat');
                        if (category == "culture") {
                            dataLayer.push({
                                event: 'outon_voirtouslesproduits_' + category + '&jouet',
                                nomcampagne: "blackfriday2019"
                            });
                        } else if (category == "vin") {
                            dataLayer.push({
                                event: 'outon_voirtouslesproduits_' + category + 's&alcool',
                                nomcampagne: "blackfriday2019"
                            });
                        } else {
                            dataLayer.push({
                                event: 'outon_voirtouslesproduits_' + category,
                                nomcampagne: "blackfriday2019"
                            });
                        }
                        show_category(category, container, tracker);
                    });

                    /* ------- Universe page back button event handler ------- */
                    container.find('.sto-' + placeholder + '-category-page .sto-category-page-banner .sto-category-banner-cta, .sto-' + placeholder + '-category-page .sto-category-banner-cta-mob').on('click', function() {
                        $('.sto-' + placeholder + '-category-page').hide();
                        $('.sto-' + placeholder + '-home-page').show();
                        resize_gutters();
                    });

                    /* ------- Universe page filtre buttons event handlers ------- */
                    container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-current').on('click', function() {
                        var elem = $(this);
                        if (container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-list').is(':visible')) {
                            container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-list').hide();
                            elem.removeClass('active');
                        } else {
                            container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-list').show();
                            elem.addClass('active');
                        }
                    });
                    container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').on('click', function() {
                        var elem = $(this);
                        if (container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').is(':visible')) {
                            container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').hide();
                            elem.removeClass('active');
                        } else {
                            container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').show();
                            elem.addClass('active');
                        }
                    });
                    container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-category-page-filter-price').on('click', function() {
                        var category = $(this).parents('.sto-' + placeholder + '-category-page').attr('category');
                        if (category === 'all') category = $(this).parents('.sto-' + placeholder + '-category-page').attr('universe');
                        var contentSort = $(this).text();
                        sorting = $(this).data('sort');

                        $(this).parents('.sto-price-filter-list').find('.active').removeClass('active');
                        $(this).addClass('active');

                        $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-current').find('.sto-current-price-filter').text(contentSort);

                        if ($('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-list').is(':visible')) {
                            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-list').hide();
                            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-current').removeClass('active');
                        } else {
                            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-list').show();
                            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-current').addClass('active');
                        }

                        $('.sto-' + placeholder + '-category-page .sto-category-page-products').empty();
                        var paginateArray = sort_products(category);
                        if (category === "bijoux") {
                            if (paginateArray.length > paginationBijoux) {
                                paginate_category(paginateArray, container, tracker);
                            } else {
                                unpaginate_category(paginateArray);
                            }
                        } else {
                            if (paginateArray.length > paginationLimit) {
                                paginate_category(paginateArray, container, tracker);
                            } else {
                                unpaginate_category(paginateArray);
                            }
                        }
                    });

                    // Resize gutters
                    resize_gutters();
                    var itemHeight = $('.sto-' + placeholder + '-prods-slider .sto-' + placeholder + '-prods-slides-wrapper .box-item.box-item-rayon').outerHeight();
                    $('.sto-' + placeholder + '-prods-slider-prev, .sto-' + placeholder + '-prods-slider-next').css({
                        'top': ((itemHeight / 2) - ($('.sto-' + placeholder + '-prods-slider-next').outerHeight() / 2)) + "px"
                    });
                    var itemHeight = $('.sto-' + placeholder + '-banner-slider .sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile').outerHeight();
                    $('.sto-' + placeholder + '-banner-slider-prev, .sto-' + placeholder + '-banner-slider-next').css({
                        'top': ((itemHeight / 2) - ($('.sto-' + placeholder + '-banner-slider-next').outerHeight() / 2)) + "px"
                    });

                    window.addEventListener("resize", function() {
                        windowSize = window.innerWidth;

                        checkWindowSize();

                        resize_gutters();

                        if (windowSize <= 1023) {
                            $('#sto-' + placeholder + '-banner-photo-link').attr('href', 'http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_750x295_08463_30TTN18');
                            $('#sto-' + placeholder + '-banner-photo-link').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': '', 'urlsortie': '" + cleanStringBlackFriday('www.photomoinscher.leclerc?cref=om_coop_blackfriday_750x295_08463_30TTN18') + "', 'event': 'blackfriday_traffickingexterne_visuels'});");
                            $('#sto-' + placeholder + '-banner-photo-link-2').attr('href', 'http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_750x295_08463_40TTN18');
                            $('#sto-' + placeholder + '-banner-photo-link-2').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': '', 'urlsortie': '" + cleanStringBlackFriday('http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_750x295_08463_40TTN18') + "', 'event': 'blackfriday_traffickingexterne_visuels'});");
                        } else {
                            $('#sto-' + placeholder + '-banner-photo-link').attr('href', 'http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_1189x176_08463_30TTN18');
                            $('#sto-' + placeholder + '-banner-photo-link').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': '', 'urlsortie': '" + cleanStringBlackFriday('www.photomoinscher.leclerc?cref=om_coop_blackfriday_1189x176_08463_30TTN18') + "', 'event': 'blackfriday_traffickingexterne_visuels'});");
                            $('#sto-' + placeholder + '-banner-photo-link-2').attr('href', 'http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_1189x176_08463_40TTN18');
                            $('#sto-' + placeholder + '-banner-photo-link-2').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': '', 'urlsortie': '" + cleanStringBlackFriday('http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_1189x176_08463_40TTN18') + "', 'event': 'blackfriday_traffickingexterne_visuels'});");
                        }

                        var itemHeight = $('.sto-' + placeholder + '-prods-slider .sto-' + placeholder + '-prods-slides-wrapper .box-item.box-item-rayon').outerHeight();
                        $('.sto-' + placeholder + '-prods-slider-prev, .sto-' + placeholder + '-prods-slider-next').css({
                            'top': ((itemHeight / 2) - ($('.sto-' + placeholder + '-prods-slider-next').outerHeight() / 2)) + "px"
                        });
                        var itemHeight = $('.sto-' + placeholder + '-banner-slider .sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile').outerHeight();
                        $('.sto-' + placeholder + '-banner-slider-prev, .sto-' + placeholder + '-banner-slider-next').css({
                            'top': ((itemHeight / 2) - ($('.sto-' + placeholder + '-banner-slider-next').outerHeight() / 2)) + "px"
                        });

                        if ($('.sto-' + placeholder + '-category-page').is(':visible')) {
                            $('.sto-' + placeholder + '-category-page .sto-category-page-products').empty();
                            var category = $('.sto-' + placeholder + '-category-page').attr('category');
                            var paginateArray = sort_products(category);
                            if (category === "bijoux") {
                                if (paginateArray.length > paginationBijoux) {
                                    paginate_category(paginateArray, container, tracker);
                                } else {
                                    unpaginate_category(paginateArray);
                                }
                            } else {
                                if (paginateArray.length > paginationLimit) {
                                    paginate_category(paginateArray, container, tracker);
                                } else {
                                    unpaginate_category(paginateArray);
                                }
                            }
                            resize_gutters();
                        }
                    });
                }).then(null, console.log.bind(console));
            });

            var removers = {};
            removers[format] = function() {
                style.unuse();
                container.remove();
            };
            return removers;
        });
    }

    // functions
    function checkWindowSize() {
        if (windowSize > 1296) {
            paginationLimit = 20;
            paginationBijoux = 19;
            hpProductLimit = 4;
        } else if (windowSize <= 1296 && windowSize > 1023) {
            paginationLimit = 15;
            paginationBijoux = 13;
            hpProductLimit = 3;
        } else if (windowSize <= 1023) {
            paginationLimit = 20;
            paginationBijoux = 15;
            hpProductLimit = 1;
            $('.sto-pagination-top.sto-category-page-pagination').remove();
            pagination_container = $('.sto-pagination-bottom.sto-category-page-pagination');
        }

        if (windowSize <= 1023) {
            $('#sto-' + placeholder + '-banner-photo-link').attr('href', 'http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_750x295_08463_30TTN18');
            $('#sto-' + placeholder + '-banner-photo-link').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': '', 'urlsortie': '" + cleanStringBlackFriday('www.photomoinscher.leclerc?cref=om_coop_blackfriday_750x295_08463_30TTN18') + "', 'event': 'blackfriday_traffickingexterne_visuels'});");
        } else {
            $('#sto-' + placeholder + '-banner-photo-link').attr('href', 'http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_1189x176_08463_30TTN18');
            $('#sto-' + placeholder + '-banner-photo-link').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': '', 'urlsortie': '" + cleanStringBlackFriday('www.photomoinscher.leclerc?cref=om_coop_blackfriday_1189x176_08463_30TTN18') + "', 'event': 'blackfriday_traffickingexterne_visuels'});");
        }
    }

    function goUpBtn() {
        $(window).scroll(function() {
            if ($(this).scrollTop() >= 500) { // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200); // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200); // Else fade out the arrow
            }
        });
        $(document).on("click", '#return-to-top', function() { // When arrow is clicked
            if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
                window.scrollTo(0, 0) // first value for left offset, second value for top offset
            } else {
                $('html').animate({
                    scrollTop: 0 // Scroll to top of body
                }, 500);
            }
        });
        $(document).on("touchstart", '#return-to-top', function() { // When arrow is clicked
            if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
                window.scrollTo(0, 0) // first value for left offset, second value for top offset
            } else {
                $('html').animate({
                    scrollTop: 0 // Scroll to top of body
                }, 500);
            }
        });
    }

    function enableSwipe() {

        // Helper function to check if event object has properties required to detect a swipe.
        function eventObjectHasProperties(obj) {
            return obj &&
                obj.originalEvent &&
                obj.originalEvent.touches &&
                obj.originalEvent.touches.length
        }

        let touchStartX;
        let touchStartY;
        $(window).on('touchstart', function(event) {
            if (!eventObjectHasProperties(event)) return;
            touchStartX = event.originalEvent.touches[0].clientX;
            touchStartY = event.originalEvent.touches[0].clientY;
            $(window).on('touchmove', detectSwipe);
        });

        function detectSwipe(event) {

            // Prevent the same swipe being identified multiple times.
            $(window).off('touchmove', detectSwipe)

            // Not sure why but sometimes these properties aren't present.
            if (!eventObjectHasProperties(event)) return;

            // Using coordinates set ontouchstart, compute change in X and Y axes as a result of touch.
            const deltaX = touchStartX - event.originalEvent.touches[0].clientX;
            const deltaY = touchStartY - event.originalEvent.touches[0].clientY;

            // Ensure the touch event was fired from within the format and that it was a horizontal swipe.
            if ($(event.target).parents('.sto-container').length && Math.abs(deltaX) > Math.abs(deltaY)) {

                // Delta for a right-to-left (aka "next") swipe will be positive.
                if (deltaX > 0) {

                    // Handle the swipe when the touch completes.
                    $(window).on('touchend', handleNextSwipe)

                    // Otherwise, it's a left-to-right (aka "previous") swipe, which is the inverse.
                } else {

                    // Handle the swipe when the touch completes.
                    $(window).on('touchend', handlePrevSwipe)
                }
            }
        }

        function handleNextSwipe() {

            // Prevent this from firing more than once per swipe.
            $(window).off('touchend', handleNextSwipe);

            // Helper function that returns the next element of a given type (e.g. indicator or product)
            function getNextElement(elementType, selector) {
                const $items = $(`.sto-${elementType}`);
                const currentIndex = $items.index($(`.sto-${elementType}.${selector}`));
                return $items.eq((currentIndex + 1) % $items.length);
            }

            // Get next elements given their type and position class.
            const $upcomingCurrent = getNextElement('indicator', 'current');
            const $upcomingPrev = getNextElement('item', 'prev-item');
            const $upcomingSelected = getNextElement('item', 'selected-item');
            const $upcomingNext = getNextElement('item', 'next-item');

            // Remove existing position classes.
            $('.sto-item').removeClass('selected-item prev-item next-item');
            $('.sto-indicator.current').removeClass('current');

            // Update next elements with relevant position class.
            $upcomingCurrent.addClass('current');
            $upcomingPrev.addClass('prev-item');
            $upcomingSelected.addClass('selected-item');
            $upcomingNext.addClass('next-item');
        }

        function handlePrevSwipe() {

            // Prevent this from firing more than once per swipe.
            $(window).off('touchend', handlePrevSwipe);

            // Helper function that returns the previous element of a given type (e.g. indicator or product)
            function getPrevElement(elementType, selector) {
                const $items = $(`.sto-${elementType}`);
                const currentIndex = $items.index($(`.sto-${elementType}.${selector}`));
                if (currentIndex === 0) {
                    return $items.eq($items.length - 1);
                } else {
                    return $items.eq(currentIndex - 1);
                }
            }

            // Get previous elements given their type and position class.
            const $upcomingCurrent = getPrevElement('indicator', 'current');
            const $upcomingPrev = getPrevElement('item', 'prev-item');
            const $upcomingSelected = getPrevElement('item', 'selected-item');
            const $upcomingNext = getPrevElement('item', 'next-item');

            // Remove existing position classes.
            $('.sto-item').removeClass('selected-item prev-item next-item');
            $('.sto-indicator.current').removeClass('current');

            // Update previous elements with relevant position class.
            $upcomingCurrent.addClass('current');
            $upcomingPrev.addClass('prev-item');
            $upcomingSelected.addClass('selected-item');
            $upcomingNext.addClass('next-item');
        }
    }

    function show_category(category, container, tracker) {
        if ($('.sto-' + placeholder + '-content-wrapper').hasClass('menuopen')) {
            $('.sto-' + placeholder + '-content-wrapper').removeClass('menuopen');
        }
        $('.sto-' + placeholder + '-home-page').hide();
        $('.sto-' + placeholder + '-category-page').show().attr('category', category);
        $('.sto-' + placeholder + '-category-page .sto-category-page-products').empty();
        $(window).unbind('scroll');
        $(window).scrollTop(0);

        pageNumber = 1;

        append_subcategories(category, container, tracker);

        subcategory = "all";
        $('.sto-' + placeholder + '-category-page').attr('subcategory', subcategory);
        if (category == "bijoux") sorting = "ASC";
        var paginateArray = sort_products(category);
        $('.sto-' + placeholder + '-category-page > .sto-category-page-products').html(paginateArray);
        if (category === "bijoux") {
            if (paginateArray.length > paginationBijoux) {
                paginate_category(paginateArray, container, tracker);
            } else {
                unpaginate_category(paginateArray);
            }
        } else {
            if (paginateArray.length > paginationLimit) {
                paginate_category(paginateArray, container, tracker);
            } else {
                unpaginate_category(paginateArray);
            }
        }
        resize_gutters();
    }

    function append_subcategories(category, container, tracker) {
        var subcategoriesArray = settings.universe[category];

        if (category !== "all") {
            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-current .sto-current-subcategory-filter').text("Toutes les catégories");
            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-current').removeClass('active');
            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-list').empty();
            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').hide();
            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-list').append('<div class="sto-category-page-filter-subcategory active" data-subcategory="all">Toutes les catégories</div>');
            $.each(subcategoriesArray, function(i, v) {
                var productLength = 0;
                if (sto.utils.fn.cleanString(v) === "entretien_et_soin_de_la_maison") {
                    var subcatHtml = $('<div class="sto-category-page-filter-subcategory" data-subcategory="' + sto.utils.fn.cleanString(v) + '">Entretien soin de la maison</div>');
                } else {
                    var subcatHtml = $('<div class="sto-category-page-filter-subcategory" data-subcategory="' + sto.utils.fn.cleanString(v) + '">' + v + '</div>');
                }
                $.each($('#sto-prod-storage-' + category + ' .sto-product-container>div'), function(idx, value) {
                    var prodBox = $(this);
                    var prodSubcategory = prodBox.data('category-1');
                    if (typeof prodSubcategory !== "undefined") {
                        try {
                            prodSubcategory = JSON.parse(prodSubcategory);
                        } catch (e) {}
                        if (typeof prodSubcategory === "object") {
                            // normalize category names
                            prodSubcategory = prodSubcategory.map(sto.utils.fn.cleanString);
                            if (prodSubcategory.indexOf(sto.utils.fn.cleanString(v)) !== -1) {
                                productLength++;
                            } else {
                                if (typeof settings.subcategories_aliases[sto.utils.fn.cleanString(v)] !== "undefined") {
                                    $.each(settings.subcategories_aliases[sto.utils.fn.cleanString(v)], function(ii, vv) {
                                        if (prodSubcategory.indexOf(sto.utils.fn.cleanString(vv)) !== -1) {
                                            productLength++;
                                        }
                                    });
                                }
                            }
                        } else {
                            if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(v)) {
                                productLength++;
                            } else {
                                if (typeof settings.subcategories_aliases[sto.utils.fn.cleanString(v)] !== "undefined") {
                                    $.each(settings.subcategories_aliases[sto.utils.fn.cleanString(v)], function(ii, vv) {
                                        if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(vv)) {
                                            productLength++;
                                        }
                                    });
                                }
                            }
                        }
                    }
                });
                if (productLength > 0)
                    $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-list').append(subcatHtml);
            });
        } else {
            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-current .sto-current-subcategory-filter').text("Toutes les catégories");
            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-current').removeClass('active');
            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-list').empty();
            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').hide();
            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-list').append('<div class="sto-category-page-filter-universe active" data-universe="all">Toutes les catégories</div>');
            $.each(settings.all_products_categories, function(idx, val) {
                var productLength = 0;
                var subcatHtml = $('<div class="sto-category-page-filter-universe" data-universe="' + sto.utils.fn.cleanString(val) + '">' + idx + '</div>');
                var productLength = $('#sto-prod-storage-' + val + ' .sto-product-container>div').length;
                if (productLength > 0)
                    $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-list').append(subcatHtml);
            });
        }

        container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-category-page-filter-subcategory').off().on('click', function() {
            var category = $(this).parents('.sto-' + placeholder + '-category-page').attr('category');
            var contentSubcategory = $(this).text();
            subcategory = $(this).data('subcategory');
            $('.sto-' + placeholder + '-category-page').attr('subcategory', subcategory);

            $(this).parents('.sto-subcategory-filter-list').find('.active').removeClass('active');
            $(this).addClass('active');

            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').find('.sto-current-subcategory-filter').text(contentSubcategory);

            if ($('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').is(':visible')) {
                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').hide();
                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').removeClass('active');
            } else {
                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').show();
                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').addClass('active');
            }

            $('.sto-' + placeholder + '-category-page .sto-category-page-products').empty();
            var paginateArray = sort_products(category);
            if (category === "bijoux") {
                if (paginateArray.length > paginationBijoux) {
                    paginate_category(paginateArray, container, tracker);
                } else {
                    unpaginate_category(paginateArray);
                }
            } else {
                if (paginateArray.length > paginationLimit) {
                    paginate_category(paginateArray, container, tracker);
                } else {
                    unpaginate_category(paginateArray);
                }
            }
        });

        container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-category-page-filter-universe').off().on('click', function() {
            var category = $(this).data('universe');
            var contentSubcategory = $(this).text();
            $('.sto-' + placeholder + '-category-page').attr('universe', category);
            //universe = $(this).data('universe');

            $(this).parents('.sto-subcategory-filter-list').find('.active').removeClass('active');
            $(this).addClass('active');

            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').find('.sto-current-subcategory-filter').text(contentSubcategory);

            if ($('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').is(':visible')) {
                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').hide();
                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').removeClass('active');
            } else {
                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').show();
                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').addClass('active');
            }

            $('.sto-' + placeholder + '-category-page .sto-category-page-products').empty();
            var paginateArray = sort_products(category);
            if (category === "bijoux") {
                if (paginateArray.length > paginationBijoux) {
                    paginate_category(paginateArray, container, tracker);
                } else {
                    unpaginate_category(paginateArray);
                }
            } else {
                if (paginateArray.length > paginationLimit) {
                    paginate_category(paginateArray, container, tracker);
                } else {
                    unpaginate_category(paginateArray);
                }
            }
        });
    }

    function sort_products(category) {
        var sortedArray = [];

        if (category !== "all") {
            $.each($('#sto-prod-storage-' + category + ' .sto-product-container .box-item.box-item-rayon'), function(i, v) {
                var prodBox = $(this);
                if (typeof prodBox.data('price') === "undefined")
                    prodBox.attr('data-price', (typeof prodBox.parent().data('price') !== "undefined") ? prodBox.parent().data('price') : "");
                if (typeof prodBox.data('id') === "undefined")
                    prodBox.attr('data-id', (typeof prodBox.parent().data('id') !== "undefined") ? prodBox.parent().data('id') : "");
                if (typeof prodBox.data('category-universe') === "undefined")
                    prodBox.attr('data-category-universe', (typeof prodBox.parent().data('category-universe') !== "undefined") ? prodBox.parent().data('category-universe') : "");
                if (typeof prodBox.data('category-1') === "undefined")
                    prodBox.attr('data-category-1', (typeof prodBox.parent().data('category-1') !== "undefined") ? prodBox.parent().data('category-1') : "");
                if (subcategory !== null && subcategory !== "all") {
                    if (prodBox.parent().length && prodBox.parent().data('category-1')) {
                        var prodSubcategory = prodBox.parent().data('category-1');
                        try {
                            prodSubcategory = JSON.parse(prodSubcategory);
                        } catch (e) {}
                        if (typeof prodSubcategory === "object") {
                            // normalize category names
                            prodSubcategory = prodSubcategory.map(sto.utils.fn.cleanString);
                            if (prodSubcategory.indexOf(sto.utils.fn.cleanString(subcategory)) !== -1) {
                                sortedArray.push(prodBox.clone(true));
                            } else {
                                if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
                                    $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
                                        if (prodSubcategory.indexOf(sto.utils.fn.cleanString(value)) !== -1) {
                                            sortedArray.push(prodBox.clone(true));
                                        }
                                    });
                                }
                            }
                        } else {
                            if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(subcategory)) {
                                sortedArray.push(prodBox.clone(true));
                            } else {
                                if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
                                    $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
                                        if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(value)) {
                                            sortedArray.push(prodBox.clone(true));
                                        }
                                    });
                                }
                            }
                        }
                    } else {
                        var prodSubcategory = prodBox.data('category-1');
                        try {
                            prodSubcategory = JSON.parse(prodSubcategory);
                        } catch (e) {}
                        if (typeof prodSubcategory === "object") {
                            // normalize category names
                            prodSubcategory = prodSubcategory.map(sto.utils.fn.cleanString);
                            if (prodSubcategory.indexOf(sto.utils.fn.cleanString(subcategory)) !== -1) {
                                sortedArray.push(prodBox.clone(true));
                            } else {
                                if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
                                    $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
                                        if (prodSubcategory.indexOf(sto.utils.fn.cleanString(value)) !== -1) {
                                            sortedArray.push(prodBox.clone(true));
                                        }
                                    });
                                }
                            }
                        } else {
                            if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(subcategory)) {
                                sortedArray.push(prodBox.clone(true));
                            } else {
                                if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
                                    $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
                                        if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(value)) {
                                            sortedArray.push(prodBox.clone(true));
                                        }
                                    });
                                }
                            }
                        }
                    }
                } else {
                    sortedArray.push(prodBox.clone(true));
                }
            });
        } else {
            $.each(settings.all_categories, function(idx, val) {
                $.each($('#sto-prod-storage-' + val + ' .sto-product-container .box-item.box-item-rayon'), function(i, v) {
                    var prodBox = $(this);
                    if (typeof prodBox.data('price') === "undefined")
                        prodBox.attr('data-price', (typeof prodBox.parent().data('price') !== "undefined") ? prodBox.parent().data('price') : "");
                    if (typeof prodBox.data('id') === "undefined")
                        prodBox.attr('data-id', (typeof prodBox.parent().data('id') !== "undefined") ? prodBox.parent().data('id') : "");
                    if (typeof prodBox.data('category-universe') === "undefined")
                        prodBox.attr('data-category-universe', (typeof prodBox.parent().data('category-universe') !== "undefined") ? prodBox.parent().data('category-universe') : "");
                    if (typeof prodBox.data('category-1') === "undefined")
                        prodBox.attr('data-category-1', (typeof prodBox.parent().data('category-1') !== "undefined") ? prodBox.parent().data('category-1') : "");
                    if (subcategory !== null && subcategory !== "all") {
                        if (prodBox.parent().length && prodBox.parent().data('category-1')) {
                            var prodSubcategory = prodBox.parent().data('category-1');
                            try {
                                prodSubcategory = JSON.parse(prodSubcategory);
                            } catch (e) {}
                            if (typeof prodSubcategory === "object") {
                                // normalize category names
                                prodSubcategory = prodSubcategory.map(sto.utils.fn.cleanString);
                                if (prodSubcategory.indexOf(sto.utils.fn.cleanString(subcategory)) !== -1) {
                                    sortedArray.push(prodBox.clone(true));
                                } else {
                                    if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
                                        $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
                                            if (prodSubcategory.indexOf(sto.utils.fn.cleanString(value)) !== -1) {
                                                sortedArray.push(prodBox.clone(true));
                                            }
                                        });
                                    }
                                }
                            } else {
                                if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(subcategory)) {
                                    sortedArray.push(prodBox.clone(true));
                                } else {
                                    if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
                                        $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
                                            if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(value)) {
                                                sortedArray.push(prodBox.clone(true));
                                            }
                                        });
                                    }
                                }
                            }
                        } else {
                            var prodSubcategory = prodBox.data('category-1');
                            try {
                                prodSubcategory = JSON.parse(prodSubcategory);
                            } catch (e) {}
                            if (typeof prodSubcategory === "object") {
                                // normalize category names
                                prodSubcategory = prodSubcategory.map(sto.utils.fn.cleanString);
                                if (prodSubcategory.indexOf(sto.utils.fn.cleanString(subcategory)) !== -1) {
                                    sortedArray.push(prodBox.clone(true));
                                } else {
                                    if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
                                        $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
                                            if (prodSubcategory.indexOf(sto.utils.fn.cleanString(value)) !== -1) {
                                                sortedArray.push(prodBox.clone(true));
                                            }
                                        });
                                    }
                                }
                            } else {
                                if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(subcategory)) {
                                    sortedArray.push(prodBox.clone(true));
                                } else {
                                    if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
                                        $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
                                            if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(value)) {
                                                sortedArray.push(prodBox.clone(true));
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    } else {
                        sortedArray.push(prodBox.clone(true));
                    }
                });
            });
        }

        if (sorting == "ASC")
            sortedArray.sort(function(a, b) {
                return parseFloat($(a).data('price')) - parseFloat($(b).data('price'))
            });
        else if (sorting == "DESC")
            sortedArray.sort(function(a, b) {
                return parseFloat($(b).data('price')) - parseFloat($(a).data('price'))
            });

        return sortedArray;
    }

    function paginate_category(paginateArray, container, tracker) {
        pagination_container.empty();
        var currentCat = $('.sto-' + placeholder + '-category-page').attr('category');
        var limit = (currentCat === "bijoux") ? paginationBijoux : paginationLimit;
        var pageNumberPaginate = (pageNumber !== null) ? pageNumber : 1;
        var paginationInProgress = 0;
        pagination_container.pagination({
            dataSource: paginateArray,
            pageSize: limit,
            pageNumber: pageNumberPaginate,
            showPageNumbers: false,
            showNavigator: true,
            callback: function(data, pagination) {
                pageNumber = pagination.pageNumber;
                if (windowSize > 1023) {
                    if (paginationInProgress !== 1) {
                        if ($(window).scrollTop() > $('.sto-' + placeholder + '-category-page .sto-category-page-products').offset().top) {
                            $(window).unbind('scroll');
                            $(window).scrollTop(0);
                        }
                        data = place_banners_paginatearray(data, pagination.pageNumber, pagination.totalNumber);
                        $('.sto-' + placeholder + '-category-page > .sto-category-page-products').empty();
                        $.each(data, function(i, v) {
                            if (typeof v.data('id') !== "undefined") {
                                if ($('.sto-product-container').find('div[data-id="' + v.data('id') + '"]').find('.box-item').length > 0) {
                                    var elemToAppend = $('.sto-product-container').find('div[data-id="' + v.data('id') + '"] .box-item').first().clone(true);
                                } else {
                                    var elemToAppend = $('.sto-product-container').find('div[data-id="' + v.data('id') + '"]').first().clone(true);
                                }
                                if (v.hasClass('eol')) elemToAppend.addClass('eol');
                                $('.sto-' + placeholder + '-category-page > .sto-category-page-products').append(elemToAppend);
                            } else {
                                $('.sto-' + placeholder + '-category-page > .sto-category-page-products').append(v)
                            }
                        });
                    }
                    paginationInProgress++;
                } else {
                    data = place_banners_paginatearray(data, pagination.pageNumber, pagination.totalNumber);
                    $('.sto-' + placeholder + '-category-page > .sto-category-page-products').empty();
                    $.each(data, function(i, v) {
                        if (typeof v.data('id') !== "undefined") {
                            if ($('.sto-product-container').find('div[data-id="' + v.data('id') + '"]').find('.box-item').length > 0) {
                                var elemToAppend = $('.sto-product-container').find('div[data-id="' + v.data('id') + '"] .box-item').first().clone(true);
                            } else {
                                var elemToAppend = $('.sto-product-container').find('div[data-id="' + v.data('id') + '"]').first().clone(true);
                            }
                            if (v.hasClass('eol')) elemToAppend.addClass('eol');
                            $('.sto-' + placeholder + '-category-page > .sto-category-page-products').append(elemToAppend);
                        } else {
                            $('.sto-' + placeholder + '-category-page > .sto-category-page-products').append(v)
                        }
                    });
                }
            },
            afterRender: function() {
                $('.paginationjs-prev.J-paginationjs-previous').blur();
                $('.paginationjs-prev.J-paginationjs-previous>a').blur();
                $('.paginationjs-next.J-paginationjs-next').blur();
                $('.paginationjs-next.J-paginationjs-next>a').blur();
            }
        });
    }

    function unpaginate_category(paginateArray) {
        pagination_container.empty();
        paginateArray = place_banners_paginatearray(paginateArray);

        $('.sto-' + placeholder + '-category-page .sto-category-page-products').html(paginateArray);
    }

    function place_banners_paginatearray(paginateArray, pageNumber, totalNumber) {
        var currentCat = $('.sto-' + placeholder + '-category-page').attr('category');

        // if category not bijoux add eol class at the end of each end of line product (4th desktop, third in break, one in mobile)
        if (currentCat !== "bijoux") {
            $.each(paginateArray, function(i, v) {
                if ((i !== 0) && ((i + 1) % hpProductLimit == 0)) {
                    $(v).addClass('eol');
                }
            });
        }

        // if category culture but subcategory filter is active
        if (currentCat === "culture" && (subcategory !== "all" && typeof settings.banners[sto.utils.fn.cleanString(subcategory)] !== "undefined")) {
            if (typeof pageNumber !== "undefined") {
                paginateArray = splice_paginatearray(sto.utils.fn.cleanString(subcategory), paginateArray, pageNumber);
            } else {
                paginateArray = splice_paginatearray(sto.utils.fn.cleanString(subcategory), paginateArray);
            }
        } else if (currentCat === "bijoux") {
            if (typeof pageNumber !== "undefined") {
                paginateArray = splice_paginatearray_bijoux(currentCat, paginateArray, pageNumber, totalNumber);
            } else {
                paginateArray = splice_paginatearray_bijoux(currentCat, paginateArray);
            }
        } else {
            if (typeof settings.banners[currentCat] !== "undefined") {
                if (typeof pageNumber !== "undefined") {
                    paginateArray = splice_paginatearray(currentCat, paginateArray, pageNumber);
                } else {
                    paginateArray = splice_paginatearray(currentCat, paginateArray);
                }
            }
        }

        // if category is bijoux addClass eol at the end because of banner form
        /*if (currentCat === "bijoux") {
            $.each(paginateArray, function(i, v) {
                if ((i !== 0) && ((i + 1) % hpProductLimit == 0)) {
                    $(v).addClass('eol');
                }
            });
        }*/

        return paginateArray;
    }

    function splice_paginatearray(cat, paginateArray, pageNumber) {
        var loopIndex = (typeof pageNumber !== "undefined" && pageNumber > 1) ? ((pageNumber == 3) ? pageNumber + 1 : pageNumber) : 0;
        var spliceIndex = 0;
        var today = new Date();
        $.each(settings.banners[cat], function(idx, value) {
            if (windowSize > 1023) {
                if (typeof pageNumber !== "undefined" && pageNumber > 1 && number_available_banners(settings.banners[cat]) <= 2) {
                    return false;
                }
                if (typeof pageNumber !== "undefined" && spliceIndex >= 2) {
                    return false;
                }
                if (loopIndex >= number_available_banners(settings.banners[cat])) {
                    return false;
                }
                if (value.date !== "all") {
                    var bannerDate = new Date(value.date);
                }
                if (value.date === "all" || (today.setHours(0, 0, 0, 0) == bannerDate.setHours(0, 0, 0, 0))) {
                    var availBanners = get_available_banners(settings.banners[cat]);
                    var value = availBanners[loopIndex];
                    if (windowSize <= 1023 && typeof value.urlMobile !== "undefined") {
                        var t = $('<a href="' + value.urlMobile + '" target="_blank"><div class="sto-gamme-banner" index="' + loopIndex + '"></div></a>');
                        t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '" + cleanStringBlackFriday(value.message) + "','urlsortie': '" + cleanStringBlackFriday(value.urlMobile) + "','event': 'blackfriday_traffickingexterne_visuels'})");
                    } else {
                        var t = $('<a href="' + value.url + '" target="_blank"><div class="sto-gamme-banner" index="' + loopIndex + '"></div></a>');
                        t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '" + cleanStringBlackFriday(value.message) + "','urlsortie': '" + cleanStringBlackFriday(value.url) + "','event': 'blackfriday_traffickingexterne_visuels'})");
                    }
                    if (cat !== "bijoux") {
                        if (spliceIndex == 0) {
                            paginateArray.splice((hpProductLimit * 2), 0, t);
                        } else if (spliceIndex == 1) {
                            paginateArray.splice(((hpProductLimit * 4) + 1), 0, t);
                            //paginateArray.push(t);
                        } else if (spliceIndex == 2) {
                            paginateArray.splice(((hpProductLimit * 4) + 2), 0, t);
                            //paginateArray.push(t);
                        } else if (spliceIndex == 3) {
                            paginateArray.splice(((hpProductLimit * 4) + 3), 0, t);
                            //paginateArray.push(t);
                        }
                    } else {
                        paginateArray.splice(((hpProductLimit * 2) + loopIndex), 0, t);
                    }
                    loopIndex++;
                    spliceIndex++;
                }
            } else {
                if (typeof pageNumber !== "undefined" && pageNumber > 1) {
                    return false;
                }
                if (value.date !== "all") {
                    var bannerDate = new Date(value.date);
                }
                if (value.date === "all" || (today.setHours(0, 0, 0, 0) == bannerDate.setHours(0, 0, 0, 0))) {
                    var availBanners = get_available_banners(settings.banners[cat]);
                    var value = availBanners[loopIndex];
                    if (typeof value !== "undefined") {
                        if (windowSize <= 1023 && typeof value.urlMobile !== "undefined") {
                            var t = $('<a href="' + value.urlMobile + '" target="_blank"><div class="sto-gamme-banner" index="' + loopIndex + '"></div></a>');
                            t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '" + cleanStringBlackFriday(value.message) + "','urlsortie': '" + cleanStringBlackFriday(value.urlMobile) + "','event': 'blackfriday_traffickingexterne_visuels'})");
                        } else {
                            var t = $('<a href="' + value.url + '" target="_blank"><div class="sto-gamme-banner" index="' + loopIndex + '"></div></a>');
                            t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '" + cleanStringBlackFriday(value.message) + "','urlsortie': '" + cleanStringBlackFriday(value.url) + "','event': 'blackfriday_traffickingexterne_visuels'})");
                        }

                        if (loopIndex === 0) {
                            paginateArray.splice(2, 0, t);
                        } else {
                            paginateArray.splice(((loopIndex * 3) + 2), 0, t);
                        }
                    }

                    loopIndex++;
                    spliceIndex++;
                }
            }
        });

        return paginateArray;
    }

    function splice_paginatearray_bijoux(cat, paginateArray, pageNumber, totalNumber) {
        var loopIndex = 0;
        var spliceIndex = 0;
        var today = new Date();
        /*if (windowSize > 1023) {*/
        if (typeof pageNumber === "undefined" || (typeof pageNumber !== "undefined" && pageNumber == 1)) {
            $.each(settings.banners[cat], function(idx, value) {
                if (value.date !== "all") {
                    var bannerDate = new Date(value.date);
                }
                if (value.date === "all" || (today.setHours(0, 0, 0, 0) == bannerDate.setHours(0, 0, 0, 0))) {
                    if (windowSize <= 1023 && typeof value.urlMobile !== "undefined") {
                        var t = $('<a href="' + value.urlMobile + '" target="_blank"><div class="sto-gamme-banner" index="' + loopIndex + '"></div></a>');
                        t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '" + cleanStringBlackFriday(value.message) + "','urlsortie': '" + cleanStringBlackFriday(value.urlMobile) + "','event': 'blackfriday_traffickingexterne_visuels'})");
                    } else {
                        var t = $('<a href="' + value.url + '" target="_blank"><div class="sto-gamme-banner" index="' + loopIndex + '"></div></a>');
                        t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '" + cleanStringBlackFriday(value.message) + "','urlsortie': '" + cleanStringBlackFriday(value.url) + "','event': 'blackfriday_traffickingexterne_visuels'})");
                    }
                    paginateArray.splice(((hpProductLimit * 2) + loopIndex), 0, t);

                    loopIndex++;
                    spliceIndex++;
                }
            });
        }
        /*} else {
            if (typeof pageNumber !== "undefined") {
                loopIndex = pageNumber-1;
            }
            $.each(settings.banners[cat], function(idx, value) {
                if (typeof pageNumber !== "undefined" && (pageNumber*paginationBijoux <= totalNumber) && spliceIndex > 0) { return false; }
                if (loopIndex >= settings.banners[cat].length) { return false; }
                if (value.date !== "all") { var bannerDate = new Date(value.date); }
                if (value.date === "all" || (today.setHours(0,0,0,0) == bannerDate.setHours(0,0,0,0))) {
                    if (windowSize <= 1023 && typeof value.urlMobile !== "undefined") {
                        var t = $('<a href="'+value.urlMobile+'" target="_blank"><div class="sto-gamme-banner" index="'+loopIndex+'"></div></a>');
                        t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '"+cleanStringBlackFriday(value.message)+"','urlsortie': '"+cleanStringBlackFriday(value.urlMobile)+"','event': 'blackfriday_traffickingexterne_visuels'})");
                    } else {
                        var t = $('<a href="'+value.url+'" target="_blank"><div class="sto-gamme-banner" index="'+loopIndex+'"></div></a>');
                        t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '"+cleanStringBlackFriday(value.message)+"','urlsortie': '"+cleanStringBlackFriday(value.url)+"','event': 'blackfriday_traffickingexterne_visuels'})");
                    }
                    if (spliceIndex == 0) {
                        paginateArray.splice((hpProductLimit * 2),0,t);
                    } else if (spliceIndex == 1) {
                        paginateArray.splice(((hpProductLimit * 4) + 1),0,t);
                        //paginateArray.push(t);
                    } else {
                        paginateArray.push(t);
                    }

                    loopIndex++;
                    spliceIndex++;
                }
            });
        }*/

        return paginateArray;
    }

    function number_available_banners(banners) {
        var nb = 0;
        var today = new Date();
        $.each(banners, function(idx, value) {
            if (value.date !== "all") {
                var bannerDate = new Date(value.date);
            }
            if (value.date === "all" || (today.setHours(0, 0, 0, 0) == bannerDate.setHours(0, 0, 0, 0))) {
                nb++;
            }
        });
        return nb;
    }

    function get_available_banners(banners) {
        var newBanners = [];
        var today = new Date();
        $.each(banners, function(idx, value) {
            if (value.date !== "all") {
                var bannerDate = new Date(value.date);
            }
            if (value.date === "all" || (today.setHours(0, 0, 0, 0) == bannerDate.setHours(0, 0, 0, 0))) {
                newBanners.push(value);
            }
        });
        return newBanners;
    }

    function resize_gutters() {
        $('.sto-' + placeholder + '-gutter>div, .sto-' + placeholder + '-gutter>div>.sto-' + placeholder + '-gutter-top').css({
            'height': $('.row[layout\\:fragment="pagecontent"]').outerHeight() + 'px'
        });
        $('.sto-' + placeholder + '-gutter>div').css({
            'top': $('.sto-' + placeholder + '-global').offset().top + 'px'
        });
    }

    function tracking_products_portal(elem) {
        elem.find('a:not(.btn)').attr('target', "_blank");
        if (elem.find('.btn.btn-achat-ligne').length)
            elem.find('.btn.btn-achat-ligne').attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","urlsortie": "' + cleanStringBlackFriday(elem.data('trackurl')) + '","product":{ "id": "' + elem.data('trackid') + '","name": "' + cleanStringBlackFriday(elem.data('trackname')) + '","price": "' + elem.data("trackprice") + '","promo": "blackfriday2019","category": "' + cleanStringBlackFriday(elem.data('trackcategory')) + '","subcategory": "' + cleanStringBlackFriday(elem.data('tracksubcategory')) + '"}, "event":"blackfriday_traffickingexterne_acheter"}); window.open("' + elem.data('trackurl') + '")').removeAttr('target');
        if (elem.find('.btn.btn-memo').length)
            elem.find('.btn.btn-memo').attr('onclick', 'dataLayer.push({"product":{ "id": "' + elem.data('trackid') + '","name": "' + cleanStringBlackFriday(elem.data('trackname')) + '","price": "' + elem.data('trackprice') + '","promo": "blackfriday2019","category": "' + cleanStringBlackFriday(elem.data('trackcategory')) + '","subcategory": "' + cleanStringBlackFriday(elem.data('tracksubcategory')) + '", "quantity": "1"}});').removeAttr('target');
    }

    function tracking_products_api(elem) {
        var urlSortie = elem.find('.btn.btn-achat-ligne').attr('href'),
            productId = elem.data('id'),
            productName = elem.find('.box-item-title>a').text(),
            productPrice = elem.data('price'),
            productCategory = elem.data('category-universe'),
            productSubcategory = elem.data('category-1');

        if (typeof productSubcategory === "object") {
            productSubcategory = JSON.stringify(productSubcategory);
        }

        if (productPrice && "" !== productPrice) {
            productPrice = productPrice.toString();
            var g = productPrice.slice(-2),
                y = productPrice.slice(0, -2);
            productPrice = y + "-" + g
        }

        elem.find('a:not(.btn.btn-achat-ligne)').attr('target', "_blank");
        if (elem.find('.btn.btn-achat-ligne').length)
            elem.find('.btn.btn-achat-ligne').attr('href', '#').attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","urlsortie": "' + cleanStringBlackFriday(urlSortie) + '","product":{ "id": "' + productId + '","name": "' + cleanStringBlackFriday(productName) + '","price": "' + productPrice + '","promo": "blackfriday2019","category": "' + cleanStringBlackFriday(productCategory) + '","subcategory": "' + cleanStringBlackFriday(productSubcategory) + '"}, "event":"blackfriday_traffickingexterne_acheter"}); window.open("' + urlSortie + '")').removeAttr('target');
    }

    function fetch_backup_products() {
        var hightech1 = $('#sto-prod-storage-hightech .sto-product-container').find('.ng-scope>.box-item').first().clone(true).attr('slider-nav', $('#sto-prod-storage-hightech .sto-product-container').find('.ng-scope>.box-item').first().find('.box-item-title').text());
        if (hightech1.attr('slider-nav').length >= 30) {
            hightech1.attr('slider-nav', hightech1.attr('slider-nav').slice(0, 30) + "...")
        }
        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(hightech1);
        var culture1 = $('#sto-prod-storage-culture .sto-product-container').find('.ng-scope>.box-item').first().clone(true).attr('slider-nav', $('#sto-prod-storage-culture .sto-product-container').find('.ng-scope>.box-item').first().find('.box-item-title').text());
        if (culture1.attr('slider-nav').length >= 30) {
            culture1.attr('slider-nav', culture1.attr('slider-nav').slice(0, 30) + "...")
        }
        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(culture1);
        var maison1 = $('#sto-prod-storage-maison .sto-product-container').find('.ng-scope>.box-item').first().clone(true).attr('slider-nav', $('#sto-prod-storage-maison .sto-product-container').find('.ng-scope>.box-item').first().find('.box-item-title').text());
        if (maison1.attr('slider-nav').length >= 30) {
            maison1.attr('slider-nav', maison1.attr('slider-nav').slice(0, 30) + "...")
        }
        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(maison1);
        var parapharmacie1 = $('#sto-prod-storage-parapharmacie .sto-product-container').find('.ng-scope>.box-item').first().clone(true).attr('slider-nav', $('#sto-prod-storage-parapharmacie .sto-product-container').find('.ng-scope>.box-item').first().find('.box-item-title').text());
        if (parapharmacie1.attr('slider-nav').length >= 30) {
            parapharmacie1.attr('slider-nav', parapharmacie1.attr('slider-nav').slice(0, 30) + "...")
        }
        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(parapharmacie1);
    }
} catch (e) {
    sto.tracker().sendHit({
        "ta": "err",
        "te": "onBuild-environnement",
        "tl": e
    });
}
