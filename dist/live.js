/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "//rscdn.storetail.net/lcl-portal/black_friday_2019/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	/*jslint browser:true, nomen:true*/
	"use strict";
	    var formatjs = __webpack_require__(200);
	    formatjs._init_;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "1df8a7fd9d302cf17d4af17d3522e451.png";

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "57874135e0ceb9caffca18ea96c2e75f.jpg";

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "021d723c2ce8514024bfe832339111c6.jpg";

/***/ }),
/* 4 */
/***/ (function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "de70545838612b64dd183b700bfdd473.png";

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "6f81c14da68da270e23301ab7ae77c58.png";

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "db5599432c3680ad286bf3bce1ead135.jpg";

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "a86324c9b4d47166239d2d563edb0689.jpg";

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "afc8392f4b736c77b8a4617381bca55b.png";

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "281aa469f7a8cafa2f9d24b09ab33747.png";

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "3d630422045b5dd8ae7c8ff39e45ca11.png";

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "c83339cb2a8ff7f223cb5b073ce38eb4.png";

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "1eab8dffb3d26fcdc253eb5594e92270.png";

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "056af934101755cba40b7ba93beda39b.png";

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "7c2a346e550619c563bb38fc633ba83d.png";

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "9232f35d312ba709af2fb7765b0a431c.png";

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "7be5df12b8a21593a86620d19b6758f3.jpg";

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "c65532cf41bb6046752b26e665af7be0.png";

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "f108c313269d31b2a77128c8c9e09724.png";

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "b537d529544c64de440f7f8bf4bd3ce8.png";

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "055c3399cfc5421e1960d99dcabfe398.jpg";

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "fdf90f7b0c4765f27ee727db5fb6abb3.jpg";

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "a89843c3cce4ce37bb52b037e9450ac2.png";

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "/Univers-Condensed-Medium.eot";

/***/ }),
/* 25 */
/***/ (function(module, exports) {

	module.exports = "<div><div class=sto-product-container></div></div>";

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(4)();
	// imports
	
	
	// module
	exports.push([module.id, ".container.header-container{padding-bottom:0}.page-container>.main-content{background-color:#fff}.sto-black-friday-gutter>div{position:absolute;bottom:0;top:0;height:2551px;overflow:hidden}.sto-black-friday-gutter>div:after{display:block;top:0;bottom:0;position:absolute;left:0;right:0;z-index:20;content:\"\"}.sto-black-friday-gutter-left:after{box-shadow:inset -18px 0 30px -20px rgba(50,50,50,.4)}.sto-black-friday-gutter-right:after{box-shadow:inset 8px 0 30px -10px rgba(50,50,50,.4)}.sto-black-friday-gutter>div>div{position:absolute;width:calc(100% + 1px);background-repeat:no-repeat;background-position:0 0}.sto-black-friday-gutter>div>.sto-black-friday-gutter-top{top:0;height:2551px;display:block}.sto-black-friday-gutter-left>div{right:0}.sto-black-friday-gutter_right>div{left:0}.sto-black-friday-gutter-left{left:0;right:622px;margin-right:50%;background-color:#fff}.sto-black-friday-gutter-right{left:622px;margin-left:50%;right:0;background-color:#fff}.sto-black-friday-gutter-left>.sto-black-friday-gutter-top{background-image:url(" + __webpack_require__(128) + ");background-position:100% 0}.sto-black-friday-gutter-right>.sto-black-friday-gutter-top{background-image:url(" + __webpack_require__(129) + ");background-position:-1px 0}@media screen and (min-width:2070px){.sto-black-friday-gutter-left>.sto-black-friday-gutter-top{background-position:100% 0}.sto-black-friday-gutter-right>.sto-black-friday-gutter-top{background-position:0 0}}", ""]);
	
	// exports


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(4)();
	// imports
	
	
	// module
	exports.push([module.id, "@font-face{font-family:Univers Condensed;src:url(" + __webpack_require__(24) + ");src:url(" + __webpack_require__(24) + "?#iefix) format(\"embedded-opentype\"),url(" + __webpack_require__(187) + ") format(\"woff2\"),url(" + __webpack_require__(186) + ") format(\"woff\"),url(" + __webpack_require__(185) + ") format(\"truetype\"),url(" + __webpack_require__(184) + "#Univers-Condensed-Medium) format(\"svg\");font-weight:500;font-style:normal}#sto-mention-legal{color:#222;padding:0 10%;font-size:9px;text-align:center;margin-bottom:40px;margin-top:40px}.sto-clearfix:after{content:\".\";height:0;line-height:0;display:block;visibility:hidden;clear:both}.sto-black-friday-global{width:100%;background-color:#fff}#sto-prod-storage{display:none!important}.sto-black-friday-loader{width:441px;height:291px;margin:0 auto;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(134) + ")}.sto-black-friday-header-wrapper .sto-black-friday-loader{position:absolute;z-index:2000;left:585px;top:77px}.sto-black-friday-nuit-blanche[bg=\"0\"]{display:none}#link-nuitblanche{pointer-events:none!important}.sto-black-friday-nuit-blanche[bg=\"1\"]{width:1244px;height:120px;margin:0 auto;background-image:url(" + __webpack_require__(55) + ");background-repeat:no-repeat;background-position:50%;pointer-events:none!important}.sto-black-friday-header-wrapper{width:100%;position:relative;background-repeat:no-repeat;background-position:50%;background-color:#de813b;max-width:1244px;margin:0 auto}.sto-black-friday-header-wrapper:after{content:\" \";position:absolute;bottom:0;width:100%;height:40px;display:block;z-index:0}.sto-black-friday-header{width:100%;max-width:1244px;margin:0 auto;background-repeat:no-repeat;background-position:50%;height:510px;position:relative;z-index:1;border-bottom:10px solid #000}.sto-black-friday-header *{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;-o-user-select:none;user-select:none}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header,.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header,.sto-black-friday-global[data-size=\"3\"] .sto-black-friday-header{background-size:cover}.sto-black-friday-header-slider-mob-next,.sto-black-friday-header-slider-mob-prev,.sto-black-friday-header-slider-next,.sto-black-friday-header-slider-prev{width:44px;height:50px;position:absolute;top:50%;margin-top:-15px;cursor:pointer;z-index:1;background-image:url(" + __webpack_require__(33) + ")}.sto-black-friday-header-slider-mob-next,.sto-black-friday-header-slider-mob-prev{display:none}.sto-black-friday-header-slider-mob-next:hover,.sto-black-friday-header-slider-mob-prev:hover,.sto-black-friday-header-slider-next:hover,.sto-black-friday-header-slider-prev:hover{background-image:url(" + __webpack_require__(7) + ")}.sto-black-friday-header-slider-mob-prev,.sto-black-friday-header-slider-prev{left:10px}.sto-black-friday-header-slider-mob-next,.sto-black-friday-header-slider-next{right:10px;transform:rotate(180deg)}.sto-black-friday-header-slider-nav{width:100%;font-size:0;position:absolute;bottom:0;z-index:1;border-top:1px solid #fff}.sto-black-friday-header-slider-nav-btn{display:inline-block;font-size:18px;font-family:Univers Condensed,Roboto Condensed,sans-serif;color:#000;width:25%;height:40px;line-height:40px;background-color:rgba(222,129,59,.5);cursor:pointer;text-align:center;text-transform:uppercase;position:relative;font-weight:700}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider-nav-btn{height:30px;line-height:30px}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider-nav-btn[data-ind=\"1\"]:after,.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider-nav-btn[data-ind=\"2\"]:after,.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider-nav-btn[data-ind=\"3\"]:after{height:20px}.sto-black-friday-header-slider-nav-btn[data-ind=\"1\"]:after,.sto-black-friday-header-slider-nav-btn[data-ind=\"2\"]:after,.sto-black-friday-header-slider-nav-btn[data-ind=\"3\"]:after{content:\" \";position:absolute;bottom:5px;right:-1px;width:2px;height:30px;background-color:#fff;display:block}.sto-black-friday-header-slider-nav-btn.sto-header-slide-active,.sto-black-friday-header-slider-nav-btn:hover{background-color:#ffa74f}.sto-black-friday-header-slider{width:100%;height:100%;overflow:hidden;position:absolute;left:0;top:0}.sto-black-friday-header-slide>div:not(.box-item-banner){width:47.7%;height:62.5%;overflow:hidden;position:absolute;left:41.1%;top:13.5%}.sto-black-friday-header-slides-wrapper{width:400%;height:100%;font-size:0;position:absolute}.sto-black-friday-header-slide{width:25%;height:100%;display:inline-block;background-position:50%;background-repeat:no-repeat;background-size:cover;vertical-align:top;overflow:hidden;position:relative;background-image:url(" + __webpack_require__(37) + ")}.sto-black-friday-global[date=\"2019-11-25\"] .sto-black-friday-header-slide[data-bg=\"2\"]{height:460px;background-size:100% 100%;cursor:pointer;background-image:url(" + __webpack_require__(173) + ")}.sto-black-friday-global[date=\"2019-11-25\"] .sto-black-friday-header-slide[data-bg=\"3\"]{height:460px;background-size:100% 100%;cursor:pointer;background-image:url(" + __webpack_require__(22) + ")}.sto-black-friday-global[date=\"2019-11-26\"] .sto-black-friday-header-slide[data-bg=\"2\"]{height:460px;background-size:100% 100%;cursor:pointer;background-image:url(" + __webpack_require__(169) + ")}.sto-black-friday-global[date=\"2019-11-26\"] .sto-black-friday-header-slide[data-bg=\"5\"]{height:460px;background-size:100% 100%;cursor:pointer;background-image:url(" + __webpack_require__(22) + ")}.sto-black-friday-global[date=\"2019-11-27\"] .sto-black-friday-header-slide[data-bg=\"1\"]{height:460px;background-size:100% 100%;cursor:pointer;background-image:url(" + __webpack_require__(171) + ")}.sto-black-friday-global[date=\"2019-11-28\"] .sto-black-friday-header-slide[data-bg=\"3\"]{height:460px;background-size:100% 100%;cursor:pointer;background-image:url(" + __webpack_require__(3) + ")}.sto-black-friday-global[date=\"2019-11-29\"] .sto-black-friday-header-slide[data-bg=\"1\"]{height:460px;background-size:100% 100%;cursor:pointer;background-image:url(" + __webpack_require__(175) + ")}.sto-black-friday-global[date=\"2019-11-29\"] .sto-black-friday-header-slide[data-bg=\"2\"]{height:460px;background-size:100% 100%;cursor:pointer;background-image:url(" + __webpack_require__(3) + ")}.sto-black-friday-global[date=\"2019-11-30\"] .sto-black-friday-header-slide[data-bg=\"6\"]{height:460px;background-size:100% 100%;cursor:pointer;background-image:url(" + __webpack_require__(3) + ")}.sto-black-friday-global[date=\"2019-12-1\"] .sto-black-friday-header-slide[data-bg=\"1\"]{height:460px;background-size:100% 100%;cursor:pointer;background-image:url(" + __webpack_require__(177) + ")}.sto-black-friday-global[date=\"2019-12-2\"] .sto-black-friday-header-slide[data-bg=\"2\"]{height:460px;background-size:100% 100%;cursor:pointer;background-image:url(" + __webpack_require__(179) + ")}.sto-black-friday-header-slide>div{height:100%;width:100%;display:inline-block;margin:0 auto;border:0;background-size:contain;position:relative;background-color:transparent}.sto-black-friday-header-slide .box-item.box-item-rayon{background-color:transparent}.sto-black-friday-header-slide>div>.box-item.box-item-rayon{height:100%;width:100%;border:none}.sto-black-friday-header-slide>div>.box-item.box-item-rayon.sto-exclu-web:after{display:none}.sto-black-friday-header-slide>div .box-item-top{height:100%}.sto-black-friday-header-slide>div .box-item-top .item-illustration{position:absolute;left:0;width:50%;top:50%;transform:translateY(-50%)}.sto-black-friday-header-slide>div .box-item-top .item-illustration img{padding-top:0}.sto-black-friday-header-slide>div .box-item-top .pricer{position:absolute;right:5%;bottom:60%;top:auto;width:50%}.sto-black-friday-header-slide>div .box-item-title{max-height:none}.sto-black-friday-header-slide>div .content{position:absolute;top:calc(20% + 92px);right:5%;width:50%;left:auto;transform:none;padding:0}.sto-black-friday-header-slide>div:after{display:none}.sto-black-friday-header-slide>div .box-item-title,.sto-black-friday-header-slide>div .sto-btn-jachete{position:relative;top:-20px}.sto-black-friday-global[data-size=\"3\"] .sto-black-friday-header-slide>div{transform:scale(.8) translateY(-30px)}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slide>div{transform:scale(.7) translateY(-44px)}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slide>div .sto-promo-img img{width:76px;height:67px}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slide>div .sto-promo-img{height:30px;width:40px}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slide>div .content{padding:0}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slide>div .sto-scratched-price{margin:0}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slide>div .sto-btn-jachete{margin-top:30px}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slide>div .box-item-title{position:relative;top:0}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slide>div{height:auto;max-height:100%;position:relative;top:50%;transform:translateY(-50%)}.sto-black-friday-header-slide[data-banner=brandalley]{background-image:url(" + __webpack_require__(152) + ")}.sto-black-friday-header-slide[data-banner=cuba]{background-image:url(" + __webpack_require__(153) + ")}.sto-black-friday-header-slide[data-banner=coffrevideo]{background-image:url(" + __webpack_require__(154) + ")}.sto-black-friday-header-slide[data-banner=nuxe]{background-image:url(" + __webpack_require__(155) + ")}.sto-black-friday-header-slide[data-banner=bijoux]{background-image:url(" + __webpack_require__(151) + ")}.sto-black-friday-header-slide[data-banner=nike]{background-image:url(" + __webpack_require__(157) + ")}.sto-black-friday-header-slide[data-banner=photo]{background-image:url(" + __webpack_require__(156) + ")}.sto-product-container{display:none}.sto-mention-legal{text-align:center}.sto-black-friday-content-main-claim{height:70px;line-height:70px;text-align:center;font-size:22px;font-family:Univers Condensed,Roboto Condensed,sans-serif;color:#2a2a2a}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-content-wrapper.sto-nav-fixed .sto-black-friday-content-main-claim,.sto-black-friday-global[data-size=\"3\"] .sto-black-friday-content-wrapper.sto-nav-fixed .sto-black-friday-content-main-claim,.sto-black-friday-global[data-size=\"4\"] .sto-black-friday-content-wrapper.sto-nav-fixed .sto-black-friday-content-main-claim{margin-bottom:180px}.sto-black-friday-content-wrapper{max-width:1244px;margin:0 auto;background-color:#fff;overflow:hidden;text-transform:uppercase;font-weight:700;margin-top:15px}.sto-black-friday-nav{position:relative;width:100%;height:180px;background-color:#fff;box-shadow:0 0 20px -4px #c3c3c3}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-content-wrapper.sto-nav-fixed .sto-black-friday-nav,.sto-black-friday-global[data-size=\"3\"] .sto-black-friday-content-wrapper.sto-nav-fixed .sto-black-friday-nav,.sto-black-friday-global[data-size=\"4\"] .sto-black-friday-content-wrapper.sto-nav-fixed .sto-black-friday-nav{position:fixed;max-width:1244px;z-index:2;top:0}.sto-black-friday-nav-slider-next,.sto-black-friday-nav-slider-prev{width:21px;height:32px;position:absolute;top:50%;margin-top:-15px;cursor:pointer;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(6) + ")}.sto-black-friday-nav-slider-next:hover,.sto-black-friday-nav-slider-prev:hover{background-image:url(" + __webpack_require__(35) + ")}.sto-black-friday-nav-slider-prev{left:19px;opacity:.5}.sto-black-friday-nav-slider-next{right:19px;transform:rotate(180deg)}.sto-black-friday-nav-menu-mob{display:none}.sto-black-friday-nav-slider{width:100%}.sto-black-friday-nav-slider,.sto-black-friday-nav-slides-wrapper{height:100%;font-size:0;margin:0 auto;overflow:hidden;position:relative}.sto-black-friday-nav-slides-wrapper{width:80%;top:0;left:0;text-align:center;padding:27px 0}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-nav-slides-wrapper,.sto-black-friday-global[data-size=\"3\"] .sto-black-friday-nav-slides-wrapper,.sto-black-friday-global[data-size=\"4\"] .sto-black-friday-nav-slides-wrapper{display:inline-block!important}.sto-black-friday-nav-slides-wrapper>a{display:inline-block;margin-top:8px}.sto-black-friday-nav-slider-nav-btn{height:52px;width:182px;margin:0 10px;cursor:pointer;background-repeat:no-repeat;background-position:50%}.sto-black-friday-nav-slider-nav-btn[data-cat=hightech]{background-image:url(" + __webpack_require__(130) + ")}.sto-black-friday-nav-slider-nav-btn[data-cat=maison]{background-image:url(" + __webpack_require__(135) + ");width:181px}.sto-black-friday-nav-slider-nav-btn[data-cat=parapharmacie]{background-image:url(" + __webpack_require__(160) + ")}.sto-black-friday-nav-slider-nav-btn[data-cat=culture]{background-image:url(" + __webpack_require__(126) + ")}.sto-black-friday-nav-slider-nav-btn[data-cat=mode]{background-image:url(" + __webpack_require__(158) + ");width:181px}.sto-black-friday-nav-slider-nav-btn[data-cat=vin]{background-image:url(" + __webpack_require__(182) + ");width:181px}.sto-black-friday-nav-slider-nav-btn[data-cat=alimentaire]{background-image:url(" + __webpack_require__(29) + ");width:181px}.sto-black-friday-nav-slider-nav-btn[data-cat=bijoux]{background-image:url(" + __webpack_require__(122) + ")}.sto-black-friday-nav-slider-nav-btn[data-cat=all]{background-image:url(" + __webpack_require__(30) + ");width:252px}.sto-black-friday-nav-slider-nav-btn a{display:inline-block;width:100%;height:100%;background-color:rgba(0,0,0,.3)}.sto-black-friday-nav-slider-nav-btn span{display:none}.sto-black-friday-content-section{padding-top:40px}.sto-black-friday-content-section-header{width:1187px;height:38px;background-repeat:no-repeat;background-position:50%;margin:0 auto;margin-bottom:25px}.sto-black-friday-content-section[data-cat=hightech] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(15) + ")}.sto-black-friday-content-section[data-cat=maison] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(16) + ")}.sto-black-friday-content-section[data-cat=parapharmacie] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(20) + ")}.sto-black-friday-content-section[data-cat=culture] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(14) + ")}.sto-black-friday-content-section[data-cat=mode] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(19) + ")}.sto-black-friday-content-section[data-cat=vin] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(23) + ")}.sto-black-friday-content-section[data-cat=alimentaire] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(5) + ")}.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(13) + ")}.sto-black-friday-content-section[data-cat=photos] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(164) + ")}.sto-black-friday-content-section[data-cat=voyages] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(183) + ")}.sto-black-friday-content-section[data-cat=optique] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(159) + ")}.sto-black-friday-content-section[data-cat=energie] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(127) + ")}.sto-black-friday-content-section[data-cat=auto] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(180) + ")}.sto-black-friday-content-section-products{position:relative;text-align:center}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-prods-slides-wrapper,.sto-black-friday-global[data-size=\"3\"] .sto-black-friday-prods-slides-wrapper,.sto-black-friday-global[data-size=\"4\"] .sto-black-friday-prods-slides-wrapper{width:-ms-calc(100% - 40px)!important;width:-o-calc(100% - 40px)!important;width:calc(100% - 40px)!important;max-width:1195px;margin:15px auto;font-size:0;position:relative;overflow:hidden;height:365px;position:static}.sto-black-friday-content-section[data-cat=telecom] .sto-black-friday-prods-slides-wrapper{background-image:url(" + __webpack_require__(50) + ");background-repeat:no-repeat;background-position:50%;cursor:pointer}.sto-black-friday-global[data-size=\"4\"] .sto-black-friday-content-section[data-cat=telecom] .sto-black-friday-prods-slides-wrapper{max-width:calc(100% - 55px);height:480px}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-content-section[data-cat=telecom] .sto-black-friday-prods-slides-wrapper,.sto-black-friday-global[data-size=\"3\"] .sto-black-friday-content-section[data-cat=telecom] .sto-black-friday-prods-slides-wrapper{max-width:100%;height:480px;background-size:cover}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=telecom] .sto-black-friday-prods-slides-wrapper{position:static;width:100%!important;background-size:cover}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=telecom] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(1) + ")!important;background-size:auto;height:102px!important;background-position:left 0!important;margin-bottom:-45px!important}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=telecom] .sto-black-friday-content-section-products{height:auto}.sto-black-friday-prods-slider-next,.sto-black-friday-prods-slider-prev{width:20%;height:80px;position:absolute;top:40%;margin-top:-15px;cursor:pointer;z-index:1;display:none;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(6) + ");display:none!important}.sto-black-friday-prods-slider-prev{left:0}.sto-black-friday-prods-slider-next{right:0;transform:rotate(180deg)}.sto-black-friday-prods-slides-wrapper.sto-products-open{height:auto!important}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-prods-slides-wrapper>div,.sto-black-friday-global[data-size=\"3\"] .sto-black-friday-prods-slides-wrapper>div,.sto-black-friday-global[data-size=\"4\"] .sto-black-friday-prods-slides-wrapper>div{display:inline-block;width:-ms-calc(25% - 11.35px)!important;width:-o-calc(25% - 11.35px)!important;width:calc(25% - 11.35px)!important;margin:0!important;height:365px;margin-right:15px!important;margin-bottom:15px!important}.sto-black-friday-global .box-item .box-item-title a{font-size:13px}.sto-black-friday-global[data-date=\"1\"] .box-item.box-item-rayon[data-id=\"914200480\"]:after,.sto-black-friday-global[data-date=\"2\"] .box-item.box-item-rayon[data-id=\"914200480\"]:after,.sto-black-friday-global[data-date=\"3\"] .box-item.box-item-rayon[data-id=\"914200480\"]:after,.sto-black-friday-global[data-date=\"4\"] .box-item.box-item-rayon[data-id=\"914200480\"]:after,.sto-black-friday-global[data-date=test] .box-item.box-item-rayon[data-id=\"914200480\"]:after{content:\" \";width:68px;height:65px;display:inline-block;position:absolute;top:-1px;left:-1px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(161) + ")}.box-item.box-item-rayon[data-id=\"914200000\"] .btn.btn-achat-ligne,.box-item.box-item-rayon[data-id=\"914200010\"] .btn.btn-achat-ligne,.box-item.box-item-rayon[data-id=\"914200100\"] .btn.btn-achat-ligne,.box-item.box-item-rayon[data-id=\"914200200\"] .btn.btn-achat-ligne,.box-item.box-item-rayon[data-id=\"914200480\"] .btn.btn-achat-ligne{display:none}.box-item.box-item-rayon{display:inline-block!important;font-weight:700}.box-item .box-item-title{min-height:35px;height:auto}.sto-black-friday-header .box-item .box-item-title{max-height:30px;max-width:300px;margin:5px auto}.sto-black-friday-product{background-color:#fff;border-radius:3px;border:1px solid #ddd}.sto-exclu-web:after{background-image:url(" + __webpack_require__(163) + ")}.sto-en-magasin:after,.sto-exclu-web:after{content:\" \";width:68px;height:65px;display:inline-block;position:absolute;top:-1px;left:-1px;background-repeat:no-repeat;background-position:50%}.sto-en-magasin:after{background-image:url(" + __webpack_require__(162) + ")}.sto-final-price{font-size:28px;line-height:.7;margin:5px 0}.sto-black-friday-global[data-date=\"6\"] .sto-final-price,.sto-black-friday-global[data-date=\"7\"] .sto-final-price,.sto-black-friday-global[data-date=\"8\"] .sto-final-price{line-height:1}.sto-final-price span.sto-sup{font-size:.6em;vertical-align:top}.sto-scratched-price{font-size:20px;line-height:.7;margin-top:25px;text-decoration:line-through;min-height:20px;vertical-align:top;display:inline-block}.pricer[spectial-price=noscratch] .sto-scratched-price{text-decoration:none;font-size:18px}.sto-scratched-price span.sto-sup{font-size:.6em;vertical-align:top}span.sto-differed,span.twoforone{font-size:18px;line-height:.7;margin-top:25px;min-height:20px;vertical-align:top;display:inline-block;text-transform:lowercase;margin-left:5px}.sto-black-friday-global .sto-promo-img{height:60px;width:70px;background-position:100% 100%;background-size:200%;position:relative;overflow:hidden;display:inline-block}.sto-black-friday-global .sto-promo-img img{width:140px;height:120px;max-width:1000px!important;max-height:1000px!important;display:block;position:absolute;right:-1px;bottom:-1px}.sto-scratched-price[special_price=offert]{text-transform:none;text-decoration:none;font-size:16px}.sto-scratched-price[special_price=offert] .sto-sup{display:none}.sto-black-friday-global .box-item-top:before{display:none!important}.sto-black-friday-global .btn-achat-ligne{margin-left:10px;padding:8px 20px;vertical-align:top}.sto-black-friday-content-section[data-cat=hightech] .sto-black-friday-prods-slides-wrapper>div{border-color:#117dc1}.sto-black-friday-content-section[data-cat=maison] .sto-black-friday-prods-slides-wrapper>div{border-color:#0c4491}.sto-black-friday-content-section[data-cat=parapharmacie] .sto-black-friday-prods-slides-wrapper>div{border-color:#c0cb42}.sto-black-friday-content-section[data-cat=culture] .sto-black-friday-prods-slides-wrapper>div{border-color:#6c378d}.sto-black-friday-content-section[data-cat=mode] .sto-black-friday-prods-slides-wrapper>div{border-color:#f59d24}.sto-black-friday-content-section[data-cat=vin] .sto-black-friday-prods-slides-wrapper>div{border-color:#4c0a35}.sto-black-friday-content-section[data-cat=alimentaire] .sto-black-friday-prods-slides-wrapper>div{border-color:#c43682}.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-prods-slides-wrapper>div{border-color:#019e93}.sto-black-friday-prods-slides-wrapper>div{display:inline-block!important;vertical-align:top;overflow:hidden;height:370px}.sto-black-friday-prods-slides-wrapper>div .box-item.box-item-rayon{height:100%}.sto-black-friday-global[data-size=\"3\"] .sto-black-friday-prods-slides-wrapper>div:nth-child(4n),.sto-black-friday-global[data-size=\"4\"] .sto-black-friday-prods-slides-wrapper>div:nth-child(4n){margin-right:0!important}.sto-black-friday-content-section-seemore{text-align:center;display:inline-block;margin:30px 0}.sto-black-friday-content-section-seemore span{font-size:18px;font-family:Univers Condensed,Roboto Condensed,sans-serif;color:#fff;padding:10px 20px;text-transform:uppercase;text-align:center;cursor:pointer}.sectionEmpty .sto-black-friday-content-section-seemore span{background-color:gray!important;pointer-events:none!important}.sto-black-friday-content-section[data-cat=hightech] .sto-black-friday-content-section-seemore span{background-color:#117dc1}.sto-black-friday-content-section[data-cat=hightech] .sto-black-friday-content-section-seemore span:hover{background-color:#2a95d8}.sto-black-friday-content-section[data-cat=maison] .sto-black-friday-content-section-seemore span{background-color:#0c4491}.sto-black-friday-content-section[data-cat=maison] .sto-black-friday-content-section-seemore span:hover{background-color:#2258a2}.sto-black-friday-content-section[data-cat=parapharmacie] .sto-black-friday-content-section-seemore span{background-color:#c0cb42}.sto-black-friday-content-section[data-cat=parapharmacie] .sto-black-friday-content-section-seemore span:hover{background-color:#d4e04c}.sto-black-friday-content-section[data-cat=culture] .sto-black-friday-content-section-seemore span{background-color:#6c378d}.sto-black-friday-content-section[data-cat=culture] .sto-black-friday-content-section-seemore span:hover{background-color:#884bae}.sto-black-friday-content-section[data-cat=mode] .sto-black-friday-content-section-seemore span{background-color:#f59d24}.sto-black-friday-content-section[data-cat=mode] .sto-black-friday-content-section-seemore span:hover{background-color:#f7af4d}.sto-black-friday-content-section[data-cat=vin] .sto-black-friday-content-section-seemore span{background-color:#4c0a35}.sto-black-friday-content-section[data-cat=vin] .sto-black-friday-content-section-seemore span:hover{background-color:#712356}.sto-black-friday-content-section[data-cat=alimentaire] .sto-black-friday-content-section-seemore span{background-color:#c43682}.sto-black-friday-content-section[data-cat=alimentaire] .sto-black-friday-content-section-seemore span:hover{background-color:#ef70b4}.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-section-seemore span{background-color:#019e93}.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-section-seemore span:hover{background-color:#43e4d9}.sto-black-friday-content-section[data-cat=hightech] .sto-black-friday-content-banner-hp{width:1189px;height:176px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(65) + ");margin:0 auto}.sto-black-friday-content-section[data-cat=parapharmacie] .sto-black-friday-content-banner-hp{width:1189px;height:176px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(71) + ");margin:0 auto}.sto-black-friday-content-section[data-cat=maison] .sto-black-friday-content-banner-hp{width:1189px;height:176px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(69) + ");margin:0 auto}.sto-black-friday-content-section[data-cat=mode] .sto-black-friday-content-banner-hp{width:1189px;height:176px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(75) + ");margin:0 auto}.sto-black-friday-content-section[data-cat=vin] .sto-black-friday-content-banner-hp{width:1189px;height:176px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(67) + ");margin:0 auto}.sto-black-friday-content-section[data-cat=culture] .sto-black-friday-content-banner-hp{width:1189px;height:176px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(63) + ");margin:0 auto}.sto-black-friday-content-section[data-cat=voyages] .sto-black-friday-content-banner-hp{width:1189px;height:176px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(77) + ");margin:0 auto}.sto-black-friday-content-section[data-cat=photos] .sto-black-friday-content-banner-hp{width:1189px;height:176px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(73) + ");margin:0 auto}.sto-black-friday-content-section[data-cat=auto] .sto-black-friday-content-banner-hp{width:1189px;height:176px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(57) + ");margin:0 auto}.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banner-hp{width:1189px;height:176px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(59) + ");margin:0 auto;margin-bottom:20px}.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banner-hp-2{width:1189px;height:176px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(61) + ");margin:0 auto}.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banner-tile{display:inline-block;width:282px;height:370px;background-color:#fff;background-repeat:no-repeat;background-position:50%;margin-top:20px;margin-left:20px;background-size:contain;border-radius:4px;border:1px solid #e0e1e1}.sto-black-friday-content-section[data-cat=bijoux] a:first-child .sto-black-friday-content-banner-tile{background-image:url(" + __webpack_require__(9) + ");margin-left:0}.sto-black-friday-content-section[data-cat=bijoux] a:nth-child(2) .sto-black-friday-content-banner-tile{background-image:url(" + __webpack_require__(10) + ")}.sto-black-friday-content-section[data-cat=bijoux] a:nth-child(3) .sto-black-friday-content-banner-tile{background-image:url(" + __webpack_require__(11) + ")}.sto-black-friday-content-section[data-cat=bijoux] a:last-child .sto-black-friday-content-banner-tile{background-image:url(" + __webpack_require__(12) + ")}.sto-black-friday-global[date=\"2019-11-25\"] .sto-black-friday-content-section[data-cat=bijoux],.sto-black-friday-global[date=\"2019-11-25\"] .sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banners-hp,.sto-black-friday-global[date=\"2019-11-25\"] .sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banners-hp-2,.sto-black-friday-global[date=\"2019-11-26\"] .sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banners-hp-2,.sto-black-friday-global[date=\"2019-11-27\"] .sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banners-hp-2,.sto-black-friday-global[date=\"2019-11-28\"] .sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banners-hp-2,.sto-black-friday-global[date=\"2019-12-01\"] .sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banners-hp-2,.sto-black-friday-global[date=\"2019-12-02\"] .sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banners-hp-2{display:none}.sto-black-friday-content-section-extra{height:480px;width:calc(100% - 40px);margin:20px auto;background-repeat:no-repeat;background-position:50%;background-size:cover}.sto-black-friday-content-section-extra.sto-extra-empty{background-color:#ccc}#sto-section-parapharmacie .sto-black-friday-content-section-extra{background-image:url(" + __webpack_require__(45) + ")}#sto-section-culture .sto-black-friday-content-section-extra[data-version=\"1\"]{background-image:url(" + __webpack_require__(44) + ")}.sto-black-friday-global[data-date=\"8\"] #sto-section-culture .sto-black-friday-content-section-extra[data-version=\"2\"]{background-image:url(" + __webpack_require__(165) + ")}#sto-section-culture .sto-black-friday-content-section-extra{background-image:url(" + __webpack_require__(43) + ")}#sto-section-mode .sto-black-friday-content-section-extra[data-extra=\"1\"]{background-image:url(" + __webpack_require__(41) + ")}#sto-section-mode .sto-black-friday-content-section-extra[data-extra=\"2\"]{background-image:url(" + __webpack_require__(47) + ")}#sto-section-mode .sto-black-friday-content-section-extra[data-extra=\"3\"]{background-image:url(" + __webpack_require__(48) + ")}#sto-section-mode .sto-black-friday-content-section-extra[data-extra=\"4\"]{background-image:url(" + __webpack_require__(8) + ")}.sto-black-friday-global[data-date=\"1\"] #sto-section-mode .sto-black-friday-content-section-extra-cta[data-extra=\"4\"],.sto-black-friday-global[data-date=\"1\"] #sto-section-mode .sto-black-friday-content-section-extra[data-extra=\"4\"],.sto-black-friday-global[data-date=\"7\"] #sto-section-mode .sto-black-friday-content-section-extra-cta[data-name=banner_bijoux],.sto-black-friday-global[data-date=\"7\"] #sto-section-mode .sto-black-friday-content-section-extra[data-name=banner_bijoux],.sto-black-friday-global[data-date=\"7\"] #sto-section-voyage .sto-black-friday-content-section-extra-cta[data-name=voyage_cuba],.sto-black-friday-global[data-date=\"7\"] #sto-section-voyage .sto-black-friday-content-section-extra-cta[data-name=voyage_madagascar],.sto-black-friday-global[data-date=\"7\"] #sto-section-voyage .sto-black-friday-content-section-extra-cta[data-name=voyage_mexique],.sto-black-friday-global[data-date=\"7\"] #sto-section-voyage .sto-black-friday-content-section-extra[data-name=voyage_cuba],.sto-black-friday-global[data-date=\"7\"] #sto-section-voyage .sto-black-friday-content-section-extra[data-name=voyage_madagascar],.sto-black-friday-global[data-date=\"7\"] #sto-section-voyage .sto-black-friday-content-section-extra[data-name=voyage_mexique],.sto-black-friday-global[data-date=\"8\"] #sto-section-mode .sto-black-friday-content-section-extra-cta[data-name=banner_bijoux],.sto-black-friday-global[data-date=\"8\"] #sto-section-mode .sto-black-friday-content-section-extra[data-name=banner_bijoux],.sto-black-friday-global[data-date=\"8\"] #sto-section-voyage .sto-black-friday-content-section-extra-cta[data-name=voyage_cuba],.sto-black-friday-global[data-date=\"8\"] #sto-section-voyage .sto-black-friday-content-section-extra-cta[data-name=voyage_madagascar],.sto-black-friday-global[data-date=\"8\"] #sto-section-voyage .sto-black-friday-content-section-extra-cta[data-name=voyage_mexique],.sto-black-friday-global[data-date=\"8\"] #sto-section-voyage .sto-black-friday-content-section-extra[data-name=voyage_cuba],.sto-black-friday-global[data-date=\"8\"] #sto-section-voyage .sto-black-friday-content-section-extra[data-name=voyage_madagascar],.sto-black-friday-global[data-date=\"8\"] #sto-section-voyage .sto-black-friday-content-section-extra[data-name=voyage_mexique],.sto-black-friday-global[data-date=test] #sto-section-mode .sto-black-friday-content-section-extra-cta[data-extra=\"4\"],.sto-black-friday-global[data-date=test] #sto-section-mode .sto-black-friday-content-section-extra[data-extra=\"4\"]{display:none}#sto-section-mode .sto-black-friday-content-section-extra[data-extra=\"4\"]{background-image:url(" + __webpack_require__(8) + ")}#sto-section-mode .sto-black-friday-content-section-extra[data-extra=\"5\"]{background-image:url(" + __webpack_require__(49) + ")}#sto-section-voyage .sto-black-friday-content-section-extra[data-extra=\"1\"]{background-image:url(" + __webpack_require__(51) + ");margin-bottom:10px;margin-top:20px}#sto-section-voyage .sto-black-friday-content-section-extra[data-extra=\"2\"]{background-image:url(" + __webpack_require__(52) + ");margin-bottom:10px}#sto-section-voyage .sto-black-friday-content-section-extra[data-extra=\"3\"]{background-image:url(" + __webpack_require__(53) + ")}#sto-section-voyage .sto-black-friday-content-section-extra[data-extra=\"4\"]{background-image:url(" + __webpack_require__(46) + ")}#sto-back-to-top{display:none;opacity:0}#sto-back-to-top[data-gototop=active]{display:block;position:fixed;top:100px;font-size:14px;padding:16px 30px 16px 40px;background-color:rgba(0,0,0,.6);color:#fff;left:50%;font-weight:400;transform:translateX(-50%);text-transform:none;border-radius:55px;cursor:pointer}#sto-back-to-top:before{content:\"\";position:absolute;background:url(/objetsportail/images/pictos/arrow-top-white.png);width:17px;height:12px;left:15px;top:calc(50% - 6px)}.box-item .content{padding:12px 12px 10px;position:absolute;bottom:0;left:50%;transform:translateX(-50%);width:100%;height:131px}.sto-black-friday-header .box-item .content{bottom:0}.sto-black-friday-header .box-item .box-item-title{min-height:0;height:auto}.main-content{background-image:none!important}.sto-btn-jachete{width:98px;height:34px;background-repeat:no-repeat;background-position:50%;border:none;background-color:#33a211;font-size:11px;text-transform:unset;font-weight:700;color:#fff}.btn.btn-memo{margin-bottom:5px}.container.header-container{max-width:1248px;background-color:#fff;padding-bottom:20px}#return-to-top{position:fixed;bottom:20px;right:20px;height:36px;display:block;display:none;z-index:21;background-image:url(" + __webpack_require__(124) + ");background-repeat:no-repeat;width:43px;background-position:100%;cursor:pointer}#return-to-top:hover{background-image:url(" + __webpack_require__(123) + ")}.sto-black-friday-global[data-size=\"3\"] .sto-black-friday-header-slider .sto-black-friday-header-slide>div{height:107%;width:100%;display:inline-block;margin:0 auto;border:0;background-size:contain}.sto-black-friday-header-slider .sto-black-friday-header-slide .box-item .content{height:88px}.sto-black-friday-global[data-size=\"3\"] .sto-black-friday-header-slider .sto-black-friday-header-slide .box-item .content{width:121%;height:67px}.sto-black-friday-global .sto-black-friday-prods-slides-wrapper{text-align:center}.sto-black-friday-global .sto-black-friday-prods-slides-wrapper .box-item.box-item-rayon{width:282px;margin-left:20px}.sto-black-friday-global .sto-black-friday-prods-slides-wrapper .box-item.box-item-rayon:first-child{margin-left:0}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-prods-slides-wrapper>div{width:calc(33% - 9px)!important;margin-right:15px;margin-bottom:15px}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-prods-slides-wrapper>div:nth-child(4n){margin-right:15px!important}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-prods-slides-wrapper>div:nth-child(3n){margin-right:0!important}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider-nav-btn{font-size:15px}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider .sto-black-friday-header-slide>div{height:107%;width:100%;display:inline-block;margin:0 auto;border:0;background-size:contain}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider .sto-black-friday-header-slide .box-item .content{width:130%;height:57px}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-main-claim{display:none}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header{height:820px;background-position:50%;background-repeat:no-repeat;background-image:url(" + __webpack_require__(17) + ")}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-prods-slider-next,.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-prods-slider-prev{display:inline-block}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section-header{width:100%;height:215px;background-repeat:no-repeat;background-position:left 5px center;margin-left:-10px;margin-bottom:10px}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=hightech] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(139) + ")}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=maison] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(140) + ")}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=parapharmacie] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(141) + ")}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=culture] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(142) + ")}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=mode] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(143) + ")}.sto-black-friday-global[data-size=\"1\"][data-date=\"7\"] .sto-black-friday-content-section[data-cat=mode] .sto-black-friday-content-section-header,.sto-black-friday-global[data-size=\"1\"][data-date=\"8\"] .sto-black-friday-content-section[data-cat=mode] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(144) + ")}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=voyage] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(146) + ")}.sto-black-friday-global[data-size=\"1\"][data-date=\"7\"] .sto-black-friday-content-section[data-cat=voyage] .sto-black-friday-content-section-header,.sto-black-friday-global[data-size=\"1\"][data-date=\"8\"] .sto-black-friday-content-section[data-cat=voyage] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(145) + ")}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=vin] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(147) + ")}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=auto] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(181) + ")}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=telecom] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(1) + ");background-position:left 35px top;height:55px;margin-bottom:-55px;position:relative;z-index:1}.sto-black-friday-global[data-size=\"1\"] .sto-mention-legal{font-size:13px;padding:0 10px}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section-seemore{display:none}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-product{width:13%;margin:0 1.84%;display:inline-block}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-prods-slides-wrapper>div{width:45%;margin:10px 0;margin-left:3%}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slider-nav{display:none}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slider-mob-next,.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slider-mob-prev,.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slider-next,.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slider-prev{width:74px;height:85px;top:60%;background-position:50%;background-repeat:no-repeat;background-image:url(" + __webpack_require__(7) + ")}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slider-mob-prev,.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slider-prev{left:0;background-position:0}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slider-mob-next,.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slider-next{right:0;transform:rotate(180deg);background-position:0}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section-products{margin:15px auto;font-size:0;position:relative;overflow:hidden;height:auto;width:100%}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slider-next,.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slider-prev{display:none}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slider{margin:0;width:100%;text-align:center}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav{height:auto}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-menu-mob{display:block;background-color:#000;cursor:pointer;position:relative;height:60px;text-align:center;min-width:285px}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-menu-mob span{display:inline-block;font-size:20px;font-family:Univers Condensed,Roboto Condensed,sans-serif;color:#fff;text-transform:uppercase;height:40px;position:relative;padding-left:50px;margin-top:15px}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-menu-mob span:before{content:\" \";display:inline-block;position:absolute;background-image:url(" + __webpack_require__(150) + ");background-repeat:no-repeat;background-position:50%;height:60px;width:40px;left:0;background-size:60%;margin-top:-15px}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-menu-mob.sto-mob-nav-open span:before{background-image:url(" + __webpack_require__(149) + ")}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-wrapper{margin:0;padding:0}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content{margin-top:0}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slides-wrapper{position:static;width:100%!important;display:none}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slider-nav-btn{width:100%!important;display:block;margin:0;height:60px;line-height:59px;border-bottom:1px solid #ccc}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slider-nav-btn[data-cat=hightech] span{color:#117dc1}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slider-nav-btn[data-cat=maison] span{color:#0c4491}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slider-nav-btn[data-cat=parapharmacie] span{color:#c0cb42}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slider-nav-btn[data-cat=culture] span{color:#6c378d}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slider-nav-btn[data-cat=mode] span{color:#f59d24}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slider-nav-btn[data-cat=voyage] span{color:#e5c90d}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slider-nav-btn[data-cat=vin] span{color:#4c0a35}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-nav-slider-nav-btn[data-cat=telecom] span{color:#c83785}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section-extra{height:480px;width:100%;margin:0 auto;background-repeat:no-repeat;background-position:50%}.sto-black-friday-content-section-extra-cta{display:none}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section-extra-cta{display:block;text-align:center;margin-top:20px;margin-bottom:35px}.sto-black-friday-content-section>a{text-decoration:none}.sto-black-friday-content-section[data-cat=hightech] .sto-black-friday-content-section-extra-cta span{background-color:#117dc1}.sto-black-friday-content-section[data-cat=maison] .sto-black-friday-content-section-extra-cta span{background-color:#0c4491}.sto-black-friday-content-section[data-cat=parapharmacie] .sto-black-friday-content-section-extra-cta span{background-color:#c0cb42}.sto-black-friday-content-section[data-cat=culture] .sto-black-friday-content-section-extra-cta span{background-color:#6c378d}.sto-black-friday-content-section[data-cat=mode] .sto-black-friday-content-section-extra-cta span{background-color:#f59d24}.sto-black-friday-content-section[data-cat=voyage] .sto-black-friday-content-section-extra-cta span{background-color:#e5c90d}.sto-black-friday-content-section[data-cat=vin] .sto-black-friday-content-section-extra-cta span{background-color:#4c0a35}.sto-black-friday-content-section[data-cat=telecom] .sto-black-friday-content-section-extra-cta span{background-color:#c83785}@media screen and (max-width:616px){.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slide>div{transform:scale(1.23) translateY(-60%)}}@media screen and (min-width:1023px){.sto-black-friday-header-slide>div .item-illustration img{max-height:none;width:auto;height:185px}.sto-black-friday-header-slide>div .box-item-top .pricer img{max-height:110px}.sto-black-friday-header .box-item .box-item-title{max-height:none}.sto-black-friday-header-slide>div.box-item .box-item-title a{font-size:18px}}@media screen and (max-width:900px){.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider .sto-black-friday-header-slide .box-item .content{width:141%;height:35px}}@media screen and (max-width:826px){.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider .sto-black-friday-header-slide .box-item .content{width:141%;height:20px}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider .sto-black-friday-header-slide .btn-memo{font-size:9px;padding-left:0;padding-right:0}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider .btn-achat-ligne{margin-left:0}}@media screen and (max-width:729px){.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider .sto-black-friday-header-slide .box-item .content{height:10px}.sto-black-friday-global .box-item .box-item-title a{font-size:10px}.sto-black-friday-global[data-size=\"2\"] .sto-black-friday-header-slider .btn-achat-ligne{margin-left:0;padding:8px 10px}}@media screen and (max-width:500px){.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-prods-slides-wrapper>div{width:70%;margin:10px 15%}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section-header{height:150px;background-size:cover}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-content-section[data-cat=telecom] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(1) + ")!important;background-size:contain;height:70px!important;background-position:left 0!important;margin-bottom:-45px!important}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slide>div{max-height:300px;transform:scale(1) translateY(-70%)}}@media screen and (max-width:376px){.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header-slide>div{transform:scale(.8) translateY(-85%)}}.container.middle{background-color:#fff;max-width:1244px}.sto-black-friday-global[data-size=\"1\"] .sto-black-friday-header .box-item .content{bottom:-83px}.sto-black-friday-global[data-size=\"1\"][data-date=\"1\"] .sto-black-friday-header .box-item.box-item-rayon[data-id=\"914200480\"]:after,.sto-black-friday-global[data-size=\"1\"][data-date=\"2\"] .sto-black-friday-header .box-item.box-item-rayon[data-id=\"914200480\"]:after,.sto-black-friday-global[data-size=\"1\"][data-date=\"3\"] .sto-black-friday-header .box-item.box-item-rayon[data-id=\"914200480\"]:after,.sto-black-friday-global[data-size=\"1\"][data-date=\"4\"] .sto-black-friday-header .box-item.box-item-rayon[data-id=\"914200480\"]:after,.sto-black-friday-global[data-size=\"1\"][data-date=test] .sto-black-friday-header .box-item.box-item-rayon[data-id=\"914200480\"]:after{top:-1px;left:50%;transform:translateX(-150%)}.sto-black-friday-global[data-date=\"6\"] .sto-black-friday-header .box-item.box-item-rayon[data-id=\"914200480\"],.sto-black-friday-global[data-date=\"7\"] .sto-black-friday-header .box-item.box-item-rayon[data-id=\"914200480\"],.sto-black-friday-global[data-date=\"8\"] .sto-black-friday-header .box-item.box-item-rayon[data-id=\"914200480\"]{display:none!important}.sto-black-friday-category-page .sto-category-page-banner{width:100%;max-width:1244px;height:103px;background:url(" + __webpack_require__(38) + ");background-repeat:no-repeat;background-position:50%;background-size:cover;position:relative}.sto-black-friday-category-page .sto-category-page-banner .sto-category-banner-cta{background:url(" + __webpack_require__(125) + ");width:128px;height:17px;background-repeat:no-repeat;background-position:50%;background-size:cover;position:absolute;left:40px;top:50%;transform:translateY(-50%);cursor:pointer}.sto-black-friday-category-page .sto-category-banner-cta-mob{display:none}.sto-black-friday-category-page .sto-category-page-header{width:1187px;height:38px;background-repeat:no-repeat;background-position:50%;margin:0 auto;margin-top:35px;margin-bottom:25px}.sto-black-friday-category-page .sto-category-page-mentions{display:none}.sto-black-friday-category-page[category=bijoux] .sto-category-page-mentions{display:block;width:100%;max-width:1189px;margin:0 auto;color:#4c4c4c;font-size:10px}.sto-black-friday-category-page[category=bijoux] .sto-category-page-mentions span{font-weight:700;font-size:12px}.sto-black-friday-category-page[category=hightech] .sto-category-page-header{background-image:url(" + __webpack_require__(15) + ")}.sto-black-friday-category-page[category=maison] .sto-category-page-header{background-image:url(" + __webpack_require__(16) + ")}.sto-black-friday-category-page[category=parapharmacie] .sto-category-page-header{background-image:url(" + __webpack_require__(20) + ")}.sto-black-friday-category-page[category=culture] .sto-category-page-header{background-image:url(" + __webpack_require__(14) + ")}.sto-black-friday-category-page[category=mode] .sto-category-page-header{background-image:url(" + __webpack_require__(19) + ")}.sto-black-friday-category-page[category=vin] .sto-category-page-header{background-image:url(" + __webpack_require__(23) + ")}.sto-black-friday-category-page[category=alimentaire] .sto-category-page-header{background-image:url(" + __webpack_require__(5) + ")}.sto-black-friday-category-page[category=bijoux] .sto-category-page-header{background-image:url(" + __webpack_require__(13) + ")}.sto-black-friday-category-page[category=all] .sto-category-page-header{background-image:url(" + __webpack_require__(31) + ")}.sto-black-friday-category-page .sto-category-page-products{text-align:left;width:1189px;margin:0 auto}.sto-black-friday-category-page .sto-category-page-products .box-item.box-item-rayon{display:inline-block!important;font-weight:700;width:282px;margin-right:20px;border-color:#117dc1;vertical-align:top;overflow:hidden;height:370px;margin-top:20px}.sto-black-friday-category-page .sto-category-page-products div.eol{margin-right:0!important}.sto-black-friday-category-page .sto-category-page-pagination .paginationjs{display:inline-block;position:relative}.sto-black-friday-category-page .sto-category-page-pagination .paginationjs .paginationjs-prev{position:absolute;top:50%;left:0;transform:translateY(-50%)}.sto-black-friday-category-page .sto-category-page-pagination .paginationjs .paginationjs-next{position:absolute;top:50%;right:0;transform:translateY(-50%)}.sto-black-friday-category-page .sto-category-page-pagination .paginationjs .paginationjs-next,.sto-black-friday-category-page .sto-category-page-pagination .paginationjs .paginationjs-next a,.sto-black-friday-category-page .sto-category-page-pagination .paginationjs .paginationjs-prev,.sto-black-friday-category-page .sto-category-page-pagination .paginationjs .paginationjs-prev a{color:transparent;width:29px;height:29px;background-position:50%;background-repeat:no-repeat;cursor:pointer}.sto-black-friday-category-page .sto-category-page-pagination .paginationjs .paginationjs-prev{background-image:url(" + __webpack_require__(132) + ")}.sto-black-friday-category-page .sto-category-page-pagination .paginationjs .paginationjs-next{background-image:url(" + __webpack_require__(166) + ")}.sto-black-friday-category-page .sto-category-page-pagination .paginationjs .paginationjs-prev.disabled{background-image:url(" + __webpack_require__(133) + ")}.sto-black-friday-category-page .sto-category-page-pagination .paginationjs .paginationjs-next.disabled{background-image:url(" + __webpack_require__(167) + ")}.sto-black-friday-category-page .sto-category-page-pagination .paginationjs .paginationjs-nav{margin:0 34px;padding:2px;font-family:Univers Condensed,Roboto Condensed,sans-serif;font-size:18px;font-weight:700}.sto-category-page-filters .sto-price-filters,.sto-category-page-filters .sto-subcategory-filters{width:300px;height:43px;float:right}.sto-category-page-filters .sto-subcategory-filters{float:left;margin-right:15px}.sto-category-page-filters .sto-price-filters .sto-price-filter-current,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-current{border:1px solid #000;width:100%;height:auto;background:#fff}.sto-category-page-filters{position:relative;z-index:10;float:left}.sto-category-page-pagination{float:right}.sto-category-page-bottom,.sto-category-page-top{width:100%;max-width:1188px;margin:0 auto}.sto-category-page-bottom .sto-category-page-pagination{margin-top:15px;margin-bottom:30px}.sto-category-page-filters .sto-price-filters .sto-price-filter-current .sto-current-price-filter,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-current .sto-current-subcategory-filter{display:inline-block;font-family:Univers Condensed,Roboto Condensed,sans-serif;font-size:15px;line-height:22px;text-transform:uppercase;font-weight:700;padding:10px;width:100%;position:relative;cursor:pointer}.sto-category-page-filters .sto-price-filters .sto-price-filter-current .sto-current-price-filter:after,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-current .sto-current-subcategory-filter:after{content:\" \";width:15px;height:10px;display:inline-block;position:absolute;top:50%;transform:translateY(-50%);right:15px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(32) + ")}.sto-category-page-filters .sto-price-filters .sto-price-filter-current.active .sto-current-price-filter:after,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-current.active .sto-current-subcategory-filter:after{background-image:url(" + __webpack_require__(36) + ")}.sto-category-page-filters .sto-price-filters .sto-price-filter-list,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-list{display:none}.sto-category-page-filters .sto-price-filters .sto-price-filter-list .sto-category-page-filter-price,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-list .sto-category-page-filter-subcategory,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-list .sto-category-page-filter-universe{cursor:pointer}.sto-category-page-filters .sto-price-filters .sto-price-filter-list .sto-category-page-filter-price.active,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-list .sto-category-page-filter-subcategory.active,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-list .sto-category-page-filter-universe.active{font-weight:700}.sto-category-page-filters .sto-price-filters .sto-price-filter-list,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-list{background:#fff;width:100%;border-left:1px solid #000;border-right:1px solid #000;border-bottom:1px solid #000}.sto-category-page-filters .sto-price-filters .sto-price-filter-list .sto-category-page-filter-price,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-list .sto-category-page-filter-subcategory,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-list .sto-category-page-filter-universe{display:inline-block;font-family:Univers Condensed,Roboto Condensed,sans-serif;font-size:15px;text-transform:uppercase;padding:10px;position:relative;width:100%;line-height:22px;border-bottom:1px solid #cdcdcd}.sto-category-page-filters .sto-price-filters .sto-price-filter-list .sto-category-page-filter-price:last-child,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-list .sto-category-page-filter-subcategory:last-child,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-list .sto-category-page-filter-universe:last-child{border-bottom:0}.sto-black-friday-category-page .sto-gamme-banner{margin-top:20px;cursor:pointer;width:1188px;height:176px;background-repeat:no-repeat;background-position:50%}.sto-black-friday-category-page[category=hightech] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(97) + ")}.sto-black-friday-category-page[category=hightech] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(95) + ")}.sto-black-friday-category-page[category=hightech] .sto-gamme-banner[index=\"2\"]{background-image:url(" + __webpack_require__(93) + ")}.sto-black-friday-category-page[category=hightech] .sto-gamme-banner[index=\"3\"]{background-image:url(" + __webpack_require__(99) + ")}.sto-black-friday-category-page[category=maison] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(89) + ")}.sto-black-friday-category-page[category=maison] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(105) + ")}.sto-black-friday-category-page[category=maison] .sto-gamme-banner[index=\"2\"]{background-image:url(" + __webpack_require__(91) + ")}.sto-black-friday-category-page[category=maison] .sto-gamme-banner[index=\"3\"]{background-image:url(" + __webpack_require__(87) + ")}.sto-black-friday-category-page[category=parapharmacie] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(115) + ")}.sto-black-friday-category-page[category=parapharmacie] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(117) + ")}.sto-black-friday-category-page[category=parapharmacie] .sto-gamme-banner[index=\"2\"]{background-image:url(" + __webpack_require__(119) + ")}.sto-black-friday-category-page[category=parapharmacie] .sto-gamme-banner[index=\"3\"]{background-image:url(" + __webpack_require__(121) + ")}.sto-black-friday-category-page[category=mode] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(107) + ")}.sto-black-friday-category-page[category=mode] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(109) + ")}.sto-black-friday-category-page[category=mode] .sto-gamme-banner[index=\"2\"]{background-image:url(" + __webpack_require__(111) + ")}.sto-black-friday-category-page[category=mode] .sto-gamme-banner[index=\"3\"]{background-image:url(" + __webpack_require__(113) + ")}.sto-black-friday-category-page[category=vin] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(101) + ")}.sto-black-friday-category-page[category=vin] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(103) + ")}.sto-black-friday-category-page[category=culture] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(79) + ")}.sto-black-friday-category-page[category=culture] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(81) + ")}.sto-black-friday-category-page[category=culture] .sto-gamme-banner[index=\"2\"]{background-image:url(" + __webpack_require__(83) + ")}.sto-black-friday-category-page[category=culture] .sto-gamme-banner[index=\"3\"]{background-image:url(" + __webpack_require__(85) + ")}.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(9) + ")}.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"0\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"1\"]{display:inline-block;width:282px;height:370px;background-color:#fff;border-radius:4px;border:1px solid #e0e1e1;background-repeat:no-repeat;background-position:50%;background-size:contain;margin-right:20px}.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(10) + ")}.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"2\"]{background-image:url(" + __webpack_require__(11) + ");margin-right:20px}.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"2\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"3\"]{display:inline-block;width:282px;height:370px;background-color:#fff;border-radius:4px;border:1px solid #e0e1e1;background-repeat:no-repeat;background-position:50%;background-size:contain}.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"3\"]{background-image:url(" + __webpack_require__(12) + ")}.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"4\"]{display:inline-block;width:282px;height:370px;background-color:#fff;border-radius:4px;border:1px solid #e0e1e1;background-repeat:no-repeat;background-position:50%;background-size:contain;background-image:url(" + __webpack_require__(40) + ");margin-right:20px}.sto-black-friday-global[date=\"2018-11-23\"] .sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"0\"],.sto-black-friday-global[date=\"2018-11-24\"] .sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"0\"]{margin-top:20px;cursor:pointer;width:1188px;height:176px;background-repeat:no-repeat;background-position:50%;background-image:url(" + __webpack_require__(39) + ")}.sto-black-friday-category-page[category=bijoux] .sto-price-filters,.sto-black-friday-category-page[category=bijoux] .sto-subcategory-filters{display:none}@media screen and (max-width:1296px) and (min-width:1024px){.sto-black-friday-header-wrapper .sto-black-friday-loader{width:40.7%;height:51.5%;left:44.6%;top:17.5%;background-size:contain}.sto-black-friday-global[date=\"2018-11-15\"] .sto-black-friday-header-slide[data-bg=\"4\"],.sto-black-friday-global[date=\"2018-11-16\"] .sto-black-friday-header-slide[data-bg=\"4\"],.sto-black-friday-global[date=\"2018-11-16\"] .sto-black-friday-header-slide[data-bg=\"6\"],.sto-black-friday-global[date=\"2018-11-19\"] .sto-black-friday-header-slide[data-bg=\"4\"],.sto-black-friday-global[date=\"2018-11-19\"] .sto-black-friday-header-slide[data-bg=\"6\"],.sto-black-friday-global[date=\"2018-11-20\"] .sto-black-friday-header-slide[data-bg=\"2\"],.sto-black-friday-global[date=\"2018-11-21\"] .sto-black-friday-header-slide[data-bg=\"1\"],.sto-black-friday-global[date=\"2018-11-21\"] .sto-black-friday-header-slide[data-bg=\"3\"],.sto-black-friday-global[date=\"2018-11-23\"] .sto-black-friday-header-slide[data-bg=\"1\"],.sto-black-friday-global[date=\"2018-11-23\"] .sto-black-friday-header-slide[data-bg=\"6\"],.sto-black-friday-global[date=\"2018-11-23\"] .sto-black-friday-header-slide[data-bg=\"7\"],.sto-black-friday-global[date=\"2018-11-24\"] .sto-black-friday-header-slide[data-bg=\"2\"],.sto-black-friday-global[date=\"2018-11-24\"] .sto-black-friday-header-slide[data-bg=\"5\"],.sto-black-friday-global[date=\"2018-11-24\"] .sto-black-friday-header-slide[data-bg=\"7\"],.sto-black-friday-global[date=\"2018-11-25\"] .sto-black-friday-header-slide[data-bg=\"7\"],.sto-black-friday-global[date=\"2018-11-26\"] .sto-black-friday-header-slide[data-bg=\"4\"]{height:92%}.sto-black-friday-category-page[category=bijoux] .sto-category-page-mentions{width:99%}.sto-black-friday-content-section .sto-black-friday-content-section-products .sto-black-friday-prods-slides-wrapper .box-item.box-item-rayon:last-child,.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banner-tile:last-child{display:none!important}.sto-black-friday-category-page .sto-category-page-products .box-item.box-item-rayon,.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner,.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"0\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"1\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"2\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"3\"],.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banner-tile,.sto-black-friday-global .sto-black-friday-prods-slides-wrapper .box-item.box-item-rayon{width:31.19%}.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"2\"]{margin-right:0}.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"3\"]{margin-right:20px}.sto-black-friday-category-page .sto-category-page-products .box-item.box-item-rayon,.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner,.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"0\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"1\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"2\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"3\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"4\"]{width:32.21%}.sto-black-friday-header{height:auto;padding-bottom:40.515%;background-size:cover}.sto-black-friday-content-section .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=alimentaire] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=mode] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=vin] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=voyages] .sto-black-friday-content-banner-hp{width:95%}.sto-black-friday-nuit-blanche[bg=\"1\"],.sto-black-friday-nuit-blanche[bg=\"2\"],.sto-black-friday-nuit-blanche[bg=\"3\"]{width:100%;height:auto;background-size:cover;padding-bottom:9.655%;pointer-events:none!important}.sto-black-friday-content-section-header{width:95%;height:auto;background-size:cover;padding-bottom:2.3%}}@media screen and (max-width:1210px){.sto-black-friday-nav-slides-wrapper{width:100%}}@media screen and (max-width:1025px) and (min-width:1024px){.sto-black-friday-nav-slider-nav-btn{margin:0 8px}}@media screen and (max-width:1023px){#return-to-top{position:fixed;bottom:20px;right:20px;width:85px;height:74px;display:block;display:none;z-index:21;background-image:url(" + __webpack_require__(18) + ");background-repeat:no-repeat;max-width:13%;background-size:contain}#return-to-top:hover{background-image:url(" + __webpack_require__(18) + ")}.sto-black-friday-header-slides-wrapper{width:100%;height:100%;font-size:0;position:relative;scroll-snap-type:x mandatory;display:flex;overflow-x:auto;webkit-overflow-scrolling:touch}.sto-black-friday-header-slide{width:100%;height:100%;background-position:50%;background-repeat:no-repeat;background-size:cover;vertical-align:top;background-image:url(" + __webpack_require__(131) + ");scroll-snap-align:start;flex-shrink:0;scroll-behavior:smooth}.sto-black-friday-header-slide.smooth-slider,.sto-black-friday-prods-slider .sto-black-friday-prods-slides-wrapper .box-item.smooth-slider{scroll-snap-align:none}.sto-black-friday-loader{width:100%;background-size:contain;height:355px}.sto-black-friday-header-wrapper .sto-black-friday-loader{width:64%;height:64%;left:18%;top:25.5%;background-size:contain}div.bodywrapper{overflow-x:hidden}.sto-black-friday-category-page[category=bijoux] .sto-category-page-mentions{width:98%;margin:0 auto;color:#4c4c4c;font-size:9px}.sto-black-friday-category-page[category=bijoux] .sto-category-page-mentions span{font-weight:700;font-size:11px}.sto-black-friday-category-page[category=bijoux] .sto-price-filters{margin:0 auto;float:none}.sto-black-friday-header-trad{bottom:0}.sto-black-friday-header{background-size:cover;border-bottom:none;height:auto;padding-bottom:140.558%}.sto-black-friday-header-slider-nav{display:none}.sto-black-friday-header-slider{width:100%;height:100%;left:0;top:0}.sto-black-friday-header-slide{background-image:url(" + __webpack_require__(17) + ");position:relative}.sto-black-friday-global[date=\"2019-11-25\"] .sto-black-friday-header-slide[data-bg=\"2\"]{height:100%;cursor:pointer;background-image:url(" + __webpack_require__(172) + ")}.sto-black-friday-global[date=\"2019-11-25\"] .sto-black-friday-header-slide[data-bg=\"3\"]{height:100%;cursor:pointer;background-image:url(" + __webpack_require__(21) + ")}.sto-black-friday-global[date=\"2019-11-26\"] .sto-black-friday-header-slide[data-bg=\"2\"]{height:100%;cursor:pointer;background-image:url(" + __webpack_require__(168) + ")}.sto-black-friday-global[date=\"2019-11-26\"] .sto-black-friday-header-slide[data-bg=\"5\"]{height:100%;cursor:pointer;background-image:url(" + __webpack_require__(21) + ")}.sto-black-friday-global[date=\"2019-11-27\"] .sto-black-friday-header-slide[data-bg=\"1\"]{height:100%;cursor:pointer;background-image:url(" + __webpack_require__(170) + ")}.sto-black-friday-global[date=\"2019-11-28\"] .sto-black-friday-header-slide[data-bg=\"3\"]{height:100%;cursor:pointer;background-image:url(" + __webpack_require__(2) + ")}.sto-black-friday-global[date=\"2019-11-29\"] .sto-black-friday-header-slide[data-bg=\"1\"]{height:100%;cursor:pointer;background-image:url(" + __webpack_require__(174) + ")}.sto-black-friday-global[date=\"2019-11-29\"] .sto-black-friday-header-slide[data-bg=\"2\"]{height:100%;cursor:pointer;background-image:url(" + __webpack_require__(2) + ")}.sto-black-friday-global[date=\"2019-11-30\"] .sto-black-friday-header-slide[data-bg=\"6\"]{height:100%;cursor:pointer;background-image:url(" + __webpack_require__(2) + ")}.sto-black-friday-global[date=\"2019-12-1\"] .sto-black-friday-header-slide[data-bg=\"1\"]{height:100%;cursor:pointer;background-image:url(" + __webpack_require__(176) + ")}.sto-black-friday-global[date=\"2019-12-2\"] .sto-black-friday-header-slide[data-bg=\"2\"]{height:100%;cursor:pointer;background-image:url(" + __webpack_require__(178) + ")}.sto-black-friday-header-slide>div{height:100%;cursor:pointer;top:50%;transform:translateY(-50%)}.box-item .content{position:static;transform:translateX(0)}.sto-black-friday-header-slide>div:not(.box-item-banner){width:64%;height:64%;left:18%;top:59.5%;display:inline-block;margin:0 auto;border:0;background-size:contain;position:absolute;background-color:transparent}.sto-black-friday-header-slide .box-item.box-item-rayon{background-color:transparent}.sto-black-friday-header-slide>div .box-item-top{height:auto}.sto-black-friday-header-slide>div .box-item-top .item-illustration{position:relative;left:auto;top:auto;transform:none;width:100%}.sto-black-friday-header-slide>div .box-item-top .pricer,.sto-black-friday-header-slide>div .content{position:relative;right:auto;top:auto;width:100%}.sto-black-friday-header-slide>div .content{left:auto;transform:none;padding:12px 12px 10px}.sto-black-friday-content-wrapper{margin-top:0}.sto-black-friday-content-wrapper .sto-black-friday-content-main-claim{background-color:#000;color:#fff;cursor:pointer}.sto-black-friday-content-wrapper .sto-black-friday-content-main-claim:before{content:\" \";background-image:url(" + __webpack_require__(137) + ");width:28px;height:23px;background-size:cover;display:inline-block;top:5px;margin-right:20px;position:relative}.sto-black-friday-content-wrapper.menuopen .sto-black-friday-content-main-claim:before{background-image:url(" + __webpack_require__(138) + ")}.sto-black-friday-nav{height:auto;display:none}.sto-black-friday-content{position:relative}.sto-black-friday-mobile-bg-menu{position:absolute;top:0;bottom:0;left:0;right:0;background-color:rgba(0,0,0,.7);z-index:10;display:none}.sto-black-friday-content-wrapper.menuopen .sto-black-friday-mobile-bg-menu,.sto-black-friday-content-wrapper.menuopen .sto-black-friday-nav{display:block}.sto-black-friday-nav-slider{overflow:initial}.sto-black-friday-nav-slides-wrapper{width:100%;padding:0;position:absolute;height:auto;z-index:20;background-color:#fff}.sto-black-friday-nav-slides-wrapper>a{margin-top:0;width:100%;border-top:2px solid #b4b4b4}.sto-black-friday-nav-slides-wrapper>a:first-child{border-top:none}.sto-black-friday-nav-slider-nav-btn{background-image:none!important;margin:0;width:100%!important;text-align:center}.sto-black-friday-nav-slider-nav-btn span{display:inline-block;font-size:19px;line-height:52px;font-family:Univers Condensed,Roboto Condensed,sans-serif;text-transform:uppercase;text-align:center;cursor:pointer}.sto-black-friday-header-slider-next,.sto-black-friday-header-slider-prev{display:none}.sto-black-friday-header-slider-mob-next,.sto-black-friday-header-slider-mob-prev{display:block}.sto-black-friday-nav-slider-nav-btn[data-cat=hightech]{color:#117dc1}.sto-black-friday-nav-slider-nav-btn[data-cat=maison]{color:#0c4491}.sto-black-friday-nav-slider-nav-btn[data-cat=parapharmacie]{color:#c0cb42}.sto-black-friday-nav-slider-nav-btn[data-cat=culture]{color:#6c378d}.sto-black-friday-nav-slider-nav-btn[data-cat=mode]{color:#f59d24}.sto-black-friday-nav-slider-nav-btn[data-cat=vin]{color:#4c0a35}.sto-black-friday-nav-slider-nav-btn[data-cat=alimentaire]{color:#c43682}.sto-black-friday-nav-slider-nav-btn[data-cat=bijoux]{color:#019e93}.sto-black-friday-nav-slider-nav-btn[data-cat=all]{color:#ab4e30}.sto-black-friday-category-page .sto-category-page-header,.sto-black-friday-content-section-header{width:88.69%}.sto-category-page-filters{width:100%}.sto-category-page-filters .sto-price-filters,.sto-category-page-filters .sto-subcategory-filters{width:calc(50% - 8px);height:80px}.sto-category-page-filters .sto-price-filters .sto-price-filter-current .sto-current-price-filter,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-current .sto-current-subcategory-filter{font-size:10px}.sto-category-page-filters .sto-price-filters .sto-price-filter-list .sto-category-page-filter-price,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-list .sto-category-page-filter-subcategory,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-list .sto-category-page-filter-universe{font-size:11px;line-height:18px}.sto-black-friday-category-page[category=maison] .sto-category-page-header,.sto-black-friday-content-section[data-cat=maison] .sto-black-friday-content-section-header{background-image:url(" + __webpack_require__(136) + ");background-size:contain;background-repeat:repeat-x;height:auto;padding-bottom:8%}.sto-black-friday-content-banners-hp,.sto-black-friday-content-section-products{position:relative}.sto-black-friday-category-page .sto-category-page-products{width:100%;text-align:center}.sto-black-friday-banner-slider,.sto-black-friday-category-page .sto-category-page-products .box-item.box-item-rayon,.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"0\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"1\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"2\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"3\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"4\"],.sto-black-friday-prods-slider{width:76.06%;position:relative;overflow:hidden;left:0;right:0;margin:0 auto;height:auto}.sto-black-friday-category-page .sto-category-page-banner .sto-category-banner-cta{display:none}.sto-black-friday-category-page .sto-category-page-products .box-item.box-item-rayon,.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"0\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"1\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"2\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"3\"],.sto-black-friday-category-page[category=bijoux] .sto-gamme-banner[index=\"4\"]{margin-top:20px}.sto-black-friday-banner-slider .sto-black-friday-banner-slides-wrapper,.sto-black-friday-prods-slider .sto-black-friday-prods-slides-wrapper{width:100%;height:100%;font-size:0;position:relative;scroll-snap-type:x mandatory;display:flex;overflow-x:auto;webkit-overflow-scrolling:touch}.sto-black-friday-banner-slider .sto-black-friday-banner-slides-wrapper .sto-black-friday-content-banner-tile,.sto-black-friday-prods-slider .sto-black-friday-prods-slides-wrapper .box-item{width:100%;background-position:50%;background-repeat:no-repeat;background-size:cover;vertical-align:top;position:relative;scroll-snap-align:start;flex-shrink:0;scroll-behavior:smooth;width:calc(100% - 2px)!important}.sto-black-friday-banner-slider-next,.sto-black-friday-banner-slider-prev,.sto-black-friday-prods-slider-next,.sto-black-friday-prods-slider-prev{width:4.92%;height:auto;padding-bottom:7%;position:absolute;background-size:contain;background-repeat:no-repeat;top:50%;cursor:pointer;z-index:1;background-image:url(" + __webpack_require__(34) + ");display:block!important}.sto-black-friday-banner-slider-prev,.sto-black-friday-prods-slider-prev{left:10px}.sto-black-friday-banner-slider-next,.sto-black-friday-prods-slider-next{right:10px;transform:rotate(180deg)}.sto-black-friday-nuit-blanche[bg=\"1\"]{background-image:url(" + __webpack_require__(54) + ")}.sto-black-friday-nuit-blanche[bg=\"1\"],.sto-black-friday-nuit-blanche[bg=\"2\"]{width:100%;height:auto;background-size:cover;padding-bottom:20%;pointer-events:none!important}.sto-black-friday-category-page .sto-gamme-banner,.sto-black-friday-category-page[category=hightech] .sto-gamme-banner[index=\"0\"],.sto-black-friday-category-page[category=hightech] .sto-gamme-banner[index=\"1\"],.sto-black-friday-category-page[category=maison] .sto-gamme-banner[index=\"0\"],.sto-black-friday-category-page[category=mode] .sto-gamme-banner[index=\"0\"],.sto-black-friday-category-page[category=mode] .sto-gamme-banner[index=\"1\"],.sto-black-friday-category-page[category=mode] .sto-gamme-banner[index=\"2\"],.sto-black-friday-category-page[category=mode] .sto-gamme-banner[index=\"3\"],.sto-black-friday-category-page[category=parapharmacie] .sto-gamme-banner[index=\"0\"],.sto-black-friday-category-page[category=vins] .sto-gamme-banner[index=\"0\"],.sto-black-friday-content-section .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=alimentaire] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=auto] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banner-hp-2,.sto-black-friday-content-section[data-cat=culture] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=hightech] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=maison] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=mode] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=parapharmacie] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=photos] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=vin] .sto-black-friday-content-banner-hp,.sto-black-friday-content-section[data-cat=voyages] .sto-black-friday-content-banner-hp{width:100%;height:auto;background-size:cover;padding-bottom:39.3334%}.sto-black-friday-content-section[data-cat=hightech] .sto-black-friday-content-banner-hp{background-image:url(" + __webpack_require__(64) + ")}.sto-black-friday-content-section[data-cat=maison] .sto-black-friday-content-banner-hp{background-image:url(" + __webpack_require__(68) + ")}.sto-black-friday-content-section[data-cat=mode] .sto-black-friday-content-banner-hp{background-image:url(" + __webpack_require__(74) + ")}.sto-black-friday-content-section[data-cat=vin] .sto-black-friday-content-banner-hp{background-image:url(" + __webpack_require__(66) + ")}.sto-black-friday-content-section[data-cat=parapharmacie] .sto-black-friday-content-banner-hp{background-image:url(" + __webpack_require__(70) + ")}.sto-black-friday-content-section[data-cat=voyages] .sto-black-friday-content-banner-hp{background-image:url(" + __webpack_require__(76) + ")}.sto-black-friday-content-section[data-cat=photos] .sto-black-friday-content-banner-hp{background-image:url(" + __webpack_require__(72) + ")}.sto-black-friday-content-section[data-cat=culture] .sto-black-friday-content-banner-hp{background-image:url(" + __webpack_require__(62) + ")}.sto-black-friday-content-section[data-cat=auto] .sto-black-friday-content-banner-hp{background-image:url(" + __webpack_require__(56) + ")}.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banner-hp{background-image:url(" + __webpack_require__(58) + ")}.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banner-hp-2{background-image:url(" + __webpack_require__(60) + ")}.sto-black-friday-category-page[category=hightech] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(96) + ")}.sto-black-friday-category-page[category=hightech] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(94) + ")}.sto-black-friday-category-page[category=hightech] .sto-gamme-banner[index=\"2\"]{background-image:url(" + __webpack_require__(92) + ")}.sto-black-friday-category-page[category=hightech] .sto-gamme-banner[index=\"3\"]{background-image:url(" + __webpack_require__(98) + ")}.sto-black-friday-category-page[category=maison] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(88) + ")}.sto-black-friday-category-page[category=maison] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(90) + ")}.sto-black-friday-category-page[category=maison] .sto-gamme-banner[index=\"2\"]{background-image:url(" + __webpack_require__(104) + ")}.sto-black-friday-category-page[category=maison] .sto-gamme-banner[index=\"3\"]{background-image:url(" + __webpack_require__(86) + ")}.sto-black-friday-category-page[category=parapharmacie] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(114) + ")}.sto-black-friday-category-page[category=parapharmacie] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(116) + ")}.sto-black-friday-category-page[category=parapharmacie] .sto-gamme-banner[index=\"2\"]{background-image:url(" + __webpack_require__(118) + ")}.sto-black-friday-category-page[category=parapharmacie] .sto-gamme-banner[index=\"3\"]{background-image:url(" + __webpack_require__(120) + ")}.sto-black-friday-category-page[category=mode] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(106) + ")}.sto-black-friday-category-page[category=mode] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(108) + ")}.sto-black-friday-category-page[category=mode] .sto-gamme-banner[index=\"2\"]{background-image:url(" + __webpack_require__(110) + ")}.sto-black-friday-category-page[category=mode] .sto-gamme-banner[index=\"3\"]{background-image:url(" + __webpack_require__(112) + ")}.sto-black-friday-category-page[category=vin] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(100) + ")}.sto-black-friday-category-page[category=vin] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(102) + ")}.sto-black-friday-category-page[category=culture] .sto-gamme-banner[index=\"0\"]{background-image:url(" + __webpack_require__(78) + ")}.sto-black-friday-category-page[category=culture] .sto-gamme-banner[index=\"1\"]{background-image:url(" + __webpack_require__(80) + ")}.sto-black-friday-category-page[category=culture] .sto-gamme-banner[index=\"2\"]{background-image:url(" + __webpack_require__(82) + ")}.sto-black-friday-category-page[category=culture] .sto-gamme-banner[index=\"3\"]{background-image:url(" + __webpack_require__(84) + ")}.sto-black-friday-category-page .sto-category-banner-cta-mob{background:url(" + __webpack_require__(42) + ");width:27%;height:auto;padding-bottom:3.635%;background-repeat:no-repeat;background-position:50%;background-size:cover;cursor:pointer;margin:4% 0;margin-left:4%;display:block}.sto-black-friday-category-page .sto-category-page-banner{background:url(" + __webpack_require__(148) + ");height:auto;padding-bottom:27.56%;background-size:cover}.sto-black-friday-content-section[data-cat=bijoux] .sto-black-friday-content-banners-hp .sto-black-friday-banner-slider{margin-top:20px}.sto-pagination-top.sto-category-page-pagination{display:none}.sto-pagination-bottom.sto-category-page-pagination{float:none;width:100%;text-align:center}.sto-category-page-filters .sto-price-filters .sto-price-filter-current .sto-current-price-filter:after,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-current .sto-current-subcategory-filter:after{width:11px;height:8px;background-size:cover}}@media screen and (max-width:320px){.box-item .item-illustration img{max-height:105px}.box-item .pricer img{max-height:80px}.sto-black-friday-header-slider-mob-next,.sto-black-friday-header-slider-mob-prev,.sto-black-friday-header-slider-next,.sto-black-friday-header-slider-prev{width:37px;height:42px;background-size:cover}.sto-black-friday-header-slider-mob-prev,.sto-black-friday-header-slider-prev{left:5px}.sto-black-friday-header-slider-mob-next,.sto-black-friday-header-slider-next{right:5px}.sto-category-page-filters .sto-price-filters,.sto-category-page-filters .sto-subcategory-filters{width:calc(50% - 4px)}.sto-category-page-filters .sto-price-filters .sto-price-filter-current .sto-current-price-filter:after,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-current .sto-current-subcategory-filter:after{width:9px;height:6px}.sto-category-page-filters .sto-price-filters .sto-price-filter-current .sto-current-price-filter,.sto-category-page-filters .sto-subcategory-filters .sto-subcategory-filter-current .sto-current-subcategory-filter{font-size:8px}.sto-category-page-filters .sto-subcategory-filters{margin-right:8px}}#scrollTopMobile{display:none}@media screen and (max-width:414px){.sto-black-friday-category-page[category=maison] .sto-category-page-header,.sto-black-friday-content-section[data-cat=maison] .sto-black-friday-content-section-header{background-size:100% 100%;height:auto;padding-bottom:10.5%}}", ""]);
	
	// exports


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "0b1d9319c9dc9c169bb87d287ff81f98.png";

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "a7c22a396ecbf7f5d2786eb971b6e7c9.png";

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "08c606cc5f5151a13579b0765a9ee16d.png";

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "fea69d4397a7291ee99046f8ac0e6b34.png";

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "30404232907f74ef58ba85591cdd0b2e.png";

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "962617944da39140bf64137cad85d688.png";

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "a5f2d1b704c5c9099f47cc08d24ad046.png";

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "fa4a087cfc0705567afac91769e17073.png";

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "0680be34f152ffa77b535091be2c72c4.jpg";

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "5513c8329778c3962c73418b9f9bb9ea.jpg";

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "20af2d3762171b40326479e542584c89.jpg";

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "19d3cd66a9e820b8a0b860e1e19b8ef9.png";

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "d71dc8395d6f85706ab9bc929f24b74e.jpg";

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "5a9440d83403476f1ab949d8c4d441b2.png";

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "d00ed93441ea33ef3aaf52e719f7793c.jpg";

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "22b9969cc8c0565ef0a2da1b9289a8d0.jpg";

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "5350f2db623d2871db165e590d6aa87d.jpg";

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "ba271cada59fcfc55206061886c0d290.jpg";

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "56e3a3d2aa04dca591bc8d1edfe95a91.jpg";

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "4da9bac6d51dc07883a41a1042c1c59e.jpg";

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "4ccbbc74f3cd6ee97435ce51f5b140a2.jpg";

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "d32d370116d4c56663c1dfa2b754ba74.jpg";

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "c42ba25263dc48d392211cc729b2d9b5.jpg";

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "e591d070e99ad7d07b88a79360fceff5.jpg";

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "f2978330a400e83d17183a0425d84c7c.jpg";

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "5e93dbcc02e06a36ea73248d041d83de.jpg";

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "16a462f1527869ce69acfa279534454c.jpg";

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "72dfe4f58ef5f7ce38d608455a33a65c.jpg";

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "8e803e628092cd726e5c51930314357d.jpg";

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "6ce5ffd274ae069095807d59108ce212.jpg";

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "baffbc85c5813f6f7632bf60e92b1d30.jpg";

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "f1b98819ce537e521ee269465c08b3bf.jpg";

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "031d2d507c32437666d6c703be3a540d.jpg";

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "84488d173aefa634c16e331212cc41ae.jpg";

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "f0d90761e38ed220b692404886d95e82.jpg";

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "765a5e4fdaa0401f7cfcd552ed70e3c3.jpg";

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "b610dbef0b4487f6da9ef2869d12282b.jpg";

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "daccd95179e8041211f3ca360e73ae04.jpg";

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "a5f734ad0644f6c8793ed7aeeb7a504e.jpg";

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "7e9eaea6ba7404e64c896b14db5856b8.png";

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "b2007c042a67b45609a60291d9561e74.png";

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "b1b449a5ecf74991aec88379441ecc55.jpg";

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "e42621aa59e6baa0088c69033389a1df.jpg";

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "20a28d93153d67c3a02dda515e497f0d.jpg";

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "272ce96bebcb554f861dd5a5544fe7e1.jpg";

/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "e76744b6a2fedc7dbb9d156e80eabbd5.jpg";

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "f642e769cc3b8a1fa5add9b0caa26069.jpg";

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "fde50ab9336da66080bf4f59da285e00.jpg";

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "97df6be8ee135ca4550a46029b0c32cd.jpg";

/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "179a8d41ecb034a4aabd088dc90b94d4.jpg";

/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "e0a2c8ac79418754602504b09f267e38.jpg";

/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "dc5f7d8f27a4d5cd32a486362db88f2a.jpg";

/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "416f48e984c1f52426046112585d54ad.jpg";

/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "8a4556d1ac302e5d2f6a301de0a5b0c9.jpg";

/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "7c98d4da2360614e9392151dea43368b.jpg";

/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "502913310075b9b488bfa546a2d92b1b.jpg";

/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "5e069dce329dabdcca672bf5929b378a.jpg";

/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "7da9cf01adf84cd44fca575a9c9a8d79.jpg";

/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "b72cf4cba2989d64dd15e22531e25b1f.jpg";

/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "59389d875905586eef03e651aa0606bc.jpg";

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "d308bdee3640043a9b8fb429695f84bf.jpg";

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "5a1a1946f1cdde7b26f175b5b1ca8137.jpg";

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "28b5acbbe630726e189758129638f349.jpg";

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "7da9cf01adf84cd44fca575a9c9a8d79.jpg";

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "b72cf4cba2989d64dd15e22531e25b1f.jpg";

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "78353906fd4601cf513417ab6499e090.jpg";

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "2d3e9424b645767505dc644e73fe4ebb.jpg";

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "fa5ba6ad6f69b1cedc23f2aef49c93d9.jpg";

/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "388812b157b14d27a6b55e7cf2411155.jpg";

/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "8602c8bd1c9392eec148485c6a9eedae.jpg";

/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "da1394ce4853b3c46540808ed96fd874.jpg";

/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "1b6ceafe9581a39114c8333e45f0ad0d.jpg";

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "5af560d10414bf88df957b4a0ca0c523.jpg";

/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "bce1d4ce9fe7aa16bda0e0bb6c4699af.jpg";

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "e88ecb3ec6b4e4fe71a9c53a03fb8da4.jpg";

/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "5e0cb6df73e5a39764d4e48bb0a13113.jpg";

/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "e0131254a0dedcf865a6ef71d4ae903c.jpg";

/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "11a2c8293821bc36bd080eb56635e12c.jpg";

/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "0ca4d638ff4fef49003a11fd6d93a56c.jpg";

/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "e0a700d4b73889f73138ea1b46f0f85f.jpg";

/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "b40ad96ce7bac3b5764db9f061118768.jpg";

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "cfd13f2ddb51285b98009e3e85ee0264.jpg";

/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "2a1b623aa71b30673a8c13d1692197e8.jpg";

/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "24913975785dbde44d259c6cab1416ac.jpg";

/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "66f29011ba264870ab9827f41e3a5cf2.jpg";

/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "baed0e4ac93fc95aecb9f198f09e62bd.jpg";

/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "2196bfa4832829b273d7b4ac14410826.jpg";

/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "016064e7d2a701b04ee74601025f7415.jpg";

/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "ee5c17a0ab814d5fc2e135779720f691.jpg";

/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "82c3862df810639c602d1a512f891da4.jpg";

/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "8c14a2ff94de641b3a9c4176c37a35be.jpg";

/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "d7af74335bec30b76abaf7f7c7317883.jpg";

/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "22ced592fcbe31a3caf5acf8b976730b.jpg";

/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "3fa6be1e4eeb92239737e02b4933e5f3.png";

/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "4a50c34f6852ce0137db72084687fb55.jpg";

/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "91cc1edf9b63149f52930f4279b21dfa.jpg";

/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "b656facc16c905b31ffefcbe2650e7ca.png";

/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "5b80c45f331199fd0c71840b64d766d6.png";

/***/ }),
/* 127 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "2f687428249c70eeaa8493339ab950fb.png";

/***/ }),
/* 128 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "e1e5f562873a2c346d33ab514faade80.png";

/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "eb9dd2e7cd3aec9dd50db90dfe80508e.png";

/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "640d717a8f342ed9d722714346415765.png";

/***/ }),
/* 131 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "e441fe97ce40715998ab310c76f38005.jpg";

/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "d45692603ebb5be4b0895ec7b5ca39b7.jpg";

/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "5772a9f7721a26a875efd758f07454e9.jpg";

/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "f4404720ece11355df318a0acf525cb1.gif";

/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "2f5a1d5dac7622d4996b2b6c025d50b1.png";

/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "ca0730f9f328330748429d5ea20a9d8d.png";

/***/ }),
/* 137 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "8e0d7379808b0b6b2a6f97f5b1680574.png";

/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "10470526ad8c64f7bc3a4887c6977e73.png";

/***/ }),
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "1d208b5708c139f180b22f320433c176.png";

/***/ }),
/* 140 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "9fe6330fcb93c6e5d8f8680af3e9699c.png";

/***/ }),
/* 141 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "6d967740b8071c4296c357373e5dff1d.png";

/***/ }),
/* 142 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "ba30df2f3ad07e8b9a07afbcd3c3a5ce.png";

/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "ae61bd84adc72ad5818988af7cbf0fd9.png";

/***/ }),
/* 144 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "e631027deae2201f1de763dbc830d585.png";

/***/ }),
/* 145 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "8db6b9c92afe61ff5d6776b551726ba9.png";

/***/ }),
/* 146 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "63ace89954fd1fc2329330a44a7be15d.png";

/***/ }),
/* 147 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "d6f5c179b6a4763488cd2d29b1192792.png";

/***/ }),
/* 148 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "bca008f197550afe607f6688caa4aef2.jpg";

/***/ }),
/* 149 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "b67bb8e503e6831505234668d114e722.png";

/***/ }),
/* 150 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "3e8e2f5eba88913ba7254aaaf1dee55d.png";

/***/ }),
/* 151 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "56230d9019ad7c597b46aca2b07b2885.png";

/***/ }),
/* 152 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "c8c3fbc272f3d5253a616cae9fb2bd3a.png";

/***/ }),
/* 153 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "77c3f03fc626ade5a5db58c717adfbc6.png";

/***/ }),
/* 154 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "01a8733099df718098f2bf5bbdd5c24e.png";

/***/ }),
/* 155 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "1e9c54dcfec90da3dbe558c3ebbb765b.png";

/***/ }),
/* 156 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "81f2b56511accbc2ea0a4afb947e0fbc.png";

/***/ }),
/* 157 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "d308c6f8b0fba4136d6be59df16b21fb.png";

/***/ }),
/* 158 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "f12af3b94ed7d65e82cdfdf44dcad330.png";

/***/ }),
/* 159 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "c8ef3825fa89354aa58a276f9f40529b.png";

/***/ }),
/* 160 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "fadaea30e7896f0cc5ff76ef2e824e43.png";

/***/ }),
/* 161 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "ade64b42e585451a2d524df4574a917d.png";

/***/ }),
/* 162 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "4fdd65490b6e0f6a3893dffaa945f59b.png";

/***/ }),
/* 163 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "84dd967e2bd18ec29070aa15f33b983a.png";

/***/ }),
/* 164 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "4949a0d6b63e771d3d2735beb5e14b38.png";

/***/ }),
/* 165 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "22150d89aa1eada777b15b0e75259f77.jpg";

/***/ }),
/* 166 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "a31bedb5bc60dfff479790da37893200.jpg";

/***/ }),
/* 167 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "2ea48a928159f83ba2dd3d0e52b01aae.jpg";

/***/ }),
/* 168 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "ab8c02c5464b0bcdad62de086020f97d.jpg";

/***/ }),
/* 169 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "8307765ae29646b6ca7b65ad703612bd.jpg";

/***/ }),
/* 170 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "a452be23bce19f523a5193998b0c661e.jpg";

/***/ }),
/* 171 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "dc2383d73c77666aa5feeea9e278193b.jpg";

/***/ }),
/* 172 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "aae5eb3bfce9f10af9bf6442b5c4eeee.jpg";

/***/ }),
/* 173 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "b5b5e4a09d36562199ce66e500e5334d.jpg";

/***/ }),
/* 174 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "a14e2b02700c01aeec7fa1871bf274da.jpg";

/***/ }),
/* 175 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "e9fa292ac60deaae12468fd7e82d8abe.jpg";

/***/ }),
/* 176 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "4c56817444727f0e59dc8a9f93a19483.jpg";

/***/ }),
/* 177 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "2c4ee954a212864e6fa05ab2b0b778a1.jpg";

/***/ }),
/* 178 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "4269a3cfd7a75a131c9320c2dd7d6c95.jpg";

/***/ }),
/* 179 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "57c8b4bdeb2ffd00a8c8e5a1680ba309.jpg";

/***/ }),
/* 180 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "0c0c3a53380937a32c559a8425c92c70.png";

/***/ }),
/* 181 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "eab0e033d1f57e7476bea7386df84e52.png";

/***/ }),
/* 182 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "8adedcb3d25cafcc05b7823a7f3b5b81.png";

/***/ }),
/* 183 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "56d93eb90949206c5739edba55ce44ac.png";

/***/ }),
/* 184 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "/Univers-Condensed-Medium.svg";

/***/ }),
/* 185 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "/Univers-Condensed-Medium.ttf";

/***/ }),
/* 186 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "/Univers-Condensed-Medium.woff";

/***/ }),
/* 187 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "/Univers-Condensed-Medium.woff2";

/***/ }),
/* 188 */
/***/ (function(module, exports) {

	module.exports = "<div class=sto-black-friday-gutter> <div class=sto-black-friday-gutter-left> <div class=sto-black-friday-gutter-top></div> </div> <div class=sto-black-friday-gutter-right> <div class=sto-black-friday-gutter-top></div> </div> </div>";

/***/ }),
/* 189 */
/***/ (function(module, exports) {

	module.exports = "<div class=sto-black-friday-global data-v=2019-11-29-v04> <a id=link-nuitblanche href=# style=cursor:initial><section class=sto-black-friday-nuit-blanche></section></a> <section class=sto-black-friday-home-page> <div class=sto-black-friday-header-wrapper> <div class=sto-black-friday-header> <div class=sto-black-friday-header-slider-prev></div> <div class=sto-black-friday-header-slider-next></div> <div class=sto-black-friday-header-slider-mob-prev></div> <div class=sto-black-friday-header-slider-mob-next></div> <div class=sto-black-friday-loader></div> <ul class=sto-black-friday-header-slider-nav> <li class=\"sto-black-friday-header-slider-nav-btn sto-header-slide-active\" data-ind=1>&nbsp;</li> <li class=sto-black-friday-header-slider-nav-btn data-ind=2>&nbsp;</li> <li class=sto-black-friday-header-slider-nav-btn data-ind=3>&nbsp;</li> <li class=sto-black-friday-header-slider-nav-btn data-ind=4>&nbsp;</li> </ul> <div class=sto-black-friday-header-slider> <div class=sto-black-friday-header-slides-wrapper> <div id=slide-1 class=\"sto-black-friday-header-slide sto-header-slide-active\" data-ind=1></div> <div id=slide-2 class=sto-black-friday-header-slide data-ind=2></div> <div id=slide-3 class=sto-black-friday-header-slide data-ind=3></div> <div id=slide-4 class=sto-black-friday-header-slide data-ind=4></div> </div> </div> </div> </div> <div class=sto-black-friday-content-wrapper> <div class=sto-black-friday-content-main-claim>Découvrez nos univers</div> <div class=sto-black-friday-nav> <div class=sto-black-friday-nav-slider> <ul class=sto-black-friday-nav-slides-wrapper> <a href=# onclick='dataLayer.push({nomclicsortant:\"high-tech\",event:\"blackfriday_clics_autopromo\"})' data-cat=hightech><li class=sto-black-friday-nav-slider-nav-btn data-cat=hightech><span>high-tech</span></li></a> <a href=# onclick='dataLayer.push({nomclicsortant:\"jardin-_maison_-_electromenager\",event:\"blackfriday_clics_autopromo\"})' data-cat=maison><li class=sto-black-friday-nav-slider-nav-btn data-cat=maison><span>jardin, maison &#38; électromenager</span></li></a> <a href=# onclick='dataLayer.push({nomclicsortant:\"parapharmacie\",event:\"blackfriday_clics_autopromo\"})' data-cat=parapharmacie><li class=sto-black-friday-nav-slider-nav-btn data-cat=parapharmacie><span>parapharmacie</span></li></a> <a href=# onclick='dataLayer.push({nomclicsortant:\"culture_-_jouets\",event:\"blackfriday_clics_autopromo\"})' data-cat=culture><li class=sto-black-friday-nav-slider-nav-btn data-cat=culture><span>culture &#38; jouets</span></li></a> <a href=# onclick='dataLayer.push({nomclicsortant:\"mode_-_sport\",event:\"blackfriday_clics_autopromo\"})' data-cat=mode><li class=sto-black-friday-nav-slider-nav-btn data-cat=mode><span>mode &#38; sport</span></li></a> <a href=# onclick='dataLayer.push({nomclicsortant:\"vins_-_alcools\",event:\"blackfriday_clics_autopromo\"})' data-cat=vin><li class=sto-black-friday-nav-slider-nav-btn data-cat=vin><span>vins &#38; alcools</span></li></a> <a href=# onclick='dataLayer.push({nomclicsortant:\"voir_tous_les_produits\",event:\"blackfriday_clics_autopromo\"})' data-cat=all><li class=sto-black-friday-nav-slider-nav-btn data-cat=all><span>voir tous les produits</span></li></a> </ul> </div> </div> <div class=sto-black-friday-content> <div class=sto-black-friday-mobile-bg-menu></div> <div class=sto-black-friday-content-section id=sto-section-hightech data-cat=hightech> <div class=sto-black-friday-content-section-header></div> <div class=sto-black-friday-content-section-products> <div class=sto-black-friday-prods-slider-prev></div> <div class=sto-black-friday-prods-slider-next></div> <div class=sto-black-friday-prods-slider> <div class=sto-black-friday-loader></div> <div class=sto-black-friday-prods-slides-wrapper></div> </div> <div class=sto-black-friday-content-section-seemore><span>Voir plus de produits</span></div> <a href=\"https://www.hightech.leclerc/black-friday-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-S48-BlackFriday&utm_content=Banniere_boutique_BlackFriday\" onclick='dataLayer.push({nomcampagne:\"blackfriday2019\",nomclicsortant:\"hp_hightech_jusqu-a_-40-_de_remise_immediate_sur_notre_selection_hightech_black_Friday\",urlsortie:\"https---gti3-hightech-leclerc-dynclick-multimedia-eleclerc-com--ept-publisher-e-leclerc-portail-ept-name-portail-s47-eurl-https-3a-2f-2fwww-hightech-leclerc-2fblack-friday-b-3futm-source-3de-leclerc-portail-26utm-campaign-3ds47-26utm-medium-3dleclerc-26utm-content-3dblackfridayboutique\",event:\"blackfriday_traffickingexterne_visuels\"})' target=_blank><div class=sto-black-friday-content-banner-hp></div></a> </div> </div> <div class=sto-black-friday-content-section id=sto-section-maison data-cat=maison> <div class=sto-black-friday-content-section-header></div> <div class=sto-black-friday-content-section-products> <div class=sto-black-friday-prods-slider-prev></div> <div class=sto-black-friday-prods-slider-next></div> <div class=sto-black-friday-prods-slider> <div class=sto-black-friday-loader></div> <div class=sto-black-friday-prods-slides-wrapper></div> </div> <div class=sto-black-friday-content-section-seemore><span>Voir plus de produits</span></div> <a href=\"https://www.maisonetloisirs.leclerc/black-friday-maison-loisirs?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-S48-BlackFriday&utm_content=BanniereHPM&L\" onclick='dataLayer.push({nomcampagne:\"blackfriday2019\",nomclicsortant:\"hp_maison_jusqu-a_-40-_sur_une_selection_de_produits_maison\",urlsortie:\"https---www-maisonetloisirs-leclerc-black-friday-maison-loisirs\",event:\"blackfriday_traffickingexterne_visuels\"})' target=_blank><div class=sto-black-friday-content-banner-hp></div></a> </div> </div> <div class=sto-black-friday-content-section id=sto-section-parapharmacie data-cat=parapharmacie> <div class=sto-black-friday-content-section-header></div> <div class=sto-black-friday-content-section-products> <div class=sto-black-friday-prods-slider-prev></div> <div class=sto-black-friday-prods-slider-next></div> <div class=sto-black-friday-prods-slider> <div class=sto-black-friday-loader></div> <div class=sto-black-friday-prods-slides-wrapper></div> </div> <a id=sto-seemore-parapharmacie href=#><div class=sto-black-friday-content-section-seemore><span>Voir plus de produits</span></div></a> <a href=\"https://www.parapharmacie.leclerc/selection/black-friday-parapharmacie?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-banniere-black-friday&utm_content=portail\" onclick='dataLayer.push({nomcampagne:\"blackfriday2019\",nomclicsortant:\"hp_parapharmacie_jusqu-a_-40-_de_remise_immediate_sur_notre_selection_parapharmacie_black_Friday\",urlsortie:\"https---gti3-parapharmacie-leclerc-dynclick-parapharmacie-leclerc--ept-publisher-e-leclerc-portail-ept-name-portail-black-friday-eurl-https-3a-2f-2fwww-parapharmacie-leclerc-2fpromotions-2f-3futm-source-3de-leclerc-portail-26utm-campaign-3dblack-friday-26utm-medium-3dleclerc-26utm-content-3dbanniere\",event:\"blackfriday_traffickingexterne_visuels\"})' target=_blank><div class=sto-black-friday-content-banner-hp></div></a> </div> </div> <div class=sto-black-friday-content-section id=sto-section-culture data-cat=culture> <div class=sto-black-friday-content-section-header></div> <div class=sto-black-friday-content-section-products> <div class=sto-black-friday-prods-slider-prev></div> <div class=sto-black-friday-prods-slider-next></div> <div class=sto-black-friday-prods-slider> <div class=sto-black-friday-loader></div> <div class=sto-black-friday-prods-slides-wrapper></div> </div> <div class=sto-black-friday-content-section-seemore><span>Voir plus de produits</span></div> <a href=\"https://www.culture.leclerc/black-friday-u?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=banniere-black-friday\" onclick='dataLayer.push({nomcampagne:\"blackfriday2019\",nomclicsortant:\"hp_culture_des_prix_incroyables_sur_notre_selection_culturelle\",urlsortie:\"http---gti3-culture-leclerc-dynclick-e-leclerc-espace-culturel--ept-publisher-e-leclerc-portail-ept-name-portail-page-dediee-black-friday-eurl-https-3a-2f-2fculture-leclerc-2fblack-friday-u-3futm-source-3de-leclerc-portail-26utm-campaign-3dpage-dediee-black-friday-26utm-medium-3dleclerc-26utm-content-3dhome-black-friday\",event:\"blackfriday_traffickingexterne_visuels\"})' target=_blank><div class=sto-black-friday-content-banner-hp></div></a> </div> </div> <div class=sto-black-friday-content-section id=sto-section-mode data-cat=mode> <div class=sto-black-friday-content-section-header></div> <div class=sto-black-friday-content-section-products> <div class=sto-black-friday-prods-slider-prev></div> <div class=sto-black-friday-prods-slider-next></div> <div class=sto-black-friday-prods-slider> <div class=sto-black-friday-loader></div> <div class=sto-black-friday-prods-slides-wrapper></div> </div> <div class=sto-black-friday-content-section-seemore><span>Voir plus de produits</span></div> <a href=\"https://www.sport.leclerc/toutes-les-offres?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-06-11-black-friday-sport&utm_content=banniere-portail-black-friday-sport\" onclick='dataLayer.push({nomcampagne:\"blackfriday2019\",nomclicsortant:\"hp_mode_decouvrez_nos_offres_exceptionnelles_sur_notre_selection_black_Friday\",urlsortie:\"https---gti3-sport-leclerc-dynclick-sport-leclerc--ept-publisher-e-leclerc-portail-ept-name-portail-blackfriday-eurl-https-3a-2f-2fwww-sport-leclerc-2fsportswear-3futm-source-3de-leclerc-portail-26utm-campaign-3dblackfriday-26utm-medium-3dleclerc-26utm-content-3dportailblackfriday\",event:\"blackfriday_traffickingexterne_visuels\"})' target=_blank><div class=sto-black-friday-content-banner-hp></div></a> </div> </div> <div class=sto-black-friday-content-section id=sto-section-vin data-cat=vin> <div class=sto-black-friday-content-section-header></div> <div class=sto-black-friday-content-section-products> <div class=sto-black-friday-prods-slider-prev></div> <div class=sto-black-friday-prods-slider-next></div> <div class=sto-black-friday-prods-slider> <div class=sto-black-friday-loader></div> <div class=sto-black-friday-prods-slides-wrapper></div> </div> <div class=sto-black-friday-content-section-seemore><span>Voir plus de produits</span></div> <a href=\"https://www.macave.leclerc/black-week?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=banniere-hp\" onclick='dataLayer.push({nomcampagne:\"blackfriday2019\",nomclicsortant:\"hp_vin_jusqu-a_10-_de_remise_immediate_sur_notre_selection_black_Friday\",urlsortie:\"https---www-macave-leclerc-black-week-utm-source-e-leclerc-portail-utm-campaign-black-friday-utm-medium-leclerc-utm-content-black-friday-2018-ectrans-1-res-sf-36\",event:\"blackfriday_traffickingexterne_visuels\"})' target=_blank><div class=sto-black-friday-content-banner-hp></div></a> </div> </div> <div class=sto-black-friday-content-section id=sto-section-photos data-cat=photos> <div class=sto-black-friday-content-section-header></div> <div class=sto-black-friday-content-section-products> <a id=sto-black-friday-banner-photo-link href=\"https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=\" onclick='dataLayer.push({nomcampagne:\"blackfriday2019\",nomclicsortant:\"hp_photo_40-_de_reduction_sur_tout_le_site\",urlsortie:\"http---www-photomoinscher-leclerc--cref-om-coop-blackfriday-750x295-08463-30ttn18\",event:\"blackfriday_traffickingexterne_visuels\"})' target=_blank><div class=sto-black-friday-content-banner-hp></div></a> </div> </div> <div class=sto-black-friday-content-section id=sto-section-voyages data-cat=voyages> <div class=sto-black-friday-content-section-header></div> <div class=sto-black-friday-content-section-products> <a id=sto-black-friday-banner-photo-link href=\"https://www.leclercvoyages.com/search?m_c.MKT=OPN.disney-2019&m_c.desti=FR.IDF.DISNEY&topHotel=false&utm_campaign=Portail&utm_source=Leclerc&utm_medium=CrossLink&utm_term=BlackFriday&utm_content=OPN_Disney2020\" onclick='dataLayer.push({nomcampagne:\"blackfriday2019\",nomclicsortant:\"hp_voyage_offre_disney_finalisee_en_cours_de_reception\",urlsortie:\"https://www.leclercvoyages.com/search?m_c.MKT=OPN.disney-2019&m_c.desti=FR.IDF.DISNEY&topHotel=false&utm_campaign=Portail&utm_source=Leclerc&utm_medium=CrossLink&utm_term=BlackFriday&utm_content=OPN_Disney2020\",event:\"blackfriday_traffickingexterne_visuels\"})' target=_blank><div class=sto-black-friday-content-banner-hp></div></a> </div> </div> <div class=sto-black-friday-content-section id=sto-section-bijoux data-cat=bijoux> <div class=sto-black-friday-content-section-header></div> <div class=sto-black-friday-content-section-products> <a id=sto-black-friday-banner-photo-link href=\"https://www.lemanegeabijoux.com/140-bijoux-black-friday?utm_source=Galec&utm_medium=banner&utm_campaign=black_friday_Galec&utm_content=jachete\" onclick='dataLayer.push({nomcampagne:\"blackfriday2019\",nomclicsortant:\"hp_bijoux1_manege-a-bijoux-1_-_sur_une_selection_de_bijoux\",urlsortie:\"https://www.lemanegeabijoux.com/140-bijoux-black-friday?utm_source=Galec&utm_medium=banner&utm_campaign=black_friday_Galec&utm_content=jachete \",event:\"blackfriday_traffickingexterne_visuels\"})' target=_blank><div class=sto-black-friday-content-banner-hp></div></a> <a id=sto-black-friday-banner-photo-link href=\"https://www.lemanegeabijoux.com/115-bijoux-black-friday?utm_source=Galec&utm_medium=banner&utm_campaign=black_friday_25&utm_content=jachete\" onclick='dataLayer.push({nomcampagne:\"blackfriday2019\",nomclicsortant:\"hp_bijoux2_manege-a-bijoux-2_-_sur_toures_les_bagues-_boucles_d-oreilles\",urlsortie:\"https://www.lemanegeabijoux.com/115-bijoux-black-friday?utm_source=Galec&utm_medium=banner&utm_campaign=black_friday_25&utm_content=jachete\",event:\"blackfriday_traffickingexterne_visuels\"})' target=_blank><div class=sto-black-friday-content-banner-hp-2></div></a> </div> </div> </div> <div id=sto-back-to-top> Retour en haut</div> </div> </section> <section class=sto-black-friday-category-page category=\"\" style=display:none> <div class=sto-category-banner-cta-mob></div> <div class=sto-category-page-banner> <div class=sto-category-banner-cta></div> </div> <div class=sto-category-page-header></div> <div class=sto-category-page-top> <div class=sto-category-page-filters> <div class=sto-subcategory-filters> <div class=sto-subcategory-filter-current> <div class=sto-current-subcategory-filter>Toutes les catégories</div> </div> <div class=sto-subcategory-filter-list> </div> </div> <div class=sto-price-filters> <div class=sto-price-filter-current> <div class=sto-current-price-filter>Prix par ordre croissant</div> </div> <div class=sto-price-filter-list> <div class=\"sto-category-page-filter-price active\" data-sort=ASC>Prix par ordre croissant</div> <div class=sto-category-page-filter-price data-sort=DESC>Prix par ordre décroissant</div> </div> </div> </div> <div class=\"sto-pagination-top sto-category-page-pagination\"></div> <div style=clear:both></div> </div> <div class=sto-category-page-products></div> <div class=sto-category-page-mentions>TOUS NOS BIJOUX SONT GARANTIS 2 ANS CONTRE TOUS DÉFAUTS OU VICES CACHÉS, L’UTILISATION ANORMALE OU L’USURE NE SONT PAS INCLUSES DANS LA GARANTIE. Bijoux agrandis de 1 à 2,2 fois. Prix valables du <span>20/11/2018 au 24/11/2018</span>. Prix variables suivant le poids, le cours de l’or, le prix et le poids des pierres précieuses. Poids métal pouvant varier en raison du caractère artisanal de la fabrication. Prix de vente indicatif maximum pour chaque dimension indiquée. Pour tout achat d’une bague en or, la 1ère mise à taille est gratuite dans tous les espaces «Le Manège à Bijoux » (selon modèles) jusqu’à une différence de 3 tailles (en agrandissement ou en réduction) dans un délai de 6 mois à compter de la date d’achat. RCS 339 160 848 - Photos non contractuelles. Les offres de cette page sont disponibles dans tous les magasins disposant d’un espace « Le Manège à Bijoux » et participant à l’opération. Dans la limite des stocks disponibles.</div> <div class=sto-category-page-bottom> <div class=\"sto-pagination-bottom sto-category-page-pagination\"></div> </div> </section> <div class=sto-black-friday-storage style=display:none> <div id=sto-prod-storage-hightech></div> <div id=sto-prod-storage-maison></div> <div id=sto-prod-storage-culture></div> <div id=sto-prod-storage-parapharmacie></div> <div id=sto-prod-storage-mode></div> <div id=sto-prod-storage-vin></div> <div id=sto-prod-storage-alimentaire></div> <div id=sto-prod-storage-bijoux></div> <div id=sto-prod-storage-slider></div> </div> <div id=sto-mention-legal> L'ABUS D'ALCOOL EST DANGEREUX POUR LA SANTÉ. À CONSOMMER AVEC MODÉRATION. LA LOI INTERDIT LA VENTE D'ALCOOL AUX MINEURS. DES CONTRÔLES SERONT RÉALISÉS EN CAISSE </div> <div id=return-to-top> <div class=icon-arrow-down></div> </div> </div> ";

/***/ }),
/* 190 */
/***/ (function(module, exports) {

	module.exports = "<div class=\"box-item box-item-rayon\" data-id=\"\" data-prod-ind=1 style=display:block> <div class=box-item-top> <div class=item-illustration> <a href=\"\"> <img alt=\"\" class=img-responsive src=http://via.placeholder.com/198x130> </a> </div> <div class=pricer> <div class=sto-final-price></div> <div class=sto-scratched-price></div> </div> </div> <div class=content ng-controller=ListeCoursesFragmentMEAProduitController ng-init=\"\"> <div class=box-item-title> <a href=\"\">CÉRÉALES “CRUESLI DE QUAKER”</a> </div> <input ng-value=quantite type=hidden> <button class=sto-btn-jachete data-href=\"\">j'achète</button> </div> </div> ";

/***/ }),
/* 191 */
/***/ (function(module, exports) {

	module.exports = "<div><div class=sto-product-container></div></div>";

/***/ }),
/* 192 */
/***/ (function(module, exports) {

	module.exports = "<div><div class=sto-product-container></div></div>";

/***/ }),
/* 193 */
/***/ (function(module, exports) {

	module.exports = "<div><div class=sto-product-container></div></div>";

/***/ }),
/* 194 */
/***/ (function(module, exports) {

	module.exports = "<div><div class=sto-product-container></div></div>";

/***/ }),
/* 195 */
/***/ (function(module, exports) {

	module.exports = "<div><div class=sto-product-container></div></div>";

/***/ }),
/* 196 */
/***/ (function(module, exports) {

	module.exports = "<div><div class=sto-product-container></div></div>";

/***/ }),
/* 197 */
/***/ (function(module, exports) {

	module.exports = {"retailer":"leclerc","crawl":"/catalogue/evenements/black-friday/selection","crawlTest":"/catalogue/evenements/black-friday-test-crawl-portail","name":"black-friday","format":"TG","live":true,"devPath":"//localhost:8080/dist/","livePath":"//rscdn.storetail.net/lcl-portal/black_friday_2019/","products_enmagasin":[],"universe":{"hightech":["Informatique Tablette","Smartphone Téléphonie, GPS","TV Photo, Son","Objets Connectés Jouets, Mobilité"],"maison":["Petit électroménager Cuisine","Entretien et soin de la maison","Beauté santé","Maison","Jardin & animalerie","Loisirs & jeux","Bricolage"],"parapharmacie":["Visage","Corps","Hygiène","Cheveux","Santé, Bien être","Naturel et Bio","Maman et Bébé","Maquillage","Minceur"],"culture":["Livre","Musique & Streaming","Vidéo","Jeux Vidéo","Jouets","Coffrets cadeaux","Billeterie","e-librairie"],"mode":["Tous les sports","Sportswear","Accessoires","Mode"],"vin":["Vins","Champagnes & effervescents","Spiritueux","Bières"],"alimentaire":["Epicerie"],"bijoux":["Bijoux"],"all":["Informatique Tablette","Smartphone Téléphonie, GPS","TV Photo, Son","Objets Connectés Jouets, Mobilité","Petit électroménager Cuisine","Entretien soin de la maison","Beauté santé","Maison","Jardin & animalerie","Loisiers & jeux","Bricolage","Visage","Corps","Hygiène","Cheveux","Santé, Bien être","Naturel et Bio","Maman et Bébé","Maquillage","Minceur","Livre","Musique & Streaming","Vidéo","Jeux Vidéo","Jouets","Coffrets cadeaux","Billeterie","e-librairie","Tous les sports","Sportswear","Accessoires","Mode","Vins","Champagnes & effervescents","Spiritueux","Bières","Epicerie","Bijoux"]},"hightech_real_subcat":["informatique_tablette","smartphone_telephonie_gps","tv_photo_son","objets_connectes_jouets_mobilite","objets_co_mobilite_jouets"],"subcategories_aliases":{"loisirs_jeux":["Loisirs et jeux"],"sante_bien_etre":["Santé"],"maquillage":["Beauté"],"maman_et_bebe":["Bébé et Maman"],"naturel_et_bio":["Bio/Nature"],"objets_connectes_jouets_mobilite":["Objets Co Mobilité Jouets"],"petit_electromenager_cuisine":["Petit électro Cuisine"],"entretien_et_soin_de_la_maison":["Entretien soin de la maison"],"musique_streaming":["Musique"]},"all_categories":["hightech","maison","parapharmacie","culture","mode","vin","alimentaire","bijoux"],"all_products_categories":{"High-Tech":"hightech","Jardin, Maison & Électroménager":"maison","Culture & Jouets":"culture","Parapharmacie":"parapharmacie","Mode & Sport":"mode","Vins & Alcools":"vin","Épicerie Fine":"alimentaire","Bijoux":"bijoux"},"banners":{"hightech":[{"date":"all","message":"Jusqu'à 35% sur une sélection de PC portables","url":"https://www.hightech.leclerc/Black_Friday_PC_portables-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-S48-BlackFriday&utm_content=Banniere_generique_pc_portable"},{"date":"all","message":"Jusqu'à 30% sur une sélection de smartphones","url":"https://www.hightech.leclerc/Black_Friday_smartphone-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-S48-BlackFriday&utm_content=Banniere_generique_smartphone"},{"date":"all","message":"Jusqu'à 40% sur une sélection de produits électroménager","url":"https://www.hightech.leclerc/boutique-black-friday-electromenager-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-S48-BlackFriday&utm_content=Banniere_generique_electromenager"},{"date":"all","message":"Jusqu'à 20% sur une sélection de TV","url":"https://www.hightech.leclerc/black-friday-tv-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-S48-BlackFriday&utm_content=Banniere_generique_tv"}],"maison":[{"date":"all","message":"Nettoyeur haute pression électrique One Extra 135 - LAVOR 49€45 au lieu de 109€90 (-55%BRII)","url":"https://www.maisonetloisirs.leclerc/nettoyeur-haute-pression-electrique-one-extra-135?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-BlackFriday&utm_content=Nettoyeur-haute-pression"},{"date":"all","message":"Jusqu'à -70€ sur une sélection de matelas","url":"https://www.maisonetloisirs.leclerc/black-friday-matelas?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-BlackFridayRubrique&utm_content=Canape"},{"date":"all","message":"ASPIRATEUR BALAI ROWENTA AIR FORCE 460 à 299€ - 100€ = 199€","url":"https://www.hightech.leclerc/entretien-et-soin-de-la-maison-u/aspirateur-u/aspirateur-balai-u/aspirateur-balai-rowenta-air-force-all-in-one-3221614003158-pr?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-S48-BlackFriday&utm_content=ASPIRATEUR_BALAI_ROWENTA_AIR_FORCE"},{"date":"all","message":"Jusqu'à -40% sur une sélection de produits électroménager","url":"https://www.hightech.leclerc/boutique-black-friday-electromenager-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-S48-BlackFriday&utm_content=Banniere_generique_electromenager"}],"parapharmacie":[{"date":"all","message":"Jusqu'à -20% de remise immédiate sur notre sélection Bioderma","url":"https://www.parapharmacie.leclerc/promotions/?pertimm_marque=BIODERMA&utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-banniere-black-friday&utm_content=bioderma"},{"date":"all","message":"Jusqu'à -25% de remise immédiate sur Bio beauté by Nuxe et sur notre sélection Nuxe","url":"https://www.parapharmacie.leclerc/promotions/?pertimm_marque=NUXE&utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-banniere-black-friday&utm_content=nuxe"},{"date":"all","message":"Jusqu'à -40% de remise immédiate sur notre sélection Bébé","url":"https://www.parapharmacie.leclerc/selection/black-friday-parapharmacie/bebe?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-banniere-black-friday&utm_content=bebe"},{"date":"all","message":"Découvrez nos promotions sur une sélection de produits cadeaux et beauté","url":"https://www.parapharmacie.leclerc/selection/black-friday-parapharmacie/cadeaux-et-beaute?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-banniere-black-friday&utm_content=cadeaux-beaute"}],"culture":[{"date":"all","message":"Prix chocs sur une sélection de jeux, consoles & accessoires","url":"https://www.culture.leclerc/jeux-video-u/black-friday-jeux-video-u?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=banniere-rubrique-jeu-video"},{"date":"all","message":"-50% sur la collection Barbie et Ken métiers de rêve","url":"https://www.culture.leclerc/jouets-u/selection-barbie-black-friday-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=banniere-rubrique-jouets"},{"date":"all","message":"2 albums achetés = 1 album offert* !","url":"https://www.culture.leclerc/2000000193649-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=banniere-rubrique-musique"},{"date":"all","message":"Livres neufs à prix réduits : À partir de -50% ","url":"https://www.culture.leclerc/livre-u/black-friday-livre-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=banniere-rubrique-livres"}],"mode":[{"date":"all","message":"Jusqu'à -50% sur le textile et les chaussures ","url":"https://www.sport.leclerc/toutes-les-offres?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-06-11-black-friday-sport&utm_content=banniere-portail-black-friday-sport"},{"date":"all","message":"Jusqu'à 100 € de remise sur la gamme électrique","url":"https://www.sport.leclerc/velo-electrique-35?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-06-11-black-friday-sport&utm_content=banniere-rubrique-black-friday"},{"date":"all","message":"jusqu'à 700 € de remise sur le matériel de fitness ","url":"https://www.sport.leclerc/marques-sport/Care/1/5411?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-06-11-black-friday-sport&utm_content=banniere-rubrique-black-friday"},{"date":"all","message":"100 euros offerts sur le produit black Friday : Trampoline 300 CM Kangui","url":"https://www.sport.leclerc/trampoline-300-cm-filet-couverture-de-proprete-echelle?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-06-11-black-friday-sport&utm_content=banniere-rubrique-black-friday"}],"vin":[{"date":"all","message":"Jusqu'à 10% de remise immédiate sur notre sélection de vins","url":"https://www.macave.leclerc/black-week?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=banniere-rubrique-generique"},{"date":"all","message":"34% de remise immédiate sur le charles de cazanove - cuvée spéciale cazanova","url":"https://www.macave.leclerc/charles-de-cazanove-cuvee-speciale-cazanova-champagne-effervescent?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-27-black-friday&utm_content=banniere-rubrique-produit4"}],"alimentaire":[],"bijoux":[],"bijoux2":[],"bijoux3":[],"jeux_video":[],"musique_streaming":[],"livre":[],"video":[],"jouets":[]},"btn_products":{"3614703496458":"BOTH","3614703496489":"BOTH","3614703496496":"BOTH","3614703496502":"BOTH","3614703496519":"BOTH","3614703496526":"BOTH","3614703496533":"BOTH","3760165464969":"BOTH","3347260903120":"BOTH","8015244200071":"BOTH","3760256554227":"BOTH","3700691412065":"BOTH","5702016111910":"BOTH","3262190412866":"BOTH","3459223545923":"BOTH","4008789050137":"BOTH","0887961368079":"BOTH","0193575000213":"BOTH","0842776102379":"BOTH","6925281937248":"BOTH","6925281925047":"BOTH","7340055352369":"BOTH","8801643812195":"BOTH","0193638727392":"BOTH","4718017428347":"BOTH","4710180523133":"BOTH","0193386609865":"BOTH","4718017428378":"BOTH","3700062636311":"BOTH","8801643695101":"BOTH","6943279420428":"BOTH","8801643829834":"BOTH","3614704972876":"BOTH","6941059620648":"BOTH","8801643649210":"BOTH","0194441170290":"BOTH","4549292062755":"BOTH","0734646679862":"BOTH","7636490063435":"BOTH","0843591083256":"BOTH","5901292511456":"BOTH","8806098387885":"BOTH","5901292513979":"BOTH","8806098424795":"BOTH","5901292513986":"BOTH","8806090173752":"BOTH","8806090169229":"BOTH","5055862314319":"BOTH","8801643730383":"BOTH","8801643741518":"BOTH","0818279022995":"BOTH","8714574659992":"BOTH","6901443290840":"BOTH","3660767966819":"BOTH","3221614003158":"BOTH","5060629980027":"BOTH","8016361938892":"BOTH","3045386358564":"BOTH","3121040073148":"BOTH","5035048666036":"BOTH","5012512164847":"BOTH","8710103799771":"BOTH","8710103921103":"BOTH"}}

/***/ }),
/* 198 */
/***/ (function(module, exports, __webpack_require__) {

	var refs = 0;
	var dispose;
	var content = __webpack_require__(27);
	if(typeof content === 'string') content = [[module.id, content, '']];
	exports.use = exports.ref = function() {
		if(!(refs++)) {
			exports.locals = content.locals;
			dispose = __webpack_require__(26)(content, {});
		}
		return exports;
	};
	exports.unuse = exports.unref = function() {
		if(!(--refs)) {
			dispose();
			dispose = null;
		}
	};
	if(false) {
		var lastRefs = module.hot.data && module.hot.data.refs || 0;
		if(lastRefs) {
			exports.ref();
			if(!content.locals) {
				refs = lastRefs;
			}
		}
		if(!content.locals) {
			module.hot.accept();
		}
		module.hot.dispose(function(data) {
			data.refs = content.locals ? 0 : refs;
			if(dispose) {
				dispose();
			}
		});
	}

/***/ }),
/* 199 */
/***/ (function(module, exports, __webpack_require__) {

	var refs = 0;
	var dispose;
	var content = __webpack_require__(28);
	if(typeof content === 'string') content = [[module.id, content, '']];
	exports.use = exports.ref = function() {
		if(!(refs++)) {
			exports.locals = content.locals;
			dispose = __webpack_require__(26)(content, {});
		}
		return exports;
	};
	exports.unuse = exports.unref = function() {
		if(!(--refs)) {
			dispose();
			dispose = null;
		}
	};
	if(false) {
		var lastRefs = module.hot.data && module.hot.data.refs || 0;
		if(lastRefs) {
			exports.ref();
			if(!content.locals) {
				refs = lastRefs;
			}
		}
		if(!content.locals) {
			module.hot.accept();
		}
		module.hot.dispose(function(data) {
			data.refs = content.locals ? 0 : refs;
			if(dispose) {
				dispose();
			}
		});
	}

/***/ }),
/* 200 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	//console.log('test');
	try {
	    var sto = window.__sto,
	        $ = window.jQuery,
	        settings = __webpack_require__(197),
	        placeholder = settings.name,
	        format = settings.format,
	        style = __webpack_require__(199),
	        html = __webpack_require__(189),
	        gutters = __webpack_require__(188),
	        gutters_style = __webpack_require__(198),
	        product_box = __webpack_require__(190),
	        prods_hightech = __webpack_require__(25),
	        prods_maison = __webpack_require__(194),
	        prods_parapharmacie = __webpack_require__(196),
	        prods_culture = __webpack_require__(193),
	        prods_mode = __webpack_require__(195),
	        prods_vins = __webpack_require__(25),
	        prods_alimentaire = __webpack_require__(191),
	        prods_bijoux = __webpack_require__(192),
	        container = $(html),
	        paginate = __webpack_require__(201),
	        pagination_container,
	        deferred = sto.utils.deferred,
	        sorting = null,
	        subcategory = null,
	        windowSize = window.innerWidth,
	        paginationLimit = 20,
	        paginationBijoux = 19,
	        hpProductLimit = 4,
	        pageNumber = null,
	        crawlPortail;
	    var cosa = [];
	
	    /*var verga = ["194441170290", "3016661150500", "3401360147508", "805278385072500", "0190781396475", "3760165464969", "376007380363802", "8013298204922", "325869133156001", "3401528520846", "842776102379", "3221614003158", "8015244200071", "8806090173752", "4008789050137", "3282779185912", "190781396475", "3760165464969", "5051889488682", "328277932706", "5030945122531", "3700538222734", "6941059620648", "3760226392033", "3700538218874", "4242002853321", "5053083123444", "841011300281500", "366143400296", "190781396475", "3760256554227", "4242002853321", "3700691412065"];
	    var counterverga = 0;
	    var vergafinal = {};*/
	
	    var fontRoboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
	    $('head').first().append(fontRoboto);
	
	    var trim = function(string) {
	            return string.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
	        },
	        cleanStringBlackFriday = function(string) {
	            try {
	                var rule_slash = [/[^a-z0-9_]/g, "-"],
	                    replacements = [
	                        [/[éèëê]/g, "e"],
	                        [/[àáâãäå]/g, "a"],
	                        [/[ìíîï]/g, "i"],
	                        [/[òóôõö]/g, "o"],
	                        [/[ùúûü]/g, "u"],
	                        [/ñ/g, "n"],
	                        [/ç/g, "c"],
	                        [/æ/g, "ae"],
	                        [/œ/g, "oe"],
	                        [/\n\r\t/g, "-"],
	                        [/_/g, "-"],
	                        [/\s/g, "_"],
	                        rule_slash //replace all non ascii characters by an underscore
	                    ],
	                    i = -1,
	                    l = replacements.length;
	                string = string.toLowerCase();
	                string = trim(string);
	
	                while (++i < l) {
	                    string = string.replace(replacements[i][0], replacements[i][1])
	                }
	                return string;
	            } catch (e) {
	                console.log("cleanStringBlackFriday error : " + e)
	            }
	        };
	
	    var getParams = function(url) {
	        var params = {};
	        var parser = document.createElement('a');
	        parser.href = url;
	        var query = parser.search.substring(1);
	        var vars = query.split('&');
	        for (var i = 0; i < vars.length; i++) {
	            var pair = vars[i].split('=');
	            params[pair[0]] = decodeURIComponent(pair[1]);
	        }
	        return params;
	    };
	
	    module.exports = {
	        init: _init_(),
	        styleGutter: style
	    }
	
	    function _init_() {
	        sto.load(format, function(tracker) {
	            var container = $(html),
	                helper_methods = sto.utils.retailerMethod,
	                url_crawling = window.location.host.indexOf("rec.e-leclerc.com") > -1 ? settings.crawlTest : settings.crawl,
	                //url_crawling = settings.crawl,
	                prodsHightech = $(prods_hightech),
	                prodsMaison = $(prods_maison),
	                prodsParapharmacie = $(prods_parapharmacie),
	                prodsCulture = $(prods_culture),
	                prodsMode = $(prods_mode),
	                prodsVins = $(prods_vins),
	                prodsAlimentaire = $(prods_alimentaire),
	                prodsBijoux = $(prods_bijoux);
	
	            $('.page-container .main-content').append(gutters);
	            $('.row[layout\\:fragment="pagecontent"]').empty();
	            $('.row[layout\\:fragment="pagecontent"]').append(container);
	
	            style.use();
	            gutters_style.use();
	            resize_gutters();
	
	            helper_methods.crawl(url_crawling).promise.then(function(d) {
	                crawlPortail = d;
	
	                var objHightech = {};
	                var objMaison = {};
	                var objParapharmacie = {};
	                var objCulture = {};
	                var objMode = {};
	                var objVins = {};
	
	                $.each(d, function(i, v) {
	                    var productBox = v.bloc,
	                        productData = (productBox.find('#btn-ajout-liste'), v.data),
	                        productUrl = productData.urlMarchand,
	                        productId = productData.techid,
	                        productName = productData.libelle,
	                        productPrice = productData.prixFinal,
	                        productCategory = productData.codeUnivers,
	                        productSubcategory = productData.codeSousUnivers;
	
	                    if (productPrice && "" !== productPrice) {
	                        productPrice = productPrice.toString();
	                        var g = productPrice.slice(-2),
	                            y = productPrice.slice(0, -2);
	                        productPrice = y + "-" + g
	                    }
	
	                    v.bloc.attr('data-trackurl', productUrl);
	                    v.bloc.attr('data-trackid', productId);
	                    v.bloc.attr('data-trackname', cleanStringBlackFriday(productName));
	                    v.bloc.attr('data-trackprice', productPrice);
	                    v.bloc.attr('data-trackcategory', cleanStringBlackFriday(productCategory));
	                    v.bloc.attr('data-tracksubcategory', cleanStringBlackFriday(productSubcategory));
	
	                    if (v.univers === "Maison, jardin & Electroménager" || v.univers === "Maison, Jardin & électroménager" || v.data.codeUnivers === "maison" || v.data.codeUnivers === "brico") {
	                        objMaison[i] = v;
	                    } else if (v.univers === "Culture & Jouets" || v.data.codeUnivers === "culture") {
	                        objCulture[i] = v;
	                    } else if (v.univers === "High Tech" || v.data.codeUnivers === "multimedia") {
	                        objHightech[i] = v;
	                    } else if (v.univers === "Parapharmacie") {
	                        objParapharmacie[i] = v;
	                    } else if (v.univers === "Mode & Sport" || v.univers === "Mode & sport") {
	                        objMode[i] = v;
	                    } else if (v.univers === "Vins & alcools") {
	                        objVins[i] = v;
	                    }
	                });
	
	                try {
	                    helper_methods.createFormat(prodsHightech, tracker.copy().update({
	                        "trackZone": "HighTech"
	                    }), Object.keys(objHightech), objHightech, 1000);
	                } catch (e) {}
	                try {
	                    helper_methods.createFormat(prodsMaison, tracker.copy().update({
	                            "trackZone": "Maison"
	                        }),
	                        Object.keys(objMaison), objMaison, 1000);
	                } catch (e) {}
	                try {
	                    helper_methods.createFormat(prodsCulture, tracker.copy().update({
	                        "trackZone": "Culture"
	                    }), Object.keys(objCulture), objCulture, 1000);
	                } catch (e) {}
	                try {
	                    helper_methods.createFormat(prodsParapharmacie, tracker.copy().update({
	                        "trackZone": "Parapharmacie"
	                    }), Object.keys(objParapharmacie), objParapharmacie, 1000);
	                } catch (e) {}
	
	                try {
	                    helper_methods.createFormat(prodsVins, tracker.copy().update({
	                        "trackZone": "Vins"
	                    }), Object.keys(objVins), objVins, 1000);
	                } catch (e) {}
	                try {
	                    helper_methods.createFormat(prodsMode, tracker.copy().update({
	                        "trackZone": "Mode"
	                    }), Object.keys(objMode), objMode, 1000);
	                } catch (e) {}
	
	                var promises_crawl = [];
	                var crawls_data = [{
	                        "tz": "HighTech",
	                        "shop": prodsHightech,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[High-Tech]=BLACK_FRIDAY&format=json"
	                    },
	                    {
	                        "tz": "HighTech",
	                        "shop": prodsHightech,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[High-Tech]=BLACK_FRIDAY&format=json"
	                    },
	                    {
	                        "tz": "HighTech",
	                        "shop": prodsHightech,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[High-Tech]=BLACK_FRIDAY&format=json"
	                    },
	                    {
	                        "tz": "HighTech",
	                        "shop": prodsHightech,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[High-Tech]=BLACK_FRIDAY&format=json"
	                    },
	                    {
	                        "tz": "HighTech",
	                        "shop": prodsHightech,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[High-Tech]=BLACK_FRIDAY&format=json"
	                    },
	                    {
	                        "tz": "HighTech",
	                        "shop": prodsHightech,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[High-Tech]=BLACK_FRIDAY&format=json"
	                    },
	                    {
	                        "tz": "Maison",
	                        "shop": prodsMaison,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[test]=blackfridaymaisonetloisirs&format=json"
	                    },
	                    {
	                        "tz": "Maison",
	                        "shop": prodsMaison,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[test]=blackfridaymaisonetloisirs&format=json"
	                    },
	                    {
	                        "tz": "Maison",
	                        "shop": prodsMaison,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[test]=blackfridaymaisonetloisirs&format=json"
	                    },
	                    {
	                        "tz": "Maison",
	                        "shop": prodsMaison,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[test]=blackfridaymaisonetloisirs&format=json"
	                    },
	                    {
	                        "tz": "Maison",
	                        "shop": prodsMaison,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[test]=blackfridaymaisonetloisirs&format=json"
	                    },
	                    {
	                        "tz": "Maison",
	                        "shop": prodsMaison,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[test]=blackfridaymaisonetloisirs&format=json"
	                    },
	                    {
	                        "tz": "Maison",
	                        "shop": prodsMaison,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[High-Tech]=BLACK_FRIDAY_PEM&format=json"
	                    },
	                    {
	                        "tz": "Maison",
	                        "shop": prodsMaison,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[High-Tech]=BLACK_FRIDAY_PEM&format=json"
	                    },
	                    {
	                        "tz": "Maison",
	                        "shop": prodsMaison,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[High-Tech]=BLACK_FRIDAY_PEM&format=json"
	                    },
	                    {
	                        "tz": "Maison",
	                        "shop": prodsMaison,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[High-Tech]=BLACK_FRIDAY_PEM&format=json"
	                    },
	                    {
	                        "tz": "Maison",
	                        "shop": prodsMaison,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[High-Tech]=BLACK_FRIDAY_PEM&format=json"
	                    },
	                    {
	                        "tz": "Maison",
	                        "shop": prodsMaison,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[High-Tech]=BLACK_FRIDAY_PEM&format=json"
	                    },
	                    {
	                        "tz": "Culture",
	                        "shop": prodsCulture,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[Tous]=promotion_black_friday&format=json"
	                    },
	                    {
	                        "tz": "Culture",
	                        "shop": prodsCulture,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[Tous]=promotion_black_friday&format=json"
	                    },
	                    {
	                        "tz": "Culture",
	                        "shop": prodsCulture,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[Tous]=promotion_black_friday&format=json"
	                    },
	                    {
	                        "tz": "Culture",
	                        "shop": prodsCulture,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[Tous]=promotion_black_friday&format=json"
	                    },
	                    {
	                        "tz": "Culture",
	                        "shop": prodsCulture,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[Tous]=promotion_black_friday&format=json"
	                    },
	                    {
	                        "tz": "Culture",
	                        "shop": prodsCulture,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[Tous]=promotion_black_friday&format=json"
	                    },
	                    {
	                        "tz": "Culture",
	                        "shop": prodsCulture,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=600&contexts[Tous]=promotion_black_friday&format=json"
	                    },
	                    {
	                        "tz": "Culture",
	                        "shop": prodsCulture,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=700&contexts[Tous]=promotion_black_friday&format=json"
	                    },
	                    {
	                        "tz": "Parapharmacie",
	                        "shop": prodsParapharmacie,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[test]=boutique_para_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Parapharmacie",
	                        "shop": prodsParapharmacie,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[test]=boutique_para_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Parapharmacie",
	                        "shop": prodsParapharmacie,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[test]=boutique_para_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Parapharmacie",
	                        "shop": prodsParapharmacie,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[test]=boutique_para_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Parapharmacie",
	                        "shop": prodsParapharmacie,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[test]=boutique_para_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Parapharmacie",
	                        "shop": prodsParapharmacie,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[test]=boutique_para_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Mode",
	                        "shop": prodsMode,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[test]=boutique_sport_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Mode",
	                        "shop": prodsMode,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[test]=boutique_sport_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Mode",
	                        "shop": prodsMode,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[test]=boutique_sport_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Mode",
	                        "shop": prodsMode,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[test]=boutique_sport_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Mode",
	                        "shop": prodsMode,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[test]=boutique_sport_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Mode",
	                        "shop": prodsMode,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[test]=boutique_sport_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Vins",
	                        "shop": prodsVins,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=0&contexts[test]=boutique_macave_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Vins",
	                        "shop": prodsVins,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=100&contexts[test]=boutique_macave_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Vins",
	                        "shop": prodsVins,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=200&contexts[test]=boutique_macave_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Vins",
	                        "shop": prodsVins,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=300&contexts[test]=boutique_macave_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Vins",
	                        "shop": prodsVins,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=400&contexts[test]=boutique_macave_blackfriday&format=json"
	                    },
	                    {
	                        "tz": "Vins",
	                        "shop": prodsVins,
	                        "qty_prod": "1000",
	                        "crawl": "https://galec-ecommerce-prod.pertimm.net/001-galec-ecom/api/?action=pertimmSearch&max_files=100&res_sf=500&contexts[test]=boutique_macave_blackfriday&format=json"
	                    }
	                ];
	
	                crawls_data.forEach(function(crawl_data) {
	                    var def = deferred();
	
	                    helper_methods.crawlAPI(crawl_data.crawl).promise.then(function(d) {
	
	                        /*console.log(crawl_data.tz, d);
	                        $.each(d, function(i, v) {
	                            //if (crawl_data.tz == "Parapharmacie") {
	                                cosa.push(i);
	                            //}
	                        });
	                        console.log(cosa+"");*/
	
	                        //Remove repated supprimer doublons
	                        /*$.each(d, function(i, v) {
	                            if (crawlPortail[i]) {
	                                delete d[i];
	                            }
	                        });*/
	
	                        $.each(crawlPortail, function(i, v) {
	                            //console.log('i',i);
	                            //console.log('crawlPortail[i]["data"]["ean"]', crawlPortail[i]["data"]["ean"]);
	                            //console.log(d.hasOwnProperty(crawlPortail[i]["data"]["ean"]));
	                            if (d.hasOwnProperty(crawlPortail[i]["data"]["ean"])) {
	                                delete d[crawlPortail[i]["data"]["ean"]];
	                            }
	                        });
	
	                        if (crawl_data.tz == "HighTech") {
	                            $.each(d, function(i, v) {
	                                var c = JSON.parse(v.category_1);
	                                var catNb = 0;
	                                $.each(settings.hightech_real_subcat, function(i, value) {
	                                    c = c.map(sto.utils.fn.cleanString);
	                                    if (c.indexOf(value) !== -1) {
	                                        catNb++;
	                                    }
	                                });
	                                if (catNb === 0) {
	                                    delete d[i];
	                                }
	                            });
	                        }
	                        def.resolve({
	                            "result": d,
	                            "data": crawl_data
	                        });
	                    });
	                    promises_crawl.push(def.promise);
	                });
	
	                deferred.all(promises_crawl).then(function(crawls_promise) {
	
	                    crawls_promise.forEach(function(crawl_promise) {
	                        try {
	                            helper_methods.createFormatAPI(crawl_promise.data.shop, tracker.copy().update({
	                                "trackZone": crawl_promise.data.tz
	                            }), Object.keys(crawl_promise.result), crawl_promise.result, crawl_promise.data.qty_prod);
	                        } catch (e) {}
	                    });
	
	                    // exception for Mode & Sport products, append portal product after API products
	                    try {
	                        helper_methods.createFormat(prodsMode, tracker.copy().update({
	                            "trackZone": "Mode"
	                        }), Object.keys(objMode), objMode, 1000);
	                    } catch (e) {}
	                    try {
	                        helper_methods.createFormat(prodsMaison, tracker.copy().update({
	                            "trackZone": "Maison"
	                        }), Object.keys(objMaison), objMaison, 1000);
	                    } catch (e) {}
	
	                    $('.sto-' + placeholder + '-loader').hide();
	
	                    $('.page-container .main-content').append(gutters);
	                    $('.row[layout\\:fragment="pagecontent"]').empty();
	                    $('.row[layout\\:fragment="pagecontent"]').append(container);
	
	                    style.use();
	                    gutters_style.use();
	
	                    var today = new Date(),
	                        date1 = new Date(2019, 11, 2),
	                        date2 = new Date(2019, 11, 3);
	
	                    //TEST
	                    /*var today = new Date(),
	                        date1 = new Date(2019, 10, 20, 14),
	                        date2 = new Date(2019, 10, 20, 18),
	                        date3 = new Date(2019, 10, 21, 6);*/
	
	
	                    $('.sto-' + placeholder + '-global').attr('date', today.getFullYear() + '-' + (today.getMonth() + 1) + "-" + (today.getDate()));
	
	                    if (today < date1) {
	                        $('.sto-' + placeholder + '-nuit-blanche').attr("bg", "0");
	                    } else if (today >= date1) {
	                        $('.sto-' + placeholder + '-nuit-blanche').attr("bg", "1");
	                    } else if (today >= date2) {
	                        $('.sto-' + placeholder + '-nuit-blanche').attr("bg", "0");
	                    }
	
	                    if (today >= date1 && today < date2) {
	                        $('#link-nuitblanche').attr('target', '_blank');
	                        $('#link-nuitblanche').attr('href', "https://www.e-leclerc.com/catalogue/evenements/black-friday/nuit-blanche");
	                        $('#link-nuitblanche').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': 'cyber_monday', 'urlsortie': 'http---www-e-leclerc-com-catalogue-evenements-nuit-blanche', 'event': 'blackfriday_traffickingexterne_visuels'});")
	                        $('#link-nuitblanche').removeAttr('style');
	                    }
	
	                    $('#sto-prod-storage-hightech').append(prodsHightech);
	                    $('#sto-prod-storage-maison').append(prodsMaison);
	                    $('#sto-prod-storage-parapharmacie').append(prodsParapharmacie);
	                    $('#sto-prod-storage-culture').append(prodsCulture);
	                    $('#sto-prod-storage-mode').append(prodsMode);
	                    $('#sto-prod-storage-vin').append(prodsVins);
	
	                    $('.sto-product-container>.box-item.box-item--achat-ligne').each(function(i, v) {
	                        var elem = $(v);
	                        tracking_products_portal(elem);
	                    });
	
	                    $('.sto-product-container>.ng-scope').each(function(i, v) {
	                        var elem = $(v);
	                        tracking_products_api(elem);
	                    });
	
	                    // btn memos + buy
	                    $('.box-item.box-item-rayon').each(function() {
	                        var elem = $(this);
	
	                        if (elem.find('.btn.btn-memo').length) {
	                            if (elem.find('.btn.btn-achat-ligne').length === 0)
	                                elem.addClass('sto-en-magasin');
	                        } else {
	                            elem.addClass('sto-exclu-web');
	                        }
	                    });
	
	                    $.each(settings.btn_products, function(i, v) {
	                        var item = $('div[data-id="' + i + '"]');
	
	                        if (item.find('.box-item').length) {
	                            item.find('.box-item').removeClass('sto-exclu-web');
	                        } else {
	                            item.removeClass('sto-exclu-web');
	                        }
	                    });
	
	                    $('#sto-prod-storage-parapharmacie').find('.sto-exclu-web').removeClass('sto-exclu-web');
	
	                    $('.box-item').find('a:not(.btn)').attr('target', "_blank");
	
	                    checkWindowSize();
	                    goUpBtn();
	
	                    // get first 4 products for each universe
	                    $('#sto-prod-storage-hightech').find('.box-item.box-item-rayon').each(function(i, v) {
	                        if ($('#sto-section-hightech .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
	                            if (i == 0) {
	                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
	                            } else {
	                                $(v).attr('data-ind', (i + 1));
	                            }
	                            $('#sto-section-hightech .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
	                        } else {
	                            return false;
	                        }
	                    });
	                    $('#sto-prod-storage-maison').find('.box-item.box-item-rayon').each(function(i, v) {
	                        if ($('#sto-section-maison .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
	                            if (i == 0) {
	                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
	                            } else {
	                                $(v).attr('data-ind', (i + 1));
	                            }
	                            $('#sto-section-maison .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
	                        } else {
	                            return false;
	                        }
	                    });
	                    $('#sto-prod-storage-culture').find('.box-item.box-item-rayon').each(function(i, v) {
	                        if ($('#sto-section-culture .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
	                            if (i == 0) {
	                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
	                            } else {
	                                $(v).attr('data-ind', (i + 1));
	                            }
	                            $('#sto-section-culture .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
	                        } else {
	                            return false;
	                        }
	                    });
	                    $('#sto-prod-storage-parapharmacie').find('.box-item.box-item-rayon').each(function(i, v) {
	                        if ($('#sto-section-parapharmacie .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
	                            if (i == 0) {
	                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
	                            } else {
	                                $(v).attr('data-ind', (i + 1));
	                            }
	                            $('#sto-section-parapharmacie .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
	                        } else {
	                            return false;
	                        }
	                    });
	                    $('#sto-prod-storage-mode').find('.box-item.box-item-rayon').each(function(i, v) {
	                        if ($('#sto-section-mode .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
	                            if (i == 0) {
	                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
	                            } else {
	                                $(v).attr('data-ind', (i + 1));
	                            }
	                            $('#sto-section-mode .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
	                        } else {
	                            return false;
	                        }
	                    });
	                    $('#sto-prod-storage-vin').find('.box-item.box-item-rayon').each(function(i, v) {
	                        if ($('#sto-section-vin .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
	                            if (i == 0) {
	                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
	                            } else {
	                                $(v).attr('data-ind', (i + 1));
	                            }
	                            $('#sto-section-vin .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
	                        } else {
	                            return false;
	                        }
	                    });
	                    $('#sto-prod-storage-alimentaire').find('.box-item.box-item-rayon').each(function(i, v) {
	                        if ($('#sto-section-alimentaire .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
	                            if (i == 0) {
	                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
	                            } else {
	                                $(v).attr('data-ind', (i + 1));
	                            }
	                            $('#sto-section-alimentaire .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
	                        } else {
	                            return false;
	                        }
	                    });
	                    $('#sto-prod-storage-bijoux').find('.box-item.box-item-rayon').each(function(i, v) {
	                        if ($('#sto-section-bijoux .sto-' + placeholder + '-prods-slides-wrapper').find('.box-item.box-item-rayon').length < 4) {
	                            if (i == 0) {
	                                $(v).addClass('sto-prods-slide-active').attr('data-ind', (i + 1));
	                            } else {
	                                $(v).attr('data-ind', (i + 1));
	                            }
	                            $('#sto-section-bijoux .sto-' + placeholder + '-prods-slides-wrapper').append($(v).clone(true));
	                        } else {
	                            return false;
	                        }
	                    });
	
	                    // fill header slider with products
	                    var slideDate1 = new Date(2019, 10, 25),
	                        slideDate2 = new Date(2019, 10, 26),
	                        slideDate3 = new Date(2019, 10, 27),
	                        slideDate4 = new Date(2019, 10, 28),
	                        slideDate5 = new Date(2019, 10, 29),
	                        slideDate6 = new Date(2019, 10, 30),
	                        slideDate7 = new Date(2019, 11, 1),
	                        slideDate8 = new Date(2019, 11, 2);
	
	
	                    if (today >= slideDate8) { //2-12
	                        //console.log('slider date 8');
	                        settings.banners.bijoux = settings.banners.bijoux3;
	                        var header1 = $('.sto-product-container').find('div[data-id="193638727392"]').first().clone(true).attr('slider-nav', "PC PORTABLE LENOVO");
	                        if (header1.find('.box-item').length) {
	                            header1 = $('.sto-product-container').find('div[data-id="193638727392"] .box-item').first().clone(true).attr('slider-nav', "PC PORTABLE LENOVO");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header1);
	
	                        var banner2 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='Consoles Xbox One'>Consoles Xbox One</div>");
	                        banner2.attr('data-index', '2');
	                        banner2.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('Consoles Xbox One') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.culture.leclerc/%20jeux-video-u/xbox-one-u/consoles-u/selection-consoles-xbox-one-black-Friday-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-12-02-black-friday&utm_content=slider-hp-consoles-xbox-one") + '", "event":"https://www.culture.leclerc/%20jeux-video-u/xbox-one-u/consoles-u/selection-consoles-xbox-one-black-Friday-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-12-02-black-friday&utm_content=slider-hp-consoles-xbox-one")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner2);
	
	                        var header3 = $('.sto-product-container').find('div[data-id="8718692617841"]').first().clone(true).attr('slider-nav', "MACHINE A GAZEIFIER SODASTREAM");
	                        if (header3.find('.box-item').length) {
	                            header3 = $('.sto-product-container').find('div[data-id="8718692617841"] .box-item').first().clone(true).attr('slider-nav', "MACHINE A GAZEIFIER SODASTREAM");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);
	
	                        var header4 = $('.sto-product-container').find('div[data-id="1168500600"]').first().clone(true).attr('slider-nav', "Trampoline Kangui");
	                        if (header4.find('.box-item').length) {
	                            header4 = $('.sto-product-container').find('div[data-id="1168500600"] .box-item').first().clone(true).attr('slider-nav', "Trampoline Kangui");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);
	
	                        var header5 = $('.sto-product-container').find('div[data-id="0734646679862"]').first().clone(true).attr('slider-nav', "PC PORTABLE");
	                        if (header5.find('.box-item').length) {
	                            header5 = $('.sto-product-container').find('div[data-id="0734646679862"] .box-item').first().clone(true).attr('slider-nav', "PC PORTABLE");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);
	
	                        var header6 = $('.sto-product-container').find('div[data-id="6970244526823"]').first().clone(true).attr('slider-nav', "Trottinette électrique");
	                        if (header6.find('.box-item').length) {
	                            header6 = $('.sto-product-container').find('div[data-id="6970244526823"] .box-item').first().clone(true).attr('slider-nav', "Trottinette électrique");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);
	
	                        fetch_backup_products();
	
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
	                            var item = $(v);
	                            if (i < 4) {
	                                if (item.hasClass('box-item-banner')) {
	                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
	                                }
	                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
	                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
	                            } else {
	                                return false;
	                            }
	                        });
	                    } else if (today >= slideDate7) { //1-12
	                        //console.log('slider date 7');
	                        settings.banners.bijoux = settings.banners.bijoux3;
	
	                        var banner1 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-40% sur tout le site'>-40% sur tout le site</div>");
	                        banner1.attr('data-index', '6');
	                        banner1.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-40% sur tout le site') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner1);
	
	                        var banner2 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='Vinyles à prix choc !'>Vinyles à prix choc !</div>");
	                        banner2.attr('data-index', '1');
	                        banner2.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('Vinyles à prix choc !') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.culture.leclerc/musique-u/black-friday-musique-selection-de-vinyles-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-12-01-black-friday&utm_content=slider-hp-vinyles") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.culture.leclerc/musique-u/black-friday-musique-selection-de-vinyles-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-12-01-black-friday&utm_content=slider-hp-vinyles")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner2);
	
	                        var header3 = $('.sto-product-container').find('div[data-id="359589544483300"]').first().clone(true).attr('slider-nav', "Xls mon action minceur");
	                        if (header3.find('.box-item').length) {
	                            header3 = $('.sto-product-container').find('div[data-id="359589544483300"] .box-item').first().clone(true).attr('slider-nav', "Xls mon action minceur");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);
	
	                        var header4 = $('.sto-product-container').find('div[data-id="6970244526823"]').first().clone(true).attr('slider-nav', "Trottinette électrique");
	                        if (header4.find('.box-item').length) {
	                            header4 = $('.sto-product-container').find('div[data-id="6970244526823"] .box-item').first().clone(true).attr('slider-nav', "Trottinette électrique");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);
	
	                        var header5 = $('.sto-product-container').find('div[data-id="3760256554227"]').first().clone(true).attr('slider-nav', "Trotinette électrique");
	                        if (header5.find('.box-item').length) {
	                            header5 = $('.sto-product-container').find('div[data-id="3760256554227"] .box-item').first().clone(true).attr('slider-nav', "Trotinette électrique");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);
	                        
	                        var header6 = $('.sto-product-container').find('div[data-id="4242002853321"]').first().clone(true).attr('slider-nav', "Robot multifonction");
	                        if (header6.find('.box-item').length) {
	                            header6 = $('.sto-product-container').find('div[data-id="4242002853321"] .box-item').first().clone(true).attr('slider-nav', "Robot multifonction");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);
	
	                        fetch_backup_products();
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
	                            var item = $(v);
	                            if (i < 4) {
	                                if (item.hasClass('box-item-banner')) {
	                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
	                                }
	                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
	                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
	                            } else {
	                                return false;
	                            }
	                        });
	                    } else if (today >= slideDate6) { //30-11
	                        //console.log('slider date 6');
	                        settings.banners.bijoux = settings.banners.bijoux2;
	                        var header1 = $('.sto-product-container').find('div[data-id="1168500240"]').first().clone(true).attr('slider-nav', "SMARTPHONE XIAOMI 32Go");
	                        if (header1.find('.box-item').length) {
	                            header1 = $('.sto-product-container').find('div[data-id="1168500240"] .box-item').first().clone(true).attr('slider-nav', "SMARTPHONE XIAOMI 32Go");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header1);
	
	                        var header2 = $('.sto-product-container').find('div[data-id="3760226392033"]').first().clone(true).attr('slider-nav', "Tick & Box - 100% foot");
	                        if (header2.find('.box-item').length) {
	                            header2 = $('.sto-product-container').find('div[data-id="3760226392033"] .box-item').first().clone(true).attr('slider-nav', "Tick & Box - 100% foot");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header2);
	
	                        var header3 = $('.sto-product-container').find('div[data-id="1168500550"]').first().clone(true).attr('slider-nav', "Nettoyeur électrique");
	                        if (header3.find('.box-item').length) {
	                            header3 = $('.sto-product-container').find('div[data-id="1168500550"] .box-item').first().clone(true).attr('slider-nav', "Nettoyeur électrique");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);
	
	                        var header4 = $('.sto-product-container').find('div[data-id="4242002853321"]').first().clone(true).attr('slider-nav', "Robot multifonction");
	                        if (header4.find('.box-item').length) {
	                            header4 = $('.sto-product-container').find('div[data-id="4242002853321"] .box-item').first().clone(true).attr('slider-nav', "Robot multifonction");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);
	
	                        var header5 = $('.sto-product-container').find('div[data-id="5053083123444"]').first().clone(true).attr('slider-nav', "FAST AND FURIOUS - DVD");
	                        if (header5.find('.box-item').length) {
	                            header5 = $('.sto-product-container').find('div[data-id="5053083123444"] .box-item').first().clone(true).attr('slider-nav', "FAST AND FURIOUS - DVD");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);
	
	                        var banner6 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-40% sur tout le site'>-40% sur tout le site</div>");
	                        banner6.attr('data-index', '6');
	                        banner6.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-40% sur tout le site') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner6);
	
	                        fetch_backup_products();
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
	                            var item = $(v);
	                            if (i < 4) {
	                                if (item.hasClass('box-item-banner')) {
	                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
	                                }
	                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
	                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
	                            } else {
	                                return false;
	                            }
	                        });
	                    } else if (today >= slideDate5) { //29-11
	                        //console.log('slider date 5');
	                        settings.banners.bijoux = settings.banners.bijoux2;
	                        var banner1 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='30% TEL sélection TV'>30% TEL sélection TV</div>");
	                        banner1.attr('data-index', '1');
	                        banner1.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('30% TEL sélection TV') + '", "urlsortie": "' + cleanStringBlackFriday("https://gti3.hightech.leclerc/dynclick/multimedia-eleclerc-com/?ept-publisher=E-Leclerc_portail&ept-name=portail-S47&eurl=https%3A%2F%2Fwww.hightech.leclerc%2Fblack-friday-la-journee-star-television-b%3Futm_source%3DE-Leclerc_portail%26utm_campaign%3DS47%26utm_medium%3Dleclerc%26utm_content%3DBLACKFRIDAYSPECIALTV") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://gti3.hightech.leclerc/dynclick/multimedia-eleclerc-com/?ept-publisher=E-Leclerc_portail&ept-name=portail-S47&eurl=https%3A%2F%2Fwww.hightech.leclerc%2Fblack-friday-la-journee-star-television-b%3Futm_source%3DE-Leclerc_portail%26utm_campaign%3DS47%26utm_medium%3Dleclerc%26utm_content%3DBLACKFRIDAYSPECIALTV")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner1);
	
	                        var banner2 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-40% sur tout le site'>-40% sur tout le site</div>");
	                        banner2.attr('data-index', '2');
	                        banner2.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-40% sur tout le site') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner2);
	
	                        var header3 = $('.sto-product-container').find('div[data-id="5051889488682"]').first().clone(true).attr('slider-nav', "Coffret Harry Potter");
	                        if (header3.find('.box-item').length) {
	                            header3 = $('.sto-product-container').find('div[data-id="5051889488682"] .box-item').first().clone(true).attr('slider-nav', "Coffret Harry Potter");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);
	
	                        var header4 = $('.sto-product-container').find('div[data-id="328277932706000"]').first().clone(true).attr('slider-nav', "Crème bébé Klorane");
	                        if (header4.find('.box-item').length) {
	                            header4 = $('.sto-product-container').find('div[data-id="328277932706000"] .box-item').first().clone(true).attr('slider-nav', "Crème bébé Klorane");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);
	
	                        var header5 = $('.sto-product-container').find('div[data-id="5030945122531"]').first().clone(true).attr('slider-nav', "Fifa 20 PS4");
	                        if (header5.find('.box-item').length) {
	                            header5 = $('.sto-product-container').find('div[data-id="5030945122531"] .box-item').first().clone(true).attr('slider-nav', "Fifa 20 PS4");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);
	
	                        var header6 = $('.sto-product-container').find('div[data-id="3700538222734"]').first().clone(true).attr('slider-nav', "Matelas mousse NATURA");
	                        if (header6.find('.box-item').length) {
	                            header6 = $('.sto-product-container').find('div[data-id="3700538222734"] .box-item').first().clone(true).attr('slider-nav', "Matelas mousse NATURA");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);
	
	                        fetch_backup_products();
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
	                            var item = $(v);
	                            if (i < 4) {
	                                if (item.hasClass('box-item-banner')) {
	                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
	                                }
	                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
	                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
	                            } else {
	                                return false;
	                            }
	                        });
	                    } else if (today >= slideDate4) { //28-11
	                        //console.log('slider date 4');
	                        var header1 = $('.sto-product-container').find('div[data-id="1168500160"]').first().clone(true).attr('slider-nav', "TV LED SAMSUNG");
	                        if (header1.find('.box-item').length) {
	                            header1 = $('.sto-product-container').find('div[data-id="1168500160"] .box-item').first().clone(true).attr('slider-nav', "TV LED LG");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header1);
	
	                        /*var header2 = $('.sto-product-container').find('div[data-id="4008789050137"]').first().clone(true).attr('slider-nav', "Commissariat Playmobil");
	                        if (header2.find('.box-item').length) {
	                            header2 = $('.sto-product-container').find('div[data-id="4008789050137"] .box-item').first().clone(true).attr('slider-nav', "Commissariat Playmobil");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header2);*/
	
	                        var banner3 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-40% sur tout le site'>-40% sur tout le site</div>");
	                        banner3.attr('data-index', '3');
	                        banner3.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-40% sur tout le site') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.photomoinscher.leclerc/?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-28-blackFriday&utm_content=")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner3);
	
	                        /*var header4 = $('.sto-product-container').find('div[data-id="328277918591200"]').first().clone(true).attr('slider-nav', "Huile sublimatrice");
	                        if (header4.find('.box-item').length) {
	                            header4 = $('.sto-product-container').find('div[data-id="328277918591200"] .box-item').first().clone(true).attr('slider-nav', "Huile sublimatrice");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);*/
	
	                        var header5 = $('.sto-product-container').find('div[data-id="0190781396475"]').first().clone(true).attr('slider-nav', "Moniteur LCD GAMING HP");
	                        if (header5.find('.box-item').length) {
	                            header5 = $('.sto-product-container').find('div[data-id="0190781396475"] .box-item').first().clone(true).attr('slider-nav', "Moniteur LCD GAMING HP");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);
	
	                        var header6 = $('.sto-product-container').find('div[data-id="1168500600"]').first().clone(true).attr('slider-nav', "Trampoline Kangui");
	                        if (header6.find('.box-item').length) {
	                            header6 = $('.sto-product-container').find('div[data-id="1168500600"] .box-item').first().clone(true).attr('slider-nav', "Trampoline Kangui");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);
	
	                        fetch_backup_products();
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
	                            var item = $(v);
	                            if (i < 4) {
	                                if (item.hasClass('box-item-banner')) {
	                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
	                                }
	                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
	                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
	                            } else {
	                                return false;
	                            }
	                        });
	                    } else if (today >= slideDate3) { //27-11
	                        //console.log('slider date 3');
	                        var banner1 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='30% TEL Lego'>30% TEL Lego</div>");
	                        banner1.attr('data-index', '1');
	                        banner1.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('30% TEL Lego') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.culture.leclerc/jouets-u/jeux-de-construction-u/lego-u?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-27-black-friday&utm_content=slider-hp-journee-star-lego") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.culture.leclerc/jouets-u/jeux-de-construction-u/lego-u?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-27-black-friday&utm_content=slider-hp-journee-star-lego")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner1);
	
	                        var header2 = $('.sto-product-container').find('div[data-id="325869133156001"]').first().clone(true).attr('slider-nav', "Dourthe - Grande cuvée");
	                        if (header2.find('.box-item').length) {
	                            header2 = $('.sto-product-container').find('div[data-id="325869133156001"] .box-item').first().clone(true).attr('slider-nav', "Dourthe - Grande cuvée");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header2);
	
	                        var header3 = $('.sto-product-container').find('div[data-id="340152852084600"]').first().clone(true).attr('slider-nav', "Atoderm Huile Lavante");
	                        if (header3.find('.box-item').length) {
	                            header3 = $('.sto-product-container').find('div[data-id="340152852084600"] .box-item').first().clone(true).attr('slider-nav', "Atoderm Huile Lavante");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);
	
	                        /*var header4 = $('.sto-product-container').find('div[data-id="328277010867500"]').first().clone(true).attr('slider-nav', "Gel douche A-DERMA");
	                        if (header4.find('.box-item').length) {
	                            header4 = $('.sto-product-container').find('div[data-id="328277010867500"] .box-item').first().clone(true).attr('slider-nav', "Gel douche A-DERMA");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);
	
	                        var header5 = $('.sto-product-container').find('div[data-id="326468000515200"]').first().clone(true).attr('slider-nav', "NUXE eau nettoyant");
	                        if (header5.find('.box-item').length) {
	                            header5 = $('.sto-product-container').find('div[data-id="326468000515200"] .box-item').first().clone(true).attr('slider-nav', "Gel douche A-DERMA");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);*/
	                        /*var header3 = $('.sto-product-container').find('div[data-id="805278385072500"]').first().clone(true).attr('slider-nav', "Prosecco Signore Giuseppe ");
	                        if (header3.find('.box-item').length) {
	                            header3 = $('.sto-product-container').find('div[data-id="805278385072500"] .box-item').first().clone(true).attr('slider-nav', "Prosecco Signore Giuseppe ");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);*/
	
	                        var header6 = $('.sto-product-container').find('div[data-id="0842776102379"]').first().clone(true).attr('slider-nav', "Enceinte Google Mini");
	                        if (header6.find('.box-item').length) {
	                            header6 = $('.sto-product-container').find('div[data-id="0842776102379"] .box-item').first().clone(true).attr('slider-nav', "Enceinte Google Mini");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);
	
	                        var header7 = $('.sto-product-container').find('div[data-id="3221614003158"]').first().clone(true).attr('slider-nav', "Aspirateur balai");
	                        if (header7.find('.box-item').length) {
	                            header7 = $('.sto-product-container').find('div[data-id="3221614003158"] .box-item').first().clone(true).attr('slider-nav', "Aspirateur balai");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header7);
	
	                        var header8 = $('.sto-product-container').find('div[data-id="8015244200071"]').first().clone(true).attr('slider-nav', "Vélo pliant électrique");
	                        if (header8.find('.box-item').length) {
	                            header8 = $('.sto-product-container').find('div[data-id="8015244200071"] .box-item').first().clone(true).attr('slider-nav', "Vélo pliant électrique");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header8);
	
	                        fetch_backup_products();
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
	                            var item = $(v);
	                            if (i < 4) {
	                                if (item.hasClass('box-item-banner')) {
	                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
	                                }
	                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
	                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
	                            } else {
	                                return false;
	                            }
	                        });
	                    } else if (today >= slideDate2) { //26-11
	                        //console.log('slider date 2');
	                        var header1 = $('.sto-product-container').find('div[data-id="6970244526823"]').first().clone(true).attr('slider-nav', "Trottinette électrique");
	                        if (header1.find('.box-item').length) {
	                            header1 = $('.sto-product-container').find('div[data-id="6970244526823"] .box-item').first().clone(true).attr('slider-nav', "Trottinette électrique");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header1);
	
	                        var banner2 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='2 albums achetés = 1 offert'>2 albums achetés = 1 offert</div>");
	                        banner2.attr('data-index', '2');
	                        banner2.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('2 albums achetés = 1 offert') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.culture.leclerc/2000000193649-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-26-black-friday&utm_content=slider-hp-2-albums-achetes-1-offert") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.culture.leclerc/2000000193649-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-26-black-friday&utm_content=slider-hp-2-albums-achetes-1-offert")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner2);
	
	                        var header3 = $('.sto-product-container').find('div[data-id="3760165464969"]').first().clone(true).attr('slider-nav', "Trampoline Kangui");
	                        if (header3.find('.box-item').length) {
	                            header3 = $('.sto-product-container').find('div[data-id="3760165464969"] .box-item').first().clone(true).attr('slider-nav', "Trampoline Kangui");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header3);
	
	                        var header4 = $('.sto-product-container').find('div[data-id="376007380363802"]').first().clone(true).attr('slider-nav', "Premières Grives 2018");
	                        if (header4.find('.box-item').length) {
	                            header4 = $('.sto-product-container').find('div[data-id="376007380363802"] .box-item').first().clone(true).attr('slider-nav', "Premières Grives 2018");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);
	
	                        var banner5 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-15€ sur tous les pneus Dunlop'>-15€ sur tous les pneus Dunlop</div>");
	                        banner5.attr('data-index', '5');
	                        banner5.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-15€ sur tous les pneus Dunlop') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.auto.leclerc/pneus-dunlop-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.auto.leclerc/pneus-dunlop-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner5);
	
	                        var header6 = $('.sto-product-container').find('div[data-id="8013298204922"]').first().clone(true).attr('slider-nav', "Nettoyeur pression ");
	                        if (header6.find('.box-item').length) {
	                            header6 = $('.sto-product-container').find('div[data-id="8013298204922"] .box-item').first().clone(true).attr('slider-nav', "Nettoyeur pression ");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);
	                        fetch_backup_products();
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
	                            var item = $(v);
	                            if (i < 4) {
	                                if (item.hasClass('box-item-banner')) {
	                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
	                                }
	                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
	                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
	                            } else {
	                                return false;
	                            }
	                        });
	                    } else if (today >= slideDate1) { //25-11
	                        //console.log('slider date 1');
	
	                        var header1 = $('.sto-product-container').find('div[data-id="0194441170290"]').first().clone(true).attr('slider-nav', "PC Portable HP 14\"");
	                        if (header1.find('.box-item').length) {
	                            header1 = $('.sto-product-container').find('div[data-id="0194441170290"] .box-item').first().clone(true).attr('slider-nav', "PC Portable HP 14\"");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header1);
	
	                        /*var header2 = $('.sto-product-container').find('div[data-id="3401360147508"]').first().clone(true).attr('slider-nav', "Bioderma SEBIUM GLOBAL");
	                        if (header2.find('.box-item').length) {
	                            header2 = $('.sto-product-container').find('div[data-id="3401360147508"] .box-item').first().clone(true).attr('slider-nav', "Bioderma SEBIUM GLOBAL");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header2);*/
	
	                        var banner2 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='Console Nintendo Switch + FIFA'>Console Nintendo Switch + FIFA</div>");
	                        banner2.attr('data-index', '2');
	                        banner2.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('Console Nintendo Switch + FIFA') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.culture.leclerc/2000000194141-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp-console-nintendo-switch-fifa-20") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.culture.leclerc/2000000194141-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp-console-nintendo-switch-fifa-20")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner2);
	
	                        var banner3 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-15€ sur tous les pneus Dunlop'>-15€ sur tous les pneus Dunlop</div>");
	                        banner3.attr('data-index', '3');
	                        banner3.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-15€ sur tous les pneus Dunlop') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.auto.leclerc/pneus-dunlop-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.auto.leclerc/pneus-dunlop-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner3);
	
	                        var header4 = $('.sto-product-container').find('div[data-id="3016661150500"]').first().clone(true).attr('slider-nav', "Blender chauffant");
	                        if (header4.find('.box-item').length) {
	                            header4 = $('.sto-product-container').find('div[data-id="3016661150500"] .box-item').first().clone(true).attr('slider-nav', "Blender chauffant");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);
	
	                        /*var header5 = $('.sto-product-container').find('div[data-id="3401360147508"]').first().clone(true).attr('slider-nav', "Bioderma SEBIUM GLOBAL");
	                        if (header5.find('.box-item').length) {
	                            header5 = $('.sto-product-container').find('div[data-id="3401360147508"] .box-item').first().clone(true).attr('slider-nav', "Bioderma SEBIUM GLOBAL");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);*/
	
	                        var header6 = $('.sto-product-container').find('div[data-id="805278385072500"]').first().clone(true).attr('slider-nav', "Prosecco Signore Giuseppe ");
	                        if (header6.find('.box-item').length) {
	                            header6 = $('.sto-product-container').find('div[data-id="805278385072500"] .box-item').first().clone(true).attr('slider-nav', "Prosecco Signore Giuseppe ");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);
	
	                        fetch_backup_products();
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
	                            var item = $(v);
	                            if (i < 4) {
	                                if (item.hasClass('box-item-banner')) {
	                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
	                                }
	                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
	                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
	                            } else {
	                                return false;
	                            }
	                        });
	                    } else {
	                        //console.log('slider other date');
	                        var header1 = $('.sto-product-container').find('div[data-id="194441170290"]').first().clone(true).attr('slider-nav', "PC Portable HP 14");
	                        if (header1.find('.box-item').length) {
	                            header1 = $('.sto-product-container').find('div[data-id="194441170290"] .box-item').first().clone(true).attr('slider-nav', "PC Portable HP 14");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header1);
	
	                        var banner2 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='Console Nintendo Switch + FIFA 20'>Console Nintendo Switch + FIFA 20</div>");
	                        banner2.attr('data-index', '2');
	                        banner2.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('Console Nintendo Switch + FIFA 20') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.culture.leclerc/2000000194141-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp-console-nintendo-switch-fifa-20") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.culture.leclerc/2000000194141-p?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp-console-nintendo-switch-fifa-20")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner2);
	
	                        var banner3 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='-15€ sur tous les pneus Dunlop'>-15€ sur tous les pneus Dunlop</div>");
	                        banner3.attr('data-index', '3');
	                        banner3.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('-15€ sur tous les pneus Dunlop') + '", "urlsortie": "' + cleanStringBlackFriday("https://www.auto.leclerc/pneus-dunlop-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://www.auto.leclerc/pneus-dunlop-b?utm_medium=leclerc&utm_source=portail&utm_campaign=2019-11-25-black-friday&utm_content=slider-hp")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner3);
	
	                        var header4 = $('.sto-product-container').find('div[data-id="3016661150500"]').first().clone(true).attr('slider-nav', "Blender chauffant");
	                        if (header4.find('.box-item').length) {
	                            header4 = $('.sto-product-container').find('div[data-id="3016661150500"] .box-item').first().clone(true).attr('slider-nav', "Blender chauffant");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header4);
	
	                        var header5 = $('.sto-product-container').find('div[data-id="3401360147508"]').first().clone(true).attr('slider-nav', "Bioderma SEBIUM GLOBAL");
	                        if (header5.find('.box-item').length) {
	                            header5 = $('.sto-product-container').find('div[data-id="3401360147508"] .box-item').first().clone(true).attr('slider-nav', "Bioderma SEBIUM GLOBAL");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);
	
	                        var header6 = $('.sto-product-container').find('div[data-id="805278385072500"]').first().clone(true).attr('slider-nav', "Prosecco Signore Giuseppe ");
	                        if (header6.find('.box-item').length) {
	                            header6 = $('.sto-product-container').find('div[data-id="805278385072500"] .box-item').first().clone(true).attr('slider-nav', "Prosecco Signore Giuseppe ");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header6);
	
	                        var banner4 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='CODE PROMO SPORT'>CODE PROMO SPORT</div>");
	                        banner4.attr('data-index', '4');
	                        banner4.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('CODE PROMO SPORT') + '", "urlsortie": "' + cleanStringBlackFriday("https://gti3.sport.leclerc/dynclick/sport-leclerc/?ept-publisher=E-Leclerc_portail&ept-name=portail-offreblackfriday&eurl=https%3A%2F%2Fwww.sport.leclerc%2Foperation-moins-30%3Futm_source%3DE-Leclerc_portail%26utm_campaign%3Doffreblackfriday%26utm_medium%3Dleclerc%26utm_content%3Doffretextile") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://gti3.sport.leclerc/dynclick/sport-leclerc/?ept-publisher=E-Leclerc_portail&ept-name=portail-offreblackfriday&eurl=https%3A%2F%2Fwww.sport.leclerc%2Foperation-moins-30%3Futm_source%3DE-Leclerc_portail%26utm_campaign%3Doffreblackfriday%26utm_medium%3Dleclerc%26utm_content%3Doffretextile")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner4);
	                        var header5 = $('.sto-product-container').find('div[data-id="8801643066956"]').first().clone(true).attr('slider-nav', "SMARTPHONE");
	                        if (header5.find('.box-item').length) {
	                            header5 = $('.sto-product-container').find('div[data-id="8801643066956"] .box-item').first().clone(true).attr('slider-nav', "SMARTPHONE");
	                        }
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(header5);
	                        var banner6 = $("<div class='box-item box-item-rayon box-item-banner' slider-nav='VENTES PRIVEES'>VENTES PRIVEES</div>");
	                        banner6.attr('data-index', '6');
	                        banner6.attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","nomclicsortant":"' + cleanStringBlackFriday('BrandAlley') + '", "urlsortie": "' + cleanStringBlackFriday("https://eulerian.brandalley.fr/dynclick/leclerc-brandalley-fr/?ept-publisher=Drive&ept-name=Drive_Tunnel_web_1189x176&eurl=https://leclerc.brandalley.fr/event/black-Friday") + '", "event":"blackfriday_traffickingexterne_visuels"});window.open("https://eulerian.brandalley.fr/dynclick/leclerc-brandalley-fr/?ept-publisher=Drive&ept-name=Drive_Tunnel_web_1189x176&eurl=https://leclerc.brandalley.fr/event/black-Friday")');
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(banner6);
	                        fetch_backup_products();
	                        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').find('.box-item').each(function(i, v) {
	                            var item = $(v);
	                            if (i < 4) {
	                                if (item.hasClass('box-item-banner')) {
	                                    $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').attr('data-bg', item.attr('data-index'));
	                                }
	                                $('.sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + (i + 1) + '"]').text(item.attr('slider-nav'));
	                                $('.sto-' + placeholder + '-header-slider .sto-' + placeholder + '-header-slides-wrapper .sto-' + placeholder + '-header-slide[data-ind="' + (i + 1) + '"]').append($(v).clone(true));
	                            } else {
	                                return false;
	                            }
	                        });
	                    }
	
	
	                    /* Apply tracking for homepage slider */
	                    pagination_container = $('.sto-' + placeholder + '-category-page .sto-category-page-pagination');
	
	                    if (/hightech/.test(window.location.href)) {
	                        // URL High-Tech detected
	                        show_category("hightech", container, tracker);
	                    } else if (/jardin\/maison/.test(window.location.href)) {
	                        // URL Jardin, Maison & Electroménager detected
	                        show_category("maison", container, tracker);
	                    } else if (/culture\/jouets/.test(window.location.href)) {
	                        // URL Culture & Jouets detected
	                        show_category("culture", container, tracker);
	                    } else if (/parapharmacie/.test(window.location.href)) {
	                        // URL Parapharmacie detected
	                        show_category("parapharmacie", container, tracker);
	                    } else if (/mode\/sport/.test(window.location.href)) {
	                        // URL Mode & Sport detected
	                        show_category("mode", container, tracker);
	                    } else if (/vins\/alcools/.test(window.location.href)) {
	                        // URL Vins & Alcools detected
	                        show_category("vin", container, tracker);
	                    } else if (/epicerie/.test(window.location.href)) {
	                        // URL Epicerie fine detected
	                        show_category("alimentaire", container, tracker);
	                    } else if (/bijoux/.test(window.location.href)) {
	                        // URL Bijoux detected
	                        show_category("bijoux", container, tracker);
	                    } else if (/all\/products/.test(window.location.href)) {
	                        // URL Tous les produits detected
	                        show_category("all", container, tracker);
	                    }
	
	                    $('.box-item').each(function(i, v) {
	                        if ($(this).find('.btn-memo').length == 0 && $(this).find('.btn-achat-ligne').length > 0) {
	                            $(this).addClass('sto-exclu-web');
	                        } else if ($(this).find('.btn-memo').length > 0 && $(this).find('.btn-achat-ligne').length <= 0) {
	                            $(this).addClass('sto-en-magasin');
	                        } else {
	                            $(this).removeClass('sto-en-magasin').removeClass('sto-exclu-web');
	                        }
	                    });
	
	                    $('.sto-' + placeholder + '-content-section').find('.sto-' + placeholder + '-prods-slides-wrapper').each(function(i, v) {
	                        var prodsWrapper = $(this);
	                        if (prodsWrapper.children().length === 0) {
	                            prodsWrapper.parents('.sto-' + placeholder + '-content-section-products').children('.sto-' + placeholder + '-prods-slider-prev').remove();
	                            prodsWrapper.parents('.sto-' + placeholder + '-content-section-products').children('.sto-' + placeholder + '-prods-slider-next').remove();
	                            prodsWrapper.parents('.sto-' + placeholder + '-prods-slider').hide();
	                        }
	                    });
	
	                    //check for empy sections
	                    var tgSections = $('.sto-black-friday-content-section');
	                    for (var i = 0; i < 8; i++) {
	                        if (tgSections.eq(i).find('.box-item').length <= 0) {
	                            tgSections.eq(i).addClass('sectionEmpty');
	
	                        }
	                    }
	
	                    /*
	                     *      EVENT HANDLERS
	                     */
	
	                    /* ------- Homepage header slider event handlers ------- */
	                    container.find('.sto-' + placeholder + '-header-slider-next').on('click', function() {
	                        var slideWidth = $('.sto-' + placeholder + '-header-slide').outerWidth();
	                        var sliderPosLeft = $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').position().left;
	                        var oldSlideIndex = $('.sto-header-slide-active').attr('data-ind');
	
	                        if (oldSlideIndex == '4') {
	                            var newSlideIndex = '1';
	                        } else {
	                            var newSlideIndex = (parseFloat(oldSlideIndex) + 1) + '';
	                        }
	
	                        $('.sto-' + placeholder + '-header-slide, .sto-' + placeholder + '-header-slider-nav-btn').removeClass('sto-header-slide-active');
	                        $('.sto-' + placeholder + '-header-slide[data-ind="' + newSlideIndex + '"], .sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + newSlideIndex + '"]').addClass('sto-header-slide-active');
	
	                        if (oldSlideIndex == '4' && !$('.sto-' + placeholder + '-header-slides-wrapper').is(':animated')) {
	                            $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').stop().animate({
	                                "left": 0
	                            }, 'slow');
	                        } else if (!$('.sto-' + placeholder + '-header-slides-wrapper').is(':animated')) {
	                            $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').stop().animate({
	                                "left": "-" + (parseInt(oldSlideIndex) * 100) + "%"
	                            }, 'slow');
	                        }
	                    });
	                    container.find('.sto-' + placeholder + '-header-slider-prev').on('click', function() {
	                        var slideWidth = $('.sto-' + placeholder + '-header-slide').outerWidth();
	                        var sliderPosLeft = $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').position().left;
	                        var oldSlideIndex = $('.sto-header-slide-active').attr('data-ind');
	
	                        if (oldSlideIndex == '1') {
	                            var newSlideIndex = '4';
	                        } else {
	                            var newSlideIndex = (parseFloat(oldSlideIndex) - 1) + '';
	                        }
	
	                        $('.sto-' + placeholder + '-header-slide, .sto-' + placeholder + '-header-slider-nav-btn').removeClass('sto-header-slide-active');
	                        $('.sto-' + placeholder + '-header-slide[data-ind="' + newSlideIndex + '"], .sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + newSlideIndex + '"]').addClass('sto-header-slide-active');
	
	                        if (oldSlideIndex == '1' && !$('.sto-' + placeholder + '-header-slides-wrapper').is(':animated')) {
	                            $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').stop().animate({
	                                "left": '-300%'
	                            }, 'slow');
	                        } else if (!$('.sto-' + placeholder + '-header-slides-wrapper').is(':animated')) {
	                            $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').stop().animate({
	                                "left": "-" + ((parseInt(newSlideIndex) - 1) * 100) + "%"
	                            }, 'slow');
	                        }
	                    });
	                    container.find('.sto-' + placeholder + '-header-slider-nav-btn').on('click', function() {
	                        var thisCat = $(this).attr('data-cat');
	                        var slideWidth = $('.sto-' + placeholder + '-header-slide').outerWidth();
	                        var thisSlideIndex = parseFloat($(this).attr('data-ind'));
	
	                        $(this).closest('.sto-' + placeholder + '-header').find('.sto-' + placeholder + '-header-slides-wrapper').stop().animate({
	                            "left": (thisSlideIndex - 1) * -slideWidth
	                        }, 'slow');
	
	                        $('.sto-' + placeholder + '-header-slide, .sto-' + placeholder + '-header-slider-nav-btn').removeClass('sto-header-slide-active');
	                        $('.sto-' + placeholder + '-header-slide[data-ind="' + thisSlideIndex + '"], .sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + thisSlideIndex + '"]').addClass('sto-header-slide-active');
	                    });
	
	                    /* ------- Homepage header MOBILE slider event handlers NEW SLIDER MOBILE HEADER ------- */
	                    container.find('.sto-' + placeholder + '-header-slider-mob-next').on('click', function() {
	                        $('.sto-black-friday-header-slide').addClass('smooth-slider');
	                        var oldSlideIndex = parseInt($('.sto-header-slide-active').attr('data-ind'));
	                        var newSlideIndex = oldSlideIndex == 4 ? 1 : oldSlideIndex + 1;
	                        $('.sto-black-friday-header-slides-wrapper').animate({
	                            scrollLeft: $('.sto-black-friday-header-slide').width() * (newSlideIndex - 1)
	                        }, 500);
	                        $('.sto-' + placeholder + '-header-slide, .sto-' + placeholder + '-header-slider-nav-btn').removeClass('sto-header-slide-active');
	                        $('.sto-' + placeholder + '-header-slide[data-ind="' + newSlideIndex + '"], .sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + newSlideIndex + '"]').addClass('sto-header-slide-active');
	                        setTimeout(function() {
	                            $('.sto-black-friday-header-slide').removeClass('smooth-slider');
	                        }, 500);
	                    });
	                    container.find('.sto-' + placeholder + '-header-slider-mob-prev').on('click', function() {
	                        $('.sto-black-friday-header-slide').addClass('smooth-slider');
	                        var oldSlideIndex = parseInt($('.sto-header-slide-active').attr('data-ind'));
	                        var newSlideIndex = oldSlideIndex == 1 ? 4 : oldSlideIndex - 1;
	                        $('.sto-black-friday-header-slides-wrapper').animate({
	                            scrollLeft: $('.sto-black-friday-header-slide').width() * (newSlideIndex - 1)
	                        }, 500);
	                        $('.sto-' + placeholder + '-header-slide, .sto-' + placeholder + '-header-slider-nav-btn').removeClass('sto-header-slide-active');
	                        $('.sto-' + placeholder + '-header-slide[data-ind="' + newSlideIndex + '"], .sto-' + placeholder + '-header-slider-nav-btn[data-ind="' + newSlideIndex + '"]').addClass('sto-header-slide-active');
	                        setTimeout(function() {
	                            $('.sto-black-friday-header-slide').removeClass('smooth-slider');
	                        }, 500);
	                    });
	
	                    /* Homepage products slider mobile event handlers NEW SLIDER MOBILE SECCION */
	                    container.find('.sto-' + placeholder + '-prods-slider-next').on('click', function() {
	                        var parent = $(this).parents('.sto-' + placeholder + '-content-section-products');
	                        var oldSlideIndex = parseInt(parent.find('.sto-prods-slide-active').attr('data-ind'));
	                        var newSlideIndex = oldSlideIndex == 4 ? 1 : oldSlideIndex + 1;
	                        parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').addClass('smooth-slider');
	                        parent.find('.sto-black-friday-prods-slides-wrapper').animate({
	                            scrollLeft: parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').width() * (newSlideIndex - 1)
	                        }, 600);
	                        parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').removeClass('sto-prods-slide-active');
	                        parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item[data-ind="' + newSlideIndex + '"]').addClass('sto-prods-slide-active');
	                        setTimeout(function() {
	                            parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').removeClass('smooth-slider');
	                        }, 500);
	                    });
	                    container.find('.sto-' + placeholder + '-prods-slider-prev').on('click', function() {
	                        var parent = $(this).parents('.sto-' + placeholder + '-content-section-products');
	                        var oldSlideIndex = parseInt(parent.find('.sto-prods-slide-active').attr('data-ind'));
	                        var newSlideIndex = oldSlideIndex == 1 ? 4 : oldSlideIndex - 1;
	                        parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').addClass('smooth-slider');
	                        parent.find('.sto-black-friday-prods-slides-wrapper').animate({
	                            scrollLeft: parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').width() * (newSlideIndex - 1)
	                        }, 600);
	                        parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').removeClass('sto-prods-slide-active');
	                        parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item[data-ind="' + newSlideIndex + '"]').addClass('sto-prods-slide-active');
	                        setTimeout(function() {
	                            parent.find('.sto-' + placeholder + '-prods-slides-wrapper .box-item').removeClass('smooth-slider');
	                        }, 500);
	                    });
	
	                    /* Homepage banner tile mobile event handler */
	                    container.find('.sto-' + placeholder + '-banner-slider-next').on('click', function() {
	                        var parent = $(this).parents('.sto-' + placeholder + '-content-section-products');
	                        var slideWidth = parent.find('.sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile').outerWidth();
	                        var sliderPosLeft = parent.find('.sto-' + placeholder + '-banner-slides-wrapper').position().left;
	                        var oldSlideIndex = parent.find('.sto-banner-slide-active').attr('data-ind');
	
	                        if (oldSlideIndex == '4') {
	                            var newSlideIndex = '1';
	                        } else {
	                            var newSlideIndex = (parseFloat(oldSlideIndex) + 1) + '';
	                        }
	
	                        parent.find('.sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile').removeClass('sto-banner-slide-active');
	                        parent.find('.sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile[data-ind="' + newSlideIndex + '"]').addClass('sto-banner-slide-active');
	
	                        if (oldSlideIndex == '4' && !$('.sto-' + placeholder + '-banner-slides-wrapper').is(':animated')) {
	                            parent.find('.sto-' + placeholder + '-banner-slides-wrapper').stop().animate({
	                                "left": 0
	                            }, 'slow');
	                        } else if (!$('.sto-' + placeholder + '-banner-slides-wrapper').is(':animated')) {
	                            parent.find('.sto-' + placeholder + '-banner-slides-wrapper').stop().animate({
	                                "left": "-" + (parseInt(oldSlideIndex) * 100) + "%"
	                            }, 'slow');
	                        }
	                    });
	                    container.find('.sto-' + placeholder + '-banner-slider-prev').on('click', function() {
	                        var parent = $(this).parents('.sto-' + placeholder + '-content-section-products');
	                        var slideWidth = parent.find('.sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile').outerWidth();
	                        var sliderPosLeft = parent.find('.sto-' + placeholder + '-banner-slides-wrapper').position().left;
	                        var oldSlideIndex = parent.find('.sto-banner-slide-active').attr('data-ind');
	
	                        if (oldSlideIndex == '1') {
	                            var newSlideIndex = '4';
	                        } else {
	                            var newSlideIndex = (parseFloat(oldSlideIndex) - 1) + '';
	                        }
	
	                        parent.find('.sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile').removeClass('sto-banner-slide-active');
	                        parent.find('.sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile[data-ind="' + newSlideIndex + '"]').addClass('sto-banner-slide-active');
	
	                        if (oldSlideIndex == '1' && !$('.sto-' + placeholder + '-banner-slides-wrapper').is(':animated')) {
	                            parent.find('.sto-' + placeholder + '-banner-slides-wrapper').stop().animate({
	                                "left": '-300%'
	                            }, 'slow');
	                        } else if (!$('.sto-' + placeholder + '-banner-slides-wrapper').is(':animated')) {
	                            parent.find('.sto-' + placeholder + '-banner-slides-wrapper').stop().animate({
	                                "left": "-" + ((parseInt(newSlideIndex) - 1) * 100) + "%"
	                            }, 'slow');
	                        }
	                    });
	
	                    /* Homepage mobile menu event handler */
	                    container.find('.sto-' + placeholder + '-content-main-claim').on('click', function() {
	                        if ($(this).parents('.sto-' + placeholder + '-content-wrapper').hasClass('menuopen')) {
	                            $(this).parents('.sto-' + placeholder + '-content-wrapper').removeClass('menuopen');
	                        } else {
	                            $(this).parents('.sto-' + placeholder + '-content-wrapper').addClass('menuopen');
	                        }
	                    });
	
	                    /* ------- Homepage universe CTA event handlers ------- */
	                    container.find('.sto-' + placeholder + '-nav-slides-wrapper > a').on('click', function(e) {
	                        //e.preventDefault();
	                        //e.stopPropagation();
	
	                        var self = $(this);
	                        var category = self.data('cat');
	                        show_category(category, container, tracker);
	                    });
	                    container.find('.sto-' + placeholder + '-content-section .sto-' + placeholder + '-content-section-seemore > span').on('click', function(e) {
	                        var self = $(this);
	                        var category = self.parents('.sto-' + placeholder + '-content-section').data('cat');
	                        if (category == "culture") {
	                            dataLayer.push({
	                                event: 'outon_voirtouslesproduits_' + category + '&jouet',
	                                nomcampagne: "blackfriday2019"
	                            });
	                        } else if (category == "vin") {
	                            dataLayer.push({
	                                event: 'outon_voirtouslesproduits_' + category + 's&alcool',
	                                nomcampagne: "blackfriday2019"
	                            });
	                        } else {
	                            dataLayer.push({
	                                event: 'outon_voirtouslesproduits_' + category,
	                                nomcampagne: "blackfriday2019"
	                            });
	                        }
	                        show_category(category, container, tracker);
	                    });
	
	                    /* ------- Universe page back button event handler ------- */
	                    container.find('.sto-' + placeholder + '-category-page .sto-category-page-banner .sto-category-banner-cta, .sto-' + placeholder + '-category-page .sto-category-banner-cta-mob').on('click', function() {
	                        $('.sto-' + placeholder + '-category-page').hide();
	                        $('.sto-' + placeholder + '-home-page').show();
	                        resize_gutters();
	                    });
	
	                    /* ------- Universe page filtre buttons event handlers ------- */
	                    container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-current').on('click', function() {
	                        var elem = $(this);
	                        if (container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-list').is(':visible')) {
	                            container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-list').hide();
	                            elem.removeClass('active');
	                        } else {
	                            container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-list').show();
	                            elem.addClass('active');
	                        }
	                    });
	                    container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').on('click', function() {
	                        var elem = $(this);
	                        if (container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').is(':visible')) {
	                            container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').hide();
	                            elem.removeClass('active');
	                        } else {
	                            container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').show();
	                            elem.addClass('active');
	                        }
	                    });
	                    container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-category-page-filter-price').on('click', function() {
	                        var category = $(this).parents('.sto-' + placeholder + '-category-page').attr('category');
	                        if (category === 'all') category = $(this).parents('.sto-' + placeholder + '-category-page').attr('universe');
	                        var contentSort = $(this).text();
	                        sorting = $(this).data('sort');
	
	                        $(this).parents('.sto-price-filter-list').find('.active').removeClass('active');
	                        $(this).addClass('active');
	
	                        $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-current').find('.sto-current-price-filter').text(contentSort);
	
	                        if ($('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-list').is(':visible')) {
	                            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-list').hide();
	                            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-current').removeClass('active');
	                        } else {
	                            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-list').show();
	                            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-price-filter-current').addClass('active');
	                        }
	
	                        $('.sto-' + placeholder + '-category-page .sto-category-page-products').empty();
	                        var paginateArray = sort_products(category);
	                        if (category === "bijoux") {
	                            if (paginateArray.length > paginationBijoux) {
	                                paginate_category(paginateArray, container, tracker);
	                            } else {
	                                unpaginate_category(paginateArray);
	                            }
	                        } else {
	                            if (paginateArray.length > paginationLimit) {
	                                paginate_category(paginateArray, container, tracker);
	                            } else {
	                                unpaginate_category(paginateArray);
	                            }
	                        }
	                    });
	
	                    // Resize gutters
	                    resize_gutters();
	                    var itemHeight = $('.sto-' + placeholder + '-prods-slider .sto-' + placeholder + '-prods-slides-wrapper .box-item.box-item-rayon').outerHeight();
	                    $('.sto-' + placeholder + '-prods-slider-prev, .sto-' + placeholder + '-prods-slider-next').css({
	                        'top': ((itemHeight / 2) - ($('.sto-' + placeholder + '-prods-slider-next').outerHeight() / 2)) + "px"
	                    });
	                    var itemHeight = $('.sto-' + placeholder + '-banner-slider .sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile').outerHeight();
	                    $('.sto-' + placeholder + '-banner-slider-prev, .sto-' + placeholder + '-banner-slider-next').css({
	                        'top': ((itemHeight / 2) - ($('.sto-' + placeholder + '-banner-slider-next').outerHeight() / 2)) + "px"
	                    });
	
	                    window.addEventListener("resize", function() {
	                        windowSize = window.innerWidth;
	
	                        checkWindowSize();
	
	                        resize_gutters();
	
	                        if (windowSize <= 1023) {
	                            $('#sto-' + placeholder + '-banner-photo-link').attr('href', 'http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_750x295_08463_30TTN18');
	                            $('#sto-' + placeholder + '-banner-photo-link').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': '', 'urlsortie': '" + cleanStringBlackFriday('www.photomoinscher.leclerc?cref=om_coop_blackfriday_750x295_08463_30TTN18') + "', 'event': 'blackfriday_traffickingexterne_visuels'});");
	                            $('#sto-' + placeholder + '-banner-photo-link-2').attr('href', 'http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_750x295_08463_40TTN18');
	                            $('#sto-' + placeholder + '-banner-photo-link-2').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': '', 'urlsortie': '" + cleanStringBlackFriday('http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_750x295_08463_40TTN18') + "', 'event': 'blackfriday_traffickingexterne_visuels'});");
	                        } else {
	                            $('#sto-' + placeholder + '-banner-photo-link').attr('href', 'http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_1189x176_08463_30TTN18');
	                            $('#sto-' + placeholder + '-banner-photo-link').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': '', 'urlsortie': '" + cleanStringBlackFriday('www.photomoinscher.leclerc?cref=om_coop_blackfriday_1189x176_08463_30TTN18') + "', 'event': 'blackfriday_traffickingexterne_visuels'});");
	                            $('#sto-' + placeholder + '-banner-photo-link-2').attr('href', 'http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_1189x176_08463_40TTN18');
	                            $('#sto-' + placeholder + '-banner-photo-link-2').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': '', 'urlsortie': '" + cleanStringBlackFriday('http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_1189x176_08463_40TTN18') + "', 'event': 'blackfriday_traffickingexterne_visuels'});");
	                        }
	
	                        var itemHeight = $('.sto-' + placeholder + '-prods-slider .sto-' + placeholder + '-prods-slides-wrapper .box-item.box-item-rayon').outerHeight();
	                        $('.sto-' + placeholder + '-prods-slider-prev, .sto-' + placeholder + '-prods-slider-next').css({
	                            'top': ((itemHeight / 2) - ($('.sto-' + placeholder + '-prods-slider-next').outerHeight() / 2)) + "px"
	                        });
	                        var itemHeight = $('.sto-' + placeholder + '-banner-slider .sto-' + placeholder + '-banner-slides-wrapper .sto-' + placeholder + '-content-banner-tile').outerHeight();
	                        $('.sto-' + placeholder + '-banner-slider-prev, .sto-' + placeholder + '-banner-slider-next').css({
	                            'top': ((itemHeight / 2) - ($('.sto-' + placeholder + '-banner-slider-next').outerHeight() / 2)) + "px"
	                        });
	
	                        if ($('.sto-' + placeholder + '-category-page').is(':visible')) {
	                            $('.sto-' + placeholder + '-category-page .sto-category-page-products').empty();
	                            var category = $('.sto-' + placeholder + '-category-page').attr('category');
	                            var paginateArray = sort_products(category);
	                            if (category === "bijoux") {
	                                if (paginateArray.length > paginationBijoux) {
	                                    paginate_category(paginateArray, container, tracker);
	                                } else {
	                                    unpaginate_category(paginateArray);
	                                }
	                            } else {
	                                if (paginateArray.length > paginationLimit) {
	                                    paginate_category(paginateArray, container, tracker);
	                                } else {
	                                    unpaginate_category(paginateArray);
	                                }
	                            }
	                            resize_gutters();
	                        }
	                    });
	                }).then(null, console.log.bind(console));
	            });
	
	            var removers = {};
	            removers[format] = function() {
	                style.unuse();
	                container.remove();
	            };
	            return removers;
	        });
	    }
	
	    // functions
	    function checkWindowSize() {
	        if (windowSize > 1296) {
	            paginationLimit = 20;
	            paginationBijoux = 19;
	            hpProductLimit = 4;
	        } else if (windowSize <= 1296 && windowSize > 1023) {
	            paginationLimit = 15;
	            paginationBijoux = 13;
	            hpProductLimit = 3;
	        } else if (windowSize <= 1023) {
	            paginationLimit = 20;
	            paginationBijoux = 15;
	            hpProductLimit = 1;
	            $('.sto-pagination-top.sto-category-page-pagination').remove();
	            pagination_container = $('.sto-pagination-bottom.sto-category-page-pagination');
	        }
	
	        if (windowSize <= 1023) {
	            $('#sto-' + placeholder + '-banner-photo-link').attr('href', 'http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_750x295_08463_30TTN18');
	            $('#sto-' + placeholder + '-banner-photo-link').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': '', 'urlsortie': '" + cleanStringBlackFriday('www.photomoinscher.leclerc?cref=om_coop_blackfriday_750x295_08463_30TTN18') + "', 'event': 'blackfriday_traffickingexterne_visuels'});");
	        } else {
	            $('#sto-' + placeholder + '-banner-photo-link').attr('href', 'http://www.photomoinscher.leclerc/?cref=om_coop_blackfriday_1189x176_08463_30TTN18');
	            $('#sto-' + placeholder + '-banner-photo-link').attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019', 'nomclicsortant': '', 'urlsortie': '" + cleanStringBlackFriday('www.photomoinscher.leclerc?cref=om_coop_blackfriday_1189x176_08463_30TTN18') + "', 'event': 'blackfriday_traffickingexterne_visuels'});");
	        }
	    }
	
	    function goUpBtn() {
	        $(window).scroll(function() {
	            if ($(this).scrollTop() >= 500) { // If page is scrolled more than 50px
	                $('#return-to-top').fadeIn(200); // Fade in the arrow
	            } else {
	                $('#return-to-top').fadeOut(200); // Else fade out the arrow
	            }
	        });
	        $(document).on("click", '#return-to-top', function() { // When arrow is clicked
	            if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
	                window.scrollTo(0, 0) // first value for left offset, second value for top offset
	            } else {
	                $('html').animate({
	                    scrollTop: 0 // Scroll to top of body
	                }, 500);
	            }
	        });
	        $(document).on("touchstart", '#return-to-top', function() { // When arrow is clicked
	            if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
	                window.scrollTo(0, 0) // first value for left offset, second value for top offset
	            } else {
	                $('html').animate({
	                    scrollTop: 0 // Scroll to top of body
	                }, 500);
	            }
	        });
	    }
	
	    function enableSwipe() {
	
	        // Helper function to check if event object has properties required to detect a swipe.
	        function eventObjectHasProperties(obj) {
	            return obj &&
	                obj.originalEvent &&
	                obj.originalEvent.touches &&
	                obj.originalEvent.touches.length
	        }
	
	        let touchStartX;
	        let touchStartY;
	        $(window).on('touchstart', function(event) {
	            if (!eventObjectHasProperties(event)) return;
	            touchStartX = event.originalEvent.touches[0].clientX;
	            touchStartY = event.originalEvent.touches[0].clientY;
	            $(window).on('touchmove', detectSwipe);
	        });
	
	        function detectSwipe(event) {
	
	            // Prevent the same swipe being identified multiple times.
	            $(window).off('touchmove', detectSwipe)
	
	            // Not sure why but sometimes these properties aren't present.
	            if (!eventObjectHasProperties(event)) return;
	
	            // Using coordinates set ontouchstart, compute change in X and Y axes as a result of touch.
	            const deltaX = touchStartX - event.originalEvent.touches[0].clientX;
	            const deltaY = touchStartY - event.originalEvent.touches[0].clientY;
	
	            // Ensure the touch event was fired from within the format and that it was a horizontal swipe.
	            if ($(event.target).parents('.sto-container').length && Math.abs(deltaX) > Math.abs(deltaY)) {
	
	                // Delta for a right-to-left (aka "next") swipe will be positive.
	                if (deltaX > 0) {
	
	                    // Handle the swipe when the touch completes.
	                    $(window).on('touchend', handleNextSwipe)
	
	                    // Otherwise, it's a left-to-right (aka "previous") swipe, which is the inverse.
	                } else {
	
	                    // Handle the swipe when the touch completes.
	                    $(window).on('touchend', handlePrevSwipe)
	                }
	            }
	        }
	
	        function handleNextSwipe() {
	
	            // Prevent this from firing more than once per swipe.
	            $(window).off('touchend', handleNextSwipe);
	
	            // Helper function that returns the next element of a given type (e.g. indicator or product)
	            function getNextElement(elementType, selector) {
	                const $items = $(`.sto-${elementType}`);
	                const currentIndex = $items.index($(`.sto-${elementType}.${selector}`));
	                return $items.eq((currentIndex + 1) % $items.length);
	            }
	
	            // Get next elements given their type and position class.
	            const $upcomingCurrent = getNextElement('indicator', 'current');
	            const $upcomingPrev = getNextElement('item', 'prev-item');
	            const $upcomingSelected = getNextElement('item', 'selected-item');
	            const $upcomingNext = getNextElement('item', 'next-item');
	
	            // Remove existing position classes.
	            $('.sto-item').removeClass('selected-item prev-item next-item');
	            $('.sto-indicator.current').removeClass('current');
	
	            // Update next elements with relevant position class.
	            $upcomingCurrent.addClass('current');
	            $upcomingPrev.addClass('prev-item');
	            $upcomingSelected.addClass('selected-item');
	            $upcomingNext.addClass('next-item');
	        }
	
	        function handlePrevSwipe() {
	
	            // Prevent this from firing more than once per swipe.
	            $(window).off('touchend', handlePrevSwipe);
	
	            // Helper function that returns the previous element of a given type (e.g. indicator or product)
	            function getPrevElement(elementType, selector) {
	                const $items = $(`.sto-${elementType}`);
	                const currentIndex = $items.index($(`.sto-${elementType}.${selector}`));
	                if (currentIndex === 0) {
	                    return $items.eq($items.length - 1);
	                } else {
	                    return $items.eq(currentIndex - 1);
	                }
	            }
	
	            // Get previous elements given their type and position class.
	            const $upcomingCurrent = getPrevElement('indicator', 'current');
	            const $upcomingPrev = getPrevElement('item', 'prev-item');
	            const $upcomingSelected = getPrevElement('item', 'selected-item');
	            const $upcomingNext = getPrevElement('item', 'next-item');
	
	            // Remove existing position classes.
	            $('.sto-item').removeClass('selected-item prev-item next-item');
	            $('.sto-indicator.current').removeClass('current');
	
	            // Update previous elements with relevant position class.
	            $upcomingCurrent.addClass('current');
	            $upcomingPrev.addClass('prev-item');
	            $upcomingSelected.addClass('selected-item');
	            $upcomingNext.addClass('next-item');
	        }
	    }
	
	    function show_category(category, container, tracker) {
	        if ($('.sto-' + placeholder + '-content-wrapper').hasClass('menuopen')) {
	            $('.sto-' + placeholder + '-content-wrapper').removeClass('menuopen');
	        }
	        $('.sto-' + placeholder + '-home-page').hide();
	        $('.sto-' + placeholder + '-category-page').show().attr('category', category);
	        $('.sto-' + placeholder + '-category-page .sto-category-page-products').empty();
	        $(window).unbind('scroll');
	        $(window).scrollTop(0);
	
	        pageNumber = 1;
	
	        append_subcategories(category, container, tracker);
	
	        subcategory = "all";
	        $('.sto-' + placeholder + '-category-page').attr('subcategory', subcategory);
	        if (category == "bijoux") sorting = "ASC";
	        var paginateArray = sort_products(category);
	        $('.sto-' + placeholder + '-category-page > .sto-category-page-products').html(paginateArray);
	        if (category === "bijoux") {
	            if (paginateArray.length > paginationBijoux) {
	                paginate_category(paginateArray, container, tracker);
	            } else {
	                unpaginate_category(paginateArray);
	            }
	        } else {
	            if (paginateArray.length > paginationLimit) {
	                paginate_category(paginateArray, container, tracker);
	            } else {
	                unpaginate_category(paginateArray);
	            }
	        }
	        resize_gutters();
	    }
	
	    function append_subcategories(category, container, tracker) {
	        var subcategoriesArray = settings.universe[category];
	
	        if (category !== "all") {
	            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-current .sto-current-subcategory-filter').text("Toutes les catégories");
	            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-current').removeClass('active');
	            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-list').empty();
	            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').hide();
	            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-list').append('<div class="sto-category-page-filter-subcategory active" data-subcategory="all">Toutes les catégories</div>');
	            $.each(subcategoriesArray, function(i, v) {
	                var productLength = 0;
	                if (sto.utils.fn.cleanString(v) === "entretien_et_soin_de_la_maison") {
	                    var subcatHtml = $('<div class="sto-category-page-filter-subcategory" data-subcategory="' + sto.utils.fn.cleanString(v) + '">Entretien soin de la maison</div>');
	                } else {
	                    var subcatHtml = $('<div class="sto-category-page-filter-subcategory" data-subcategory="' + sto.utils.fn.cleanString(v) + '">' + v + '</div>');
	                }
	                $.each($('#sto-prod-storage-' + category + ' .sto-product-container>div'), function(idx, value) {
	                    var prodBox = $(this);
	                    var prodSubcategory = prodBox.data('category-1');
	                    if (typeof prodSubcategory !== "undefined") {
	                        try {
	                            prodSubcategory = JSON.parse(prodSubcategory);
	                        } catch (e) {}
	                        if (typeof prodSubcategory === "object") {
	                            // normalize category names
	                            prodSubcategory = prodSubcategory.map(sto.utils.fn.cleanString);
	                            if (prodSubcategory.indexOf(sto.utils.fn.cleanString(v)) !== -1) {
	                                productLength++;
	                            } else {
	                                if (typeof settings.subcategories_aliases[sto.utils.fn.cleanString(v)] !== "undefined") {
	                                    $.each(settings.subcategories_aliases[sto.utils.fn.cleanString(v)], function(ii, vv) {
	                                        if (prodSubcategory.indexOf(sto.utils.fn.cleanString(vv)) !== -1) {
	                                            productLength++;
	                                        }
	                                    });
	                                }
	                            }
	                        } else {
	                            if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(v)) {
	                                productLength++;
	                            } else {
	                                if (typeof settings.subcategories_aliases[sto.utils.fn.cleanString(v)] !== "undefined") {
	                                    $.each(settings.subcategories_aliases[sto.utils.fn.cleanString(v)], function(ii, vv) {
	                                        if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(vv)) {
	                                            productLength++;
	                                        }
	                                    });
	                                }
	                            }
	                        }
	                    }
	                });
	                if (productLength > 0)
	                    $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-list').append(subcatHtml);
	            });
	        } else {
	            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-current .sto-current-subcategory-filter').text("Toutes les catégories");
	            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-current').removeClass('active');
	            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-list').empty();
	            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').hide();
	            $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-list').append('<div class="sto-category-page-filter-universe active" data-universe="all">Toutes les catégories</div>');
	            $.each(settings.all_products_categories, function(idx, val) {
	                var productLength = 0;
	                var subcatHtml = $('<div class="sto-category-page-filter-universe" data-universe="' + sto.utils.fn.cleanString(val) + '">' + idx + '</div>');
	                var productLength = $('#sto-prod-storage-' + val + ' .sto-product-container>div').length;
	                if (productLength > 0)
	                    $('.sto-' + placeholder + '-category-page .sto-subcategory-filter-list').append(subcatHtml);
	            });
	        }
	
	        container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-category-page-filter-subcategory').off().on('click', function() {
	            var category = $(this).parents('.sto-' + placeholder + '-category-page').attr('category');
	            var contentSubcategory = $(this).text();
	            subcategory = $(this).data('subcategory');
	            $('.sto-' + placeholder + '-category-page').attr('subcategory', subcategory);
	
	            $(this).parents('.sto-subcategory-filter-list').find('.active').removeClass('active');
	            $(this).addClass('active');
	
	            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').find('.sto-current-subcategory-filter').text(contentSubcategory);
	
	            if ($('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').is(':visible')) {
	                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').hide();
	                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').removeClass('active');
	            } else {
	                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').show();
	                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').addClass('active');
	            }
	
	            $('.sto-' + placeholder + '-category-page .sto-category-page-products').empty();
	            var paginateArray = sort_products(category);
	            if (category === "bijoux") {
	                if (paginateArray.length > paginationBijoux) {
	                    paginate_category(paginateArray, container, tracker);
	                } else {
	                    unpaginate_category(paginateArray);
	                }
	            } else {
	                if (paginateArray.length > paginationLimit) {
	                    paginate_category(paginateArray, container, tracker);
	                } else {
	                    unpaginate_category(paginateArray);
	                }
	            }
	        });
	
	        container.find('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-category-page-filter-universe').off().on('click', function() {
	            var category = $(this).data('universe');
	            var contentSubcategory = $(this).text();
	            $('.sto-' + placeholder + '-category-page').attr('universe', category);
	            //universe = $(this).data('universe');
	
	            $(this).parents('.sto-subcategory-filter-list').find('.active').removeClass('active');
	            $(this).addClass('active');
	
	            $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').find('.sto-current-subcategory-filter').text(contentSubcategory);
	
	            if ($('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').is(':visible')) {
	                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').hide();
	                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').removeClass('active');
	            } else {
	                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-list').show();
	                $('.sto-' + placeholder + '-category-page .sto-category-page-filters .sto-subcategory-filter-current').addClass('active');
	            }
	
	            $('.sto-' + placeholder + '-category-page .sto-category-page-products').empty();
	            var paginateArray = sort_products(category);
	            if (category === "bijoux") {
	                if (paginateArray.length > paginationBijoux) {
	                    paginate_category(paginateArray, container, tracker);
	                } else {
	                    unpaginate_category(paginateArray);
	                }
	            } else {
	                if (paginateArray.length > paginationLimit) {
	                    paginate_category(paginateArray, container, tracker);
	                } else {
	                    unpaginate_category(paginateArray);
	                }
	            }
	        });
	    }
	
	    function sort_products(category) {
	        var sortedArray = [];
	
	        if (category !== "all") {
	            $.each($('#sto-prod-storage-' + category + ' .sto-product-container .box-item.box-item-rayon'), function(i, v) {
	                var prodBox = $(this);
	                if (typeof prodBox.data('price') === "undefined")
	                    prodBox.attr('data-price', (typeof prodBox.parent().data('price') !== "undefined") ? prodBox.parent().data('price') : "");
	                if (typeof prodBox.data('id') === "undefined")
	                    prodBox.attr('data-id', (typeof prodBox.parent().data('id') !== "undefined") ? prodBox.parent().data('id') : "");
	                if (typeof prodBox.data('category-universe') === "undefined")
	                    prodBox.attr('data-category-universe', (typeof prodBox.parent().data('category-universe') !== "undefined") ? prodBox.parent().data('category-universe') : "");
	                if (typeof prodBox.data('category-1') === "undefined")
	                    prodBox.attr('data-category-1', (typeof prodBox.parent().data('category-1') !== "undefined") ? prodBox.parent().data('category-1') : "");
	                if (subcategory !== null && subcategory !== "all") {
	                    if (prodBox.parent().length && prodBox.parent().data('category-1')) {
	                        var prodSubcategory = prodBox.parent().data('category-1');
	                        try {
	                            prodSubcategory = JSON.parse(prodSubcategory);
	                        } catch (e) {}
	                        if (typeof prodSubcategory === "object") {
	                            // normalize category names
	                            prodSubcategory = prodSubcategory.map(sto.utils.fn.cleanString);
	                            if (prodSubcategory.indexOf(sto.utils.fn.cleanString(subcategory)) !== -1) {
	                                sortedArray.push(prodBox.clone(true));
	                            } else {
	                                if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
	                                    $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
	                                        if (prodSubcategory.indexOf(sto.utils.fn.cleanString(value)) !== -1) {
	                                            sortedArray.push(prodBox.clone(true));
	                                        }
	                                    });
	                                }
	                            }
	                        } else {
	                            if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(subcategory)) {
	                                sortedArray.push(prodBox.clone(true));
	                            } else {
	                                if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
	                                    $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
	                                        if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(value)) {
	                                            sortedArray.push(prodBox.clone(true));
	                                        }
	                                    });
	                                }
	                            }
	                        }
	                    } else {
	                        var prodSubcategory = prodBox.data('category-1');
	                        try {
	                            prodSubcategory = JSON.parse(prodSubcategory);
	                        } catch (e) {}
	                        if (typeof prodSubcategory === "object") {
	                            // normalize category names
	                            prodSubcategory = prodSubcategory.map(sto.utils.fn.cleanString);
	                            if (prodSubcategory.indexOf(sto.utils.fn.cleanString(subcategory)) !== -1) {
	                                sortedArray.push(prodBox.clone(true));
	                            } else {
	                                if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
	                                    $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
	                                        if (prodSubcategory.indexOf(sto.utils.fn.cleanString(value)) !== -1) {
	                                            sortedArray.push(prodBox.clone(true));
	                                        }
	                                    });
	                                }
	                            }
	                        } else {
	                            if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(subcategory)) {
	                                sortedArray.push(prodBox.clone(true));
	                            } else {
	                                if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
	                                    $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
	                                        if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(value)) {
	                                            sortedArray.push(prodBox.clone(true));
	                                        }
	                                    });
	                                }
	                            }
	                        }
	                    }
	                } else {
	                    sortedArray.push(prodBox.clone(true));
	                }
	            });
	        } else {
	            $.each(settings.all_categories, function(idx, val) {
	                $.each($('#sto-prod-storage-' + val + ' .sto-product-container .box-item.box-item-rayon'), function(i, v) {
	                    var prodBox = $(this);
	                    if (typeof prodBox.data('price') === "undefined")
	                        prodBox.attr('data-price', (typeof prodBox.parent().data('price') !== "undefined") ? prodBox.parent().data('price') : "");
	                    if (typeof prodBox.data('id') === "undefined")
	                        prodBox.attr('data-id', (typeof prodBox.parent().data('id') !== "undefined") ? prodBox.parent().data('id') : "");
	                    if (typeof prodBox.data('category-universe') === "undefined")
	                        prodBox.attr('data-category-universe', (typeof prodBox.parent().data('category-universe') !== "undefined") ? prodBox.parent().data('category-universe') : "");
	                    if (typeof prodBox.data('category-1') === "undefined")
	                        prodBox.attr('data-category-1', (typeof prodBox.parent().data('category-1') !== "undefined") ? prodBox.parent().data('category-1') : "");
	                    if (subcategory !== null && subcategory !== "all") {
	                        if (prodBox.parent().length && prodBox.parent().data('category-1')) {
	                            var prodSubcategory = prodBox.parent().data('category-1');
	                            try {
	                                prodSubcategory = JSON.parse(prodSubcategory);
	                            } catch (e) {}
	                            if (typeof prodSubcategory === "object") {
	                                // normalize category names
	                                prodSubcategory = prodSubcategory.map(sto.utils.fn.cleanString);
	                                if (prodSubcategory.indexOf(sto.utils.fn.cleanString(subcategory)) !== -1) {
	                                    sortedArray.push(prodBox.clone(true));
	                                } else {
	                                    if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
	                                        $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
	                                            if (prodSubcategory.indexOf(sto.utils.fn.cleanString(value)) !== -1) {
	                                                sortedArray.push(prodBox.clone(true));
	                                            }
	                                        });
	                                    }
	                                }
	                            } else {
	                                if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(subcategory)) {
	                                    sortedArray.push(prodBox.clone(true));
	                                } else {
	                                    if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
	                                        $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
	                                            if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(value)) {
	                                                sortedArray.push(prodBox.clone(true));
	                                            }
	                                        });
	                                    }
	                                }
	                            }
	                        } else {
	                            var prodSubcategory = prodBox.data('category-1');
	                            try {
	                                prodSubcategory = JSON.parse(prodSubcategory);
	                            } catch (e) {}
	                            if (typeof prodSubcategory === "object") {
	                                // normalize category names
	                                prodSubcategory = prodSubcategory.map(sto.utils.fn.cleanString);
	                                if (prodSubcategory.indexOf(sto.utils.fn.cleanString(subcategory)) !== -1) {
	                                    sortedArray.push(prodBox.clone(true));
	                                } else {
	                                    if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
	                                        $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
	                                            if (prodSubcategory.indexOf(sto.utils.fn.cleanString(value)) !== -1) {
	                                                sortedArray.push(prodBox.clone(true));
	                                            }
	                                        });
	                                    }
	                                }
	                            } else {
	                                if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(subcategory)) {
	                                    sortedArray.push(prodBox.clone(true));
	                                } else {
	                                    if (typeof settings.subcategories_aliases[subcategory] !== "undefined") {
	                                        $.each(settings.subcategories_aliases[subcategory], function(idx, value) {
	                                            if (sto.utils.fn.cleanString(prodSubcategory) == sto.utils.fn.cleanString(value)) {
	                                                sortedArray.push(prodBox.clone(true));
	                                            }
	                                        });
	                                    }
	                                }
	                            }
	                        }
	                    } else {
	                        sortedArray.push(prodBox.clone(true));
	                    }
	                });
	            });
	        }
	
	        if (sorting == "ASC")
	            sortedArray.sort(function(a, b) {
	                return parseFloat($(a).data('price')) - parseFloat($(b).data('price'))
	            });
	        else if (sorting == "DESC")
	            sortedArray.sort(function(a, b) {
	                return parseFloat($(b).data('price')) - parseFloat($(a).data('price'))
	            });
	
	        return sortedArray;
	    }
	
	    function paginate_category(paginateArray, container, tracker) {
	        pagination_container.empty();
	        var currentCat = $('.sto-' + placeholder + '-category-page').attr('category');
	        var limit = (currentCat === "bijoux") ? paginationBijoux : paginationLimit;
	        var pageNumberPaginate = (pageNumber !== null) ? pageNumber : 1;
	        var paginationInProgress = 0;
	        pagination_container.pagination({
	            dataSource: paginateArray,
	            pageSize: limit,
	            pageNumber: pageNumberPaginate,
	            showPageNumbers: false,
	            showNavigator: true,
	            callback: function(data, pagination) {
	                pageNumber = pagination.pageNumber;
	                if (windowSize > 1023) {
	                    if (paginationInProgress !== 1) {
	                        if ($(window).scrollTop() > $('.sto-' + placeholder + '-category-page .sto-category-page-products').offset().top) {
	                            $(window).unbind('scroll');
	                            $(window).scrollTop(0);
	                        }
	                        data = place_banners_paginatearray(data, pagination.pageNumber, pagination.totalNumber);
	                        $('.sto-' + placeholder + '-category-page > .sto-category-page-products').empty();
	                        $.each(data, function(i, v) {
	                            if (typeof v.data('id') !== "undefined") {
	                                if ($('.sto-product-container').find('div[data-id="' + v.data('id') + '"]').find('.box-item').length > 0) {
	                                    var elemToAppend = $('.sto-product-container').find('div[data-id="' + v.data('id') + '"] .box-item').first().clone(true);
	                                } else {
	                                    var elemToAppend = $('.sto-product-container').find('div[data-id="' + v.data('id') + '"]').first().clone(true);
	                                }
	                                if (v.hasClass('eol')) elemToAppend.addClass('eol');
	                                $('.sto-' + placeholder + '-category-page > .sto-category-page-products').append(elemToAppend);
	                            } else {
	                                $('.sto-' + placeholder + '-category-page > .sto-category-page-products').append(v)
	                            }
	                        });
	                    }
	                    paginationInProgress++;
	                } else {
	                    data = place_banners_paginatearray(data, pagination.pageNumber, pagination.totalNumber);
	                    $('.sto-' + placeholder + '-category-page > .sto-category-page-products').empty();
	                    $.each(data, function(i, v) {
	                        if (typeof v.data('id') !== "undefined") {
	                            if ($('.sto-product-container').find('div[data-id="' + v.data('id') + '"]').find('.box-item').length > 0) {
	                                var elemToAppend = $('.sto-product-container').find('div[data-id="' + v.data('id') + '"] .box-item').first().clone(true);
	                            } else {
	                                var elemToAppend = $('.sto-product-container').find('div[data-id="' + v.data('id') + '"]').first().clone(true);
	                            }
	                            if (v.hasClass('eol')) elemToAppend.addClass('eol');
	                            $('.sto-' + placeholder + '-category-page > .sto-category-page-products').append(elemToAppend);
	                        } else {
	                            $('.sto-' + placeholder + '-category-page > .sto-category-page-products').append(v)
	                        }
	                    });
	                }
	            },
	            afterRender: function() {
	                $('.paginationjs-prev.J-paginationjs-previous').blur();
	                $('.paginationjs-prev.J-paginationjs-previous>a').blur();
	                $('.paginationjs-next.J-paginationjs-next').blur();
	                $('.paginationjs-next.J-paginationjs-next>a').blur();
	            }
	        });
	    }
	
	    function unpaginate_category(paginateArray) {
	        pagination_container.empty();
	        paginateArray = place_banners_paginatearray(paginateArray);
	
	        $('.sto-' + placeholder + '-category-page .sto-category-page-products').html(paginateArray);
	    }
	
	    function place_banners_paginatearray(paginateArray, pageNumber, totalNumber) {
	        var currentCat = $('.sto-' + placeholder + '-category-page').attr('category');
	
	        // if category not bijoux add eol class at the end of each end of line product (4th desktop, third in break, one in mobile)
	        if (currentCat !== "bijoux") {
	            $.each(paginateArray, function(i, v) {
	                if ((i !== 0) && ((i + 1) % hpProductLimit == 0)) {
	                    $(v).addClass('eol');
	                }
	            });
	        }
	
	        // if category culture but subcategory filter is active
	        if (currentCat === "culture" && (subcategory !== "all" && typeof settings.banners[sto.utils.fn.cleanString(subcategory)] !== "undefined")) {
	            if (typeof pageNumber !== "undefined") {
	                paginateArray = splice_paginatearray(sto.utils.fn.cleanString(subcategory), paginateArray, pageNumber);
	            } else {
	                paginateArray = splice_paginatearray(sto.utils.fn.cleanString(subcategory), paginateArray);
	            }
	        } else if (currentCat === "bijoux") {
	            if (typeof pageNumber !== "undefined") {
	                paginateArray = splice_paginatearray_bijoux(currentCat, paginateArray, pageNumber, totalNumber);
	            } else {
	                paginateArray = splice_paginatearray_bijoux(currentCat, paginateArray);
	            }
	        } else {
	            if (typeof settings.banners[currentCat] !== "undefined") {
	                if (typeof pageNumber !== "undefined") {
	                    paginateArray = splice_paginatearray(currentCat, paginateArray, pageNumber);
	                } else {
	                    paginateArray = splice_paginatearray(currentCat, paginateArray);
	                }
	            }
	        }
	
	        // if category is bijoux addClass eol at the end because of banner form
	        /*if (currentCat === "bijoux") {
	            $.each(paginateArray, function(i, v) {
	                if ((i !== 0) && ((i + 1) % hpProductLimit == 0)) {
	                    $(v).addClass('eol');
	                }
	            });
	        }*/
	
	        return paginateArray;
	    }
	
	    function splice_paginatearray(cat, paginateArray, pageNumber) {
	        var loopIndex = (typeof pageNumber !== "undefined" && pageNumber > 1) ? ((pageNumber == 3) ? pageNumber + 1 : pageNumber) : 0;
	        var spliceIndex = 0;
	        var today = new Date();
	        $.each(settings.banners[cat], function(idx, value) {
	            if (windowSize > 1023) {
	                if (typeof pageNumber !== "undefined" && pageNumber > 1 && number_available_banners(settings.banners[cat]) <= 2) {
	                    return false;
	                }
	                if (typeof pageNumber !== "undefined" && spliceIndex >= 2) {
	                    return false;
	                }
	                if (loopIndex >= number_available_banners(settings.banners[cat])) {
	                    return false;
	                }
	                if (value.date !== "all") {
	                    var bannerDate = new Date(value.date);
	                }
	                if (value.date === "all" || (today.setHours(0, 0, 0, 0) == bannerDate.setHours(0, 0, 0, 0))) {
	                    var availBanners = get_available_banners(settings.banners[cat]);
	                    var value = availBanners[loopIndex];
	                    if (windowSize <= 1023 && typeof value.urlMobile !== "undefined") {
	                        var t = $('<a href="' + value.urlMobile + '" target="_blank"><div class="sto-gamme-banner" index="' + loopIndex + '"></div></a>');
	                        t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '" + cleanStringBlackFriday(value.message) + "','urlsortie': '" + cleanStringBlackFriday(value.urlMobile) + "','event': 'blackfriday_traffickingexterne_visuels'})");
	                    } else {
	                        var t = $('<a href="' + value.url + '" target="_blank"><div class="sto-gamme-banner" index="' + loopIndex + '"></div></a>');
	                        t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '" + cleanStringBlackFriday(value.message) + "','urlsortie': '" + cleanStringBlackFriday(value.url) + "','event': 'blackfriday_traffickingexterne_visuels'})");
	                    }
	                    if (cat !== "bijoux") {
	                        if (spliceIndex == 0) {
	                            paginateArray.splice((hpProductLimit * 2), 0, t);
	                        } else if (spliceIndex == 1) {
	                            paginateArray.splice(((hpProductLimit * 4) + 1), 0, t);
	                            //paginateArray.push(t);
	                        } else if (spliceIndex == 2) {
	                            paginateArray.splice(((hpProductLimit * 4) + 2), 0, t);
	                            //paginateArray.push(t);
	                        } else if (spliceIndex == 3) {
	                            paginateArray.splice(((hpProductLimit * 4) + 3), 0, t);
	                            //paginateArray.push(t);
	                        }
	                    } else {
	                        paginateArray.splice(((hpProductLimit * 2) + loopIndex), 0, t);
	                    }
	                    loopIndex++;
	                    spliceIndex++;
	                }
	            } else {
	                if (typeof pageNumber !== "undefined" && pageNumber > 1) {
	                    return false;
	                }
	                if (value.date !== "all") {
	                    var bannerDate = new Date(value.date);
	                }
	                if (value.date === "all" || (today.setHours(0, 0, 0, 0) == bannerDate.setHours(0, 0, 0, 0))) {
	                    var availBanners = get_available_banners(settings.banners[cat]);
	                    var value = availBanners[loopIndex];
	                    if (typeof value !== "undefined") {
	                        if (windowSize <= 1023 && typeof value.urlMobile !== "undefined") {
	                            var t = $('<a href="' + value.urlMobile + '" target="_blank"><div class="sto-gamme-banner" index="' + loopIndex + '"></div></a>');
	                            t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '" + cleanStringBlackFriday(value.message) + "','urlsortie': '" + cleanStringBlackFriday(value.urlMobile) + "','event': 'blackfriday_traffickingexterne_visuels'})");
	                        } else {
	                            var t = $('<a href="' + value.url + '" target="_blank"><div class="sto-gamme-banner" index="' + loopIndex + '"></div></a>');
	                            t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '" + cleanStringBlackFriday(value.message) + "','urlsortie': '" + cleanStringBlackFriday(value.url) + "','event': 'blackfriday_traffickingexterne_visuels'})");
	                        }
	
	                        if (loopIndex === 0) {
	                            paginateArray.splice(2, 0, t);
	                        } else {
	                            paginateArray.splice(((loopIndex * 3) + 2), 0, t);
	                        }
	                    }
	
	                    loopIndex++;
	                    spliceIndex++;
	                }
	            }
	        });
	
	        return paginateArray;
	    }
	
	    function splice_paginatearray_bijoux(cat, paginateArray, pageNumber, totalNumber) {
	        var loopIndex = 0;
	        var spliceIndex = 0;
	        var today = new Date();
	        /*if (windowSize > 1023) {*/
	        if (typeof pageNumber === "undefined" || (typeof pageNumber !== "undefined" && pageNumber == 1)) {
	            $.each(settings.banners[cat], function(idx, value) {
	                if (value.date !== "all") {
	                    var bannerDate = new Date(value.date);
	                }
	                if (value.date === "all" || (today.setHours(0, 0, 0, 0) == bannerDate.setHours(0, 0, 0, 0))) {
	                    if (windowSize <= 1023 && typeof value.urlMobile !== "undefined") {
	                        var t = $('<a href="' + value.urlMobile + '" target="_blank"><div class="sto-gamme-banner" index="' + loopIndex + '"></div></a>');
	                        t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '" + cleanStringBlackFriday(value.message) + "','urlsortie': '" + cleanStringBlackFriday(value.urlMobile) + "','event': 'blackfriday_traffickingexterne_visuels'})");
	                    } else {
	                        var t = $('<a href="' + value.url + '" target="_blank"><div class="sto-gamme-banner" index="' + loopIndex + '"></div></a>');
	                        t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '" + cleanStringBlackFriday(value.message) + "','urlsortie': '" + cleanStringBlackFriday(value.url) + "','event': 'blackfriday_traffickingexterne_visuels'})");
	                    }
	                    paginateArray.splice(((hpProductLimit * 2) + loopIndex), 0, t);
	
	                    loopIndex++;
	                    spliceIndex++;
	                }
	            });
	        }
	        /*} else {
	            if (typeof pageNumber !== "undefined") {
	                loopIndex = pageNumber-1;
	            }
	            $.each(settings.banners[cat], function(idx, value) {
	                if (typeof pageNumber !== "undefined" && (pageNumber*paginationBijoux <= totalNumber) && spliceIndex > 0) { return false; }
	                if (loopIndex >= settings.banners[cat].length) { return false; }
	                if (value.date !== "all") { var bannerDate = new Date(value.date); }
	                if (value.date === "all" || (today.setHours(0,0,0,0) == bannerDate.setHours(0,0,0,0))) {
	                    if (windowSize <= 1023 && typeof value.urlMobile !== "undefined") {
	                        var t = $('<a href="'+value.urlMobile+'" target="_blank"><div class="sto-gamme-banner" index="'+loopIndex+'"></div></a>');
	                        t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '"+cleanStringBlackFriday(value.message)+"','urlsortie': '"+cleanStringBlackFriday(value.urlMobile)+"','event': 'blackfriday_traffickingexterne_visuels'})");
	                    } else {
	                        var t = $('<a href="'+value.url+'" target="_blank"><div class="sto-gamme-banner" index="'+loopIndex+'"></div></a>');
	                        t.attr('onclick', "dataLayer.push({'nomcampagne': 'blackfriday2019','nomclicsortant': '"+cleanStringBlackFriday(value.message)+"','urlsortie': '"+cleanStringBlackFriday(value.url)+"','event': 'blackfriday_traffickingexterne_visuels'})");
	                    }
	                    if (spliceIndex == 0) {
	                        paginateArray.splice((hpProductLimit * 2),0,t);
	                    } else if (spliceIndex == 1) {
	                        paginateArray.splice(((hpProductLimit * 4) + 1),0,t);
	                        //paginateArray.push(t);
	                    } else {
	                        paginateArray.push(t);
	                    }
	
	                    loopIndex++;
	                    spliceIndex++;
	                }
	            });
	        }*/
	
	        return paginateArray;
	    }
	
	    function number_available_banners(banners) {
	        var nb = 0;
	        var today = new Date();
	        $.each(banners, function(idx, value) {
	            if (value.date !== "all") {
	                var bannerDate = new Date(value.date);
	            }
	            if (value.date === "all" || (today.setHours(0, 0, 0, 0) == bannerDate.setHours(0, 0, 0, 0))) {
	                nb++;
	            }
	        });
	        return nb;
	    }
	
	    function get_available_banners(banners) {
	        var newBanners = [];
	        var today = new Date();
	        $.each(banners, function(idx, value) {
	            if (value.date !== "all") {
	                var bannerDate = new Date(value.date);
	            }
	            if (value.date === "all" || (today.setHours(0, 0, 0, 0) == bannerDate.setHours(0, 0, 0, 0))) {
	                newBanners.push(value);
	            }
	        });
	        return newBanners;
	    }
	
	    function resize_gutters() {
	        $('.sto-' + placeholder + '-gutter>div, .sto-' + placeholder + '-gutter>div>.sto-' + placeholder + '-gutter-top').css({
	            'height': $('.row[layout\\:fragment="pagecontent"]').outerHeight() + 'px'
	        });
	        $('.sto-' + placeholder + '-gutter>div').css({
	            'top': $('.sto-' + placeholder + '-global').offset().top + 'px'
	        });
	    }
	
	    function tracking_products_portal(elem) {
	        elem.find('a:not(.btn)').attr('target', "_blank");
	        if (elem.find('.btn.btn-achat-ligne').length)
	            elem.find('.btn.btn-achat-ligne').attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","urlsortie": "' + cleanStringBlackFriday(elem.data('trackurl')) + '","product":{ "id": "' + elem.data('trackid') + '","name": "' + cleanStringBlackFriday(elem.data('trackname')) + '","price": "' + elem.data("trackprice") + '","promo": "blackfriday2019","category": "' + cleanStringBlackFriday(elem.data('trackcategory')) + '","subcategory": "' + cleanStringBlackFriday(elem.data('tracksubcategory')) + '"}, "event":"blackfriday_traffickingexterne_acheter"}); window.open("' + elem.data('trackurl') + '")').removeAttr('target');
	        if (elem.find('.btn.btn-memo').length)
	            elem.find('.btn.btn-memo').attr('onclick', 'dataLayer.push({"product":{ "id": "' + elem.data('trackid') + '","name": "' + cleanStringBlackFriday(elem.data('trackname')) + '","price": "' + elem.data('trackprice') + '","promo": "blackfriday2019","category": "' + cleanStringBlackFriday(elem.data('trackcategory')) + '","subcategory": "' + cleanStringBlackFriday(elem.data('tracksubcategory')) + '", "quantity": "1"}});').removeAttr('target');
	    }
	
	    function tracking_products_api(elem) {
	        var urlSortie = elem.find('.btn.btn-achat-ligne').attr('href'),
	            productId = elem.data('id'),
	            productName = elem.find('.box-item-title>a').text(),
	            productPrice = elem.data('price'),
	            productCategory = elem.data('category-universe'),
	            productSubcategory = elem.data('category-1');
	
	        if (typeof productSubcategory === "object") {
	            productSubcategory = JSON.stringify(productSubcategory);
	        }
	
	        if (productPrice && "" !== productPrice) {
	            productPrice = productPrice.toString();
	            var g = productPrice.slice(-2),
	                y = productPrice.slice(0, -2);
	            productPrice = y + "-" + g
	        }
	
	        elem.find('a:not(.btn.btn-achat-ligne)').attr('target', "_blank");
	        if (elem.find('.btn.btn-achat-ligne').length)
	            elem.find('.btn.btn-achat-ligne').attr('href', '#').attr('onclick', 'dataLayer.push({"nomcampagne": "blackfriday2019","urlsortie": "' + cleanStringBlackFriday(urlSortie) + '","product":{ "id": "' + productId + '","name": "' + cleanStringBlackFriday(productName) + '","price": "' + productPrice + '","promo": "blackfriday2019","category": "' + cleanStringBlackFriday(productCategory) + '","subcategory": "' + cleanStringBlackFriday(productSubcategory) + '"}, "event":"blackfriday_traffickingexterne_acheter"}); window.open("' + urlSortie + '")').removeAttr('target');
	    }
	
	    function fetch_backup_products() {
	        var hightech1 = $('#sto-prod-storage-hightech .sto-product-container').find('.ng-scope>.box-item').first().clone(true).attr('slider-nav', $('#sto-prod-storage-hightech .sto-product-container').find('.ng-scope>.box-item').first().find('.box-item-title').text());
	        if (hightech1.attr('slider-nav').length >= 30) {
	            hightech1.attr('slider-nav', hightech1.attr('slider-nav').slice(0, 30) + "...")
	        }
	        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(hightech1);
	        var culture1 = $('#sto-prod-storage-culture .sto-product-container').find('.ng-scope>.box-item').first().clone(true).attr('slider-nav', $('#sto-prod-storage-culture .sto-product-container').find('.ng-scope>.box-item').first().find('.box-item-title').text());
	        if (culture1.attr('slider-nav').length >= 30) {
	            culture1.attr('slider-nav', culture1.attr('slider-nav').slice(0, 30) + "...")
	        }
	        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(culture1);
	        var maison1 = $('#sto-prod-storage-maison .sto-product-container').find('.ng-scope>.box-item').first().clone(true).attr('slider-nav', $('#sto-prod-storage-maison .sto-product-container').find('.ng-scope>.box-item').first().find('.box-item-title').text());
	        if (maison1.attr('slider-nav').length >= 30) {
	            maison1.attr('slider-nav', maison1.attr('slider-nav').slice(0, 30) + "...")
	        }
	        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(maison1);
	        var parapharmacie1 = $('#sto-prod-storage-parapharmacie .sto-product-container').find('.ng-scope>.box-item').first().clone(true).attr('slider-nav', $('#sto-prod-storage-parapharmacie .sto-product-container').find('.ng-scope>.box-item').first().find('.box-item-title').text());
	        if (parapharmacie1.attr('slider-nav').length >= 30) {
	            parapharmacie1.attr('slider-nav', parapharmacie1.attr('slider-nav').slice(0, 30) + "...")
	        }
	        $('.sto-' + placeholder + '-storage #sto-prod-storage-slider').append(parapharmacie1);
	    }
	} catch (e) {
	    sto.tracker().sendHit({
	        "ta": "err",
	        "te": "onBuild-environnement",
	        "tl": e
	    });
	}


/***/ }),
/* 201 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/*
	 * pagination.js 2.1.4
	 * A jQuery plugin to provide simple yet fully customisable pagination.
	 * https://github.com/superRaytin/paginationjs
	 *
	 * Homepage: http://pagination.js.org
	 *
	 * Copyright 2014-2100, superRaytin
	 * Released under the MIT license.
	 */
	
	(function(global, $) {
	
	    if (typeof $ === 'undefined') {
	        throwError('Pagination requires jQuery.');
	    }
	
	    var pluginName = 'pagination';
	
	    var pluginHookMethod = 'addHook';
	
	    var eventPrefix = '__pagination-';
	
	    // Conflict, use backup
	    if ($.fn.pagination) {
	        pluginName = 'pagination2';
	    }
	
	    $.fn[pluginName] = function(options) {
	
	        if (typeof options === 'undefined') {
	            return this;
	        }
	
	        var container = $(this);
	
	        var attributes = $.extend({}, $.fn[pluginName].defaults, options);
	
	        var pagination = {
	
	            initialize: function() {
	                var self = this;
	
	                // Cache attributes of current instance
	                if (!container.data('pagination')) {
	                    container.data('pagination', {});
	                }
	
	                if (self.callHook('beforeInit') === false) return;
	
	                // Pagination has been initialized, destroy it
	                if (container.data('pagination').initialized) {
	                    $('.paginationjs', container).remove();
	                }
	
	                // Whether to disable Pagination at the initialization
	                self.disabled = !!attributes.disabled;
	
	                // Model will be passed to the callback function
	                var model = self.model = {
	                    pageRange: attributes.pageRange,
	                    pageSize: attributes.pageSize
	                };
	
	                // dataSource`s type is unknown, parse it to find true data
	                self.parseDataSource(attributes.dataSource, function(dataSource) {
	
	                    // Currently in asynchronous mode
	                    self.isAsync = Helpers.isString(dataSource);
	                    if (Helpers.isArray(dataSource)) {
	                        model.totalNumber = attributes.totalNumber = dataSource.length;
	                    }
	
	                    // Currently in asynchronous mode and a totalNumberLocator is specified
	                    self.isDynamicTotalNumber = self.isAsync && attributes.totalNumberLocator;
	
	                    // There is only one page
	                    if (attributes.hideWhenLessThanOnePage) {
	                        if (self.getTotalPage() <= 1) return;
	                    }
	
	                    var el = self.render(true);
	
	                    // Add extra className to the pagination element
	                    if (attributes.className) {
	                        el.addClass(attributes.className);
	                    }
	
	                    model.el = el;
	
	                    // Append/prepend pagination element to the container
	                    container[attributes.position === 'bottom' ? 'append' : 'prepend'](el);
	
	                    // Bind events
	                    self.observer();
	
	                    // Pagination is currently initialized
	                    container.data('pagination').initialized = true;
	
	                    // Will be invoked after initialized
	                    self.callHook('afterInit', el);
	                });
	            },
	
	            render: function(isBoot) {
	                var self = this;
	                var model = self.model;
	                var el = $(".paginationjs").length > 0 ? $(".paginationjs") : $('<div class="paginationjs"></div>');
	                var isForced = isBoot !== true;
	
	                self.callHook('beforeRender', isForced);
	
	                var currentPage = model.pageNumber || attributes.pageNumber;
	                var pageRange = attributes.pageRange;
	                var totalPage = self.getTotalPage();
	
	                var rangeStart = currentPage - pageRange;
	                var rangeEnd = currentPage + pageRange;
	
	                if (rangeEnd > totalPage) {
	                    rangeEnd = totalPage;
	                    rangeStart = totalPage - pageRange * 2;
	                    rangeStart = rangeStart < 1 ? 1 : rangeStart;
	                }
	
	                if (rangeStart <= 1) {
	                    rangeStart = 1;
	                    rangeEnd = Math.min(pageRange * 2 + 1, totalPage);
	                }
	
	                el.html(self.generateHTML({
	                    currentPage: currentPage,
	                    pageRange: pageRange,
	                    rangeStart: rangeStart,
	                    rangeEnd: rangeEnd
	                }));
	
	                self.callHook('afterRender', isForced);
	
	                return el;
	            },
	
	            // Generate HTML content from the template
	            generateHTML: function(args) {
	                var self = this;
	                var currentPage = args.currentPage;
	                var totalPage = self.getTotalPage();
	                var rangeStart = args.rangeStart;
	                var rangeEnd = args.rangeEnd;
	
	                var totalNumber = self.getTotalNumber();
	
	                var showPrevious = attributes.showPrevious;
	                var showNext = attributes.showNext;
	                var showPageNumbers = attributes.showPageNumbers;
	                var showNavigator = attributes.showNavigator;
	                var showGoInput = attributes.showGoInput;
	                var showGoButton = attributes.showGoButton;
	
	                var pageLink = attributes.pageLink;
	                var prevText = attributes.prevText;
	                var nextText = attributes.nextText;
	                var ellipsisText = attributes.ellipsisText;
	                var goButtonText = attributes.goButtonText;
	
	                var classPrefix = attributes.classPrefix;
	                var activeClassName = attributes.activeClassName;
	                var disableClassName = attributes.disableClassName;
	                var ulClassName = attributes.ulClassName;
	
	                var html = '';
	                var goInput = '<input type="text" class="J-paginationjs-go-pagenumber">';
	                var goButton = '<input type="button" class="J-paginationjs-go-button" value="' + goButtonText + '">';
	                var formattedString;
	                var i;
	
	                var formatNavigator = $.isFunction(attributes.formatNavigator) ? attributes.formatNavigator(currentPage, totalPage, totalNumber) : attributes.formatNavigator;
	                var formatGoInput = $.isFunction(attributes.formatGoInput) ? attributes.formatGoInput(goInput, currentPage, totalPage, totalNumber) : attributes.formatGoInput;
	                var formatGoButton = $.isFunction(attributes.formatGoButton) ? attributes.formatGoButton(goButton, currentPage, totalPage, totalNumber) : attributes.formatGoButton;
	
	                var autoHidePrevious = $.isFunction(attributes.autoHidePrevious) ? attributes.autoHidePrevious() : attributes.autoHidePrevious;
	                var autoHideNext = $.isFunction(attributes.autoHideNext) ? attributes.autoHideNext() : attributes.autoHideNext;
	
	                var header = $.isFunction(attributes.header) ? attributes.header(currentPage, totalPage, totalNumber) : attributes.header;
	                var footer = $.isFunction(attributes.footer) ? attributes.footer(currentPage, totalPage, totalNumber) : attributes.footer;
	
	                // Whether to display header
	                if (header) {
	                    formattedString = self.replaceVariables(header, {
	                        currentPage: currentPage,
	                        totalPage: totalPage,
	                        totalNumber: totalNumber
	                    });
	                    html += formattedString;
	                }
	
	                if (showPrevious || showPageNumbers || showNext) {
	                    html += '<div class="paginationjs-pages">';
	
	                    if (ulClassName) {
	                        html += '<ul class="' + ulClassName + '">';
	                    } else {
	                        html += '<ul>';
	                    }
	
	                    // Whether to display the Previous button
	                    if (showPrevious) {
	                        if (currentPage <= 1) {
	                            if (!autoHidePrevious) {
	                                html += '<li class="' + classPrefix + '-prev ' + disableClassName + '"><a>' + prevText + '<\/a><\/li>';
	                            }
	                        } else {
	                            html += '<li class="' + classPrefix + '-prev J-paginationjs-previous" data-num="' + (currentPage - 1) + '" title="Previous page"><a href="' + pageLink + '">' + prevText + '<\/a><\/li>';
	                        }
	                    }
	
	                    // Whether to display the pages
	                    if (showPageNumbers) {
	                        if (rangeStart <= 3) {
	                            for (i = 1; i < rangeStart; i++) {
	                                if (i == currentPage) {
	                                    html += '<li class="' + classPrefix + '-page J-paginationjs-page ' + activeClassName + '" data-num="' + i + '"><a>' + i + '<\/a><\/li>';
	                                } else {
	                                    html += '<li class="' + classPrefix + '-page J-paginationjs-page" data-num="' + i + '"><a href="' + pageLink + '">' + i + '<\/a><\/li>';
	                                }
	                            }
	                        } else {
	                            if (attributes.showFirstOnEllipsisShow) {
	                                html += '<li class="' + classPrefix + '-page ' + classPrefix + '-first J-paginationjs-page" data-num="1"><a href="' + pageLink + '">1<\/a><\/li>';
	                            }
	                            html += '<li class="' + classPrefix + '-ellipsis ' + disableClassName + '"><a>' + ellipsisText + '<\/a><\/li>';
	                        }
	
	                        for (i = rangeStart; i <= rangeEnd; i++) {
	                            if (i == currentPage) {
	                                html += '<li class="' + classPrefix + '-page J-paginationjs-page ' + activeClassName + '" data-num="' + i + '"><a>' + i + '<\/a><\/li>';
	                            } else {
	                                html += '<li class="' + classPrefix + '-page J-paginationjs-page" data-num="' + i + '"><a href="' + pageLink + '">' + i + '<\/a><\/li>';
	                            }
	                        }
	
	                        if (rangeEnd >= totalPage - 2) {
	                            for (i = rangeEnd + 1; i <= totalPage; i++) {
	                                html += '<li class="' + classPrefix + '-page J-paginationjs-page" data-num="' + i + '"><a href="' + pageLink + '">' + i + '<\/a><\/li>';
	                            }
	                        } else {
	                            html += '<li class="' + classPrefix + '-ellipsis ' + disableClassName + '"><a>' + ellipsisText + '<\/a><\/li>';
	
	                            if (attributes.showLastOnEllipsisShow) {
	                                html += '<li class="' + classPrefix + '-page ' + classPrefix + '-last J-paginationjs-page" data-num="' + totalPage + '"><a href="' + pageLink + '">' + totalPage + '<\/a><\/li>';
	                            }
	                        }
	                    }
	
	                    // Whether to display the Next button
	                    if (showNext) {
	                        if (currentPage >= totalPage) {
	                            if (!autoHideNext) {
	                                html += '<li class="' + classPrefix + '-next ' + disableClassName + '"><a>' + nextText + '<\/a><\/li>';
	                            }
	                        } else {
	                            html += '<li class="' + classPrefix + '-next J-paginationjs-next" data-num="' + (currentPage + 1) + '" title="Next page"><a href="' + pageLink + '">' + nextText + '<\/a><\/li>';
	                        }
	                    }
	                    html += '<\/ul><\/div>';
	                }
	
	                // Whether to display the navigator
	                if (showNavigator) {
	                    if (formatNavigator) {
	                        formattedString = self.replaceVariables(formatNavigator, {
	                            currentPage: currentPage,
	                            totalPage: totalPage,
	                            totalNumber: totalNumber
	                        });
	                        html += '<div class="' + classPrefix + '-nav J-paginationjs-nav">' + formattedString + '<\/div>';
	                    }
	                }
	
	                // Whether to display the Go input
	                if (showGoInput) {
	                    if (formatGoInput) {
	                        formattedString = self.replaceVariables(formatGoInput, {
	                            currentPage: currentPage,
	                            totalPage: totalPage,
	                            totalNumber: totalNumber,
	                            input: goInput
	                        });
	                        html += '<div class="' + classPrefix + '-go-input">' + formattedString + '</div>';
	                    }
	                }
	
	                // Whether to display the Go button
	                if (showGoButton) {
	                    if (formatGoButton) {
	                        formattedString = self.replaceVariables(formatGoButton, {
	                            currentPage: currentPage,
	                            totalPage: totalPage,
	                            totalNumber: totalNumber,
	                            button: goButton
	                        });
	                        html += '<div class="' + classPrefix + '-go-button">' + formattedString + '</div>';
	                    }
	                }
	
	                // Whether to display footer
	                if (footer) {
	                    formattedString = self.replaceVariables(footer, {
	                        currentPage: currentPage,
	                        totalPage: totalPage,
	                        totalNumber: totalNumber
	                    });
	                    html += formattedString;
	                }
	
	                return html;
	            },
	
	            // Find totalNumber from the remote response
	            // Only available in asynchronous mode
	            findTotalNumberFromRemoteResponse: function(response) {
	                var self = this;
	                self.model.totalNumber = attributes.totalNumberLocator(response);
	            },
	
	            // Go to the specified page
	            go: function(number, callback) {
	                var self = this;
	                var model = self.model;
	
	                if (self.disabled) return;
	
	                var pageNumber = number;
	                pageNumber = parseInt(pageNumber);
	
	                // Page number is out of bounds
	                if (!pageNumber || pageNumber < 1) return;
	
	                var pageSize = attributes.pageSize;
	                var totalNumber = self.getTotalNumber();
	                var totalPage = self.getTotalPage();
	
	                // Page number is out of bounds
	                if (totalNumber > 0) {
	                    if (pageNumber > totalPage) return;
	                }
	
	                // Pick data fragment in synchronous mode
	                if (!self.isAsync) {
	                    render(self.getDataFragment(pageNumber));
	                    return;
	                }
	
	                var postData = {};
	                var alias = attributes.alias || {};
	                postData[alias.pageSize ? alias.pageSize : 'pageSize'] = pageSize;
	                postData[alias.pageNumber ? alias.pageNumber : 'pageNumber'] = pageNumber;
	
	                var ajaxParams = $.isFunction(attributes.ajax) ? attributes.ajax() : attributes.ajax;
	                var formatAjaxParams = {
	                    type: 'get',
	                    cache: false,
	                    data: {},
	                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
	                    dataType: 'json',
	                    async: true
	                };
	
	                $.extend(true, formatAjaxParams, ajaxParams);
	                $.extend(formatAjaxParams.data, postData);
	
	                formatAjaxParams.url = attributes.dataSource;
	                formatAjaxParams.success = function(response) {
	                    if (self.isDynamicTotalNumber) {
	                        self.findTotalNumberFromRemoteResponse(response);
	                    } else {
	                        self.model.totalNumber = attributes.totalNumber;
	                    }
	                    render(self.filterDataByLocator(response));
	                };
	                formatAjaxParams.error = function(jqXHR, textStatus, errorThrown) {
	                    attributes.formatAjaxError && attributes.formatAjaxError(jqXHR, textStatus, errorThrown);
	                    self.enable();
	                };
	
	                self.disable();
	
	                $.ajax(formatAjaxParams);
	
	                function render(data) {
	                    // Will be invoked before paging
	                    if (self.callHook('beforePaging', pageNumber) === false) return false;
	
	                    // Pagination direction
	                    model.direction = typeof model.pageNumber === 'undefined' ? 0 : (pageNumber > model.pageNumber ? 1 : -1);
	
	                    model.pageNumber = pageNumber;
	
	                    self.render();
	
	                    if (self.disabled && self.isAsync) {
	                        // enable pagination
	                        self.enable();
	                    }
	
	                    // cache model data
	                    container.data('pagination').model = model;
	
	                    // format result data before callback invoked
	                    if (attributes.formatResult) {
	                        var cloneData = $.extend(true, [], data);
	                        if (!Helpers.isArray(data = attributes.formatResult(cloneData))) {
	                            data = cloneData;
	                        }
	                    }
	
	                    container.data('pagination').currentPageData = data;
	
	                    // invoke callback
	                    self.doCallback(data, callback);
	
	                    self.callHook('afterPaging', pageNumber);
	
	                    // pageNumber now is the first page
	                    if (pageNumber == 1) {
	                        self.callHook('afterIsFirstPage');
	                    }
	
	                    // pageNumber now is the last page
	                    if (pageNumber == self.getTotalPage()) {
	                        self.callHook('afterIsLastPage');
	                    }
	                }
	            },
	
	            doCallback: function(data, customCallback) {
	                var self = this;
	                var model = self.model;
	
	                if ($.isFunction(customCallback)) {
	                    customCallback(data, model);
	                } else if ($.isFunction(attributes.callback)) {
	                    attributes.callback(data, model);
	                }
	            },
	
	            destroy: function() {
	                // Before destroy
	                if (this.callHook('beforeDestroy') === false) return;
	
	                this.model.el.remove();
	                container.off();
	
	                // Remove style element
	                $('#paginationjs-style').remove();
	
	                // After destroyed
	                this.callHook('afterDestroy');
	            },
	
	            previous: function(callback) {
	                this.go(this.model.pageNumber - 1, callback);
	            },
	
	            next: function(callback) {
	                this.go(this.model.pageNumber + 1, callback);
	            },
	
	            disable: function() {
	                var self = this;
	                var source = self.isAsync ? 'async' : 'sync';
	
	                // Before disabled
	                if (self.callHook('beforeDisable', source) === false) return;
	
	                self.disabled = true;
	                self.model.disabled = true;
	
	                // After disabled
	                self.callHook('afterDisable', source);
	            },
	
	            enable: function() {
	                var self = this;
	                var source = self.isAsync ? 'async' : 'sync';
	
	                // Before enabled
	                if (self.callHook('beforeEnable', source) === false) return;
	
	                self.disabled = false;
	                self.model.disabled = false;
	
	                // After enabled
	                self.callHook('afterEnable', source);
	            },
	
	            refresh: function(callback) {
	                this.go(this.model.pageNumber, callback);
	            },
	
	            show: function() {
	                var self = this;
	
	                if (self.model.el.is(':visible')) return;
	
	                self.model.el.show();
	            },
	
	            hide: function() {
	                var self = this;
	
	                if (!self.model.el.is(':visible')) return;
	
	                self.model.el.hide();
	            },
	
	            // Parse variables in the template
	            replaceVariables: function(template, variables) {
	                var formattedString;
	
	                for (var key in variables) {
	                    var value = variables[key];
	                    var regexp = new RegExp('<%=\\s*' + key + '\\s*%>', 'img');
	
	                    formattedString = (formattedString || template).replace(regexp, value);
	                }
	
	                return formattedString;
	            },
	
	            // Get data fragment
	            getDataFragment: function(number) {
	                var pageSize = attributes.pageSize;
	                var dataSource = attributes.dataSource;
	                var totalNumber = this.getTotalNumber();
	
	                var start = pageSize * (number - 1) + 1;
	                var end = Math.min(number * pageSize, totalNumber);
	
	                return dataSource.slice(start - 1, end);
	            },
	
	            // Get total number
	            getTotalNumber: function() {
	                return this.model.totalNumber || attributes.totalNumber || 0;
	            },
	
	            // Get total page
	            getTotalPage: function() {
	                return Math.ceil(this.getTotalNumber() / attributes.pageSize);
	            },
	
	            // Get locator
	            getLocator: function(locator) {
	                var result;
	
	                if (typeof locator === 'string') {
	                    result = locator;
	                } else if ($.isFunction(locator)) {
	                    result = locator();
	                } else {
	                    throwError('"locator" is incorrect. (String | Function)');
	                }
	
	                return result;
	            },
	
	            // Filter data by "locator"
	            filterDataByLocator: function(dataSource) {
	                var locator = this.getLocator(attributes.locator);
	                var filteredData;
	
	                // Datasource is an Object, use "locator" to locate the true data
	                if (Helpers.isObject(dataSource)) {
	                    try {
	                        $.each(locator.split('.'), function(index, item) {
	                            filteredData = (filteredData ? filteredData : dataSource)[item].clone(true);
	                        });
	                    } catch (e) {}
	
	                    if (!filteredData) {
	                        throwError('dataSource.' + locator + ' is undefined.');
	                    } else if (!Helpers.isArray(filteredData)) {
	                        throwError('dataSource.' + locator + ' must be an Array.');
	                    }
	                }
	
	                return filteredData || dataSource;
	            },
	
	            // Parse dataSource
	            parseDataSource: function(dataSource, callback) {
	                var self = this;
	
	                if (Helpers.isObject(dataSource)) {
	                    callback(attributes.dataSource = self.filterDataByLocator(dataSource));
	                } else if (Helpers.isArray(dataSource)) {
	                    callback(attributes.dataSource = dataSource);
	                } else if ($.isFunction(dataSource)) {
	                    attributes.dataSource(function(data) {
	                        if (!Helpers.isArray(data)) {
	                            throwError('The parameter of "done" Function should be an Array.');
	                        }
	                        self.parseDataSource.call(self, data, callback);
	                    });
	                } else if (typeof dataSource === 'string') {
	                    if (/^https?|file:/.test(dataSource)) {
	                        attributes.ajaxDataType = 'jsonp';
	                    }
	                    callback(dataSource);
	                } else {
	                    throwError('Unexpected type of "dataSource".');
	                }
	            },
	
	            callHook: function(hook) {
	                var paginationData = container.data('pagination');
	                var result;
	
	                var args = Array.prototype.slice.apply(arguments);
	                args.shift();
	
	                if (attributes[hook] && $.isFunction(attributes[hook])) {
	                    if (attributes[hook].apply(global, args) === false) {
	                        result = false;
	                    }
	                }
	
	                if (paginationData.hooks && paginationData.hooks[hook]) {
	                    $.each(paginationData.hooks[hook], function(index, item) {
	                        if (item.apply(global, args) === false) {
	                            result = false;
	                        }
	                    });
	                }
	
	                return result !== false;
	            },
	
	            observer: function() {
	                var self = this;
	                var el = $(".paginationjs");
	
	                // Go to specified page number
	                container.on(eventPrefix + 'go', function(event, pageNumber, done) {
	                    pageNumber = parseInt($.trim(pageNumber));
	
	                    if (!pageNumber) return;
	
	                    if (!$.isNumeric(pageNumber)) {
	                        throwError('"pageNumber" is incorrect. (Number)');
	                    }
	
	                    self.go(pageNumber, done);
	                });
	
	                // Page number button click
	                el.delegate('.J-paginationjs-page', 'click', function(event) {
	                    var current = $(event.currentTarget);
	                    var pageNumber = $.trim(current.attr('data-num'));
	
	                    if (!pageNumber || current.hasClass(attributes.disableClassName) || current.hasClass(attributes.activeClassName)) return;
	
	                    // Before page button clicked
	                    if (self.callHook('beforePageOnClick', event, pageNumber) === false) return false;
	
	                    self.go(pageNumber);
	
	                    // After page button clicked
	                    self.callHook('afterPageOnClick', event, pageNumber);
	
	                    if (!attributes.pageLink) return false;
	                });
	
	                // Previous button click
	                el.delegate('.J-paginationjs-previous', 'click', function(event) {
	                    if ($(window).scrollTop() > $('.sto-category-page-products').offset().top) {
	                        $(window).scrollTop(0);
	                    }
	                    var current = $(event.currentTarget);
	                    var pageNumber = $.trim(current.attr('data-num'));
	
	                    if (!pageNumber || current.hasClass(attributes.disableClassName)) return;
	
	                    // Before previous clicked
	                    if (self.callHook('beforePreviousOnClick', event, pageNumber) === false) return false;
	
	                    self.go(pageNumber);
	
	                    // After previous clicked
	                    self.callHook('afterPreviousOnClick', event, pageNumber);
	
	                    if (!attributes.pageLink) return false;
	                });
	
	                // Next button click
	                el.delegate('.J-paginationjs-next', 'click', function(event) {
	                    if ($(window).scrollTop() > $('.sto-category-page-products').offset().top) {
	                        $(window).scrollTop(0);
	                    }
	                    var current = $(event.currentTarget);
	                    var pageNumber = $.trim(current.attr('data-num'));
	
	
	                    if (!pageNumber || current.hasClass(attributes.disableClassName)) return;
	
	                    // Before next clicked
	                    if (self.callHook('beforeNextOnClick', event, pageNumber) === false) return false;
	
	                    self.go(pageNumber);
	
	                    // After next clicked
	                    self.callHook('afterNextOnClick', event, pageNumber);
	
	                    if (!attributes.pageLink) return false;
	                });
	
	                //Return to first page after filter
	                /*$('.sto-subcategory-filter-list>div').on('click', function(event) {
	                    var current = $(event.currentTarget);
	                    var pageNumber = $.trim(current.attr('data-num'));
	
	                    console.log('current', current);
	                    console.log('pageNumber', pageNumber);
	
	                    if (!pageNumber || current.hasClass(attributes.disableClassName) || current.hasClass(attributes.activeClassName)) return;
	
	                    // Before page button clicked
	                    if (self.callHook('beforePageOnClick', event, pageNumber) === false) return false;
	
	                    self.go(pageNumber);
	
	                    // After page button clicked
	                    self.callHook('afterPageOnClick', event, pageNumber);
	
	                    if (!attributes.pageLink) return false;
	                });*/
	
	                // Go button click
	                el.delegate('.J-paginationjs-go-button', 'click', function(event) {
	                    var pageNumber = $('.J-paginationjs-go-pagenumber', el).val();
	
	                    // Before Go button clicked
	                    if (self.callHook('beforeGoButtonOnClick', event, pageNumber) === false) return false;
	
	                    container.trigger(eventPrefix + 'go', pageNumber);
	
	                    // After Go button clicked
	                    self.callHook('afterGoButtonOnClick', event, pageNumber);
	                });
	
	                // go input enter
	                el.delegate('.J-paginationjs-go-pagenumber', 'keyup', function(event) {
	                    if (event.which === 13) {
	                        var pageNumber = $(event.currentTarget).val();
	
	                        // Before Go input enter
	                        if (self.callHook('beforeGoInputOnEnter', event, pageNumber) === false) return false;
	
	                        container.trigger(eventPrefix + 'go', pageNumber);
	
	                        // Regains focus
	                        $('.J-paginationjs-go-pagenumber', el).focus();
	
	                        // After Go input enter
	                        self.callHook('afterGoInputOnEnter', event, pageNumber);
	                    }
	                });
	
	                // Previous page
	                container.on(eventPrefix + 'previous', function(event, done) {
	                    self.previous(done);
	                });
	
	                // Next page
	                container.on(eventPrefix + 'next', function(event, done) {
	                    self.next(done);
	                });
	
	                // Disable
	                container.on(eventPrefix + 'disable', function() {
	                    self.disable();
	                });
	
	                // Enable
	                container.on(eventPrefix + 'enable', function() {
	                    self.enable();
	                });
	
	                // Refresh
	                container.on(eventPrefix + 'refresh', function(event, done) {
	                    self.refresh(done);
	                });
	
	                // Show
	                container.on(eventPrefix + 'show', function() {
	                    self.show();
	                });
	
	                // Hide
	                container.on(eventPrefix + 'hide', function() {
	                    self.hide();
	                });
	
	                // Destroy
	                container.on(eventPrefix + 'destroy', function() {
	                    self.destroy();
	                });
	
	                // Whether to load the default page
	                var validTotalPage = Math.max(self.getTotalPage(), 1)
	                var defaultPageNumber = attributes.pageNumber;
	                // Default pageNumber should be 1 when totalNumber is dynamic
	                if (self.isDynamicTotalNumber) {
	                    defaultPageNumber = 1;
	                }
	                if (attributes.triggerPagingOnInit) {
	                    container.trigger(eventPrefix + 'go', Math.min(defaultPageNumber, validTotalPage));
	                }
	            }
	        };
	
	        // Pagination has been initialized
	        if (container.data('pagination') && container.data('pagination').initialized === true) {
	            // Handle events
	            if ($.isNumeric(options)) {
	                // eg: container.pagination(5)
	                container.trigger.call(this, eventPrefix + 'go', options, arguments[1]);
	                return this;
	            } else if (typeof options === 'string') {
	                var args = Array.prototype.slice.apply(arguments);
	                args[0] = eventPrefix + args[0];
	
	                switch (options) {
	                    case 'previous':
	                    case 'next':
	                    case 'go':
	                    case 'disable':
	                    case 'enable':
	                    case 'refresh':
	                    case 'show':
	                    case 'hide':
	                    case 'destroy':
	                        container.trigger.apply(this, args);
	                        break;
	                        // Get selected page number
	                    case 'getSelectedPageNum':
	                        if (container.data('pagination').model) {
	                            return container.data('pagination').model.pageNumber;
	                        } else {
	                            return container.data('pagination').attributes.pageNumber;
	                        }
	                        // Get total page
	                    case 'getTotalPage':
	                        return Math.ceil(container.data('pagination').model.totalNumber / container.data('pagination').model.pageSize);
	                        // Get data of selected page
	                    case 'getSelectedPageData':
	                        return container.data('pagination').currentPageData;
	                        // Whether pagination has been disabled
	                    case 'isDisabled':
	                        return container.data('pagination').model.disabled === true;
	                    default:
	                        throwError('Unknown action: ' + options);
	                }
	                return this;
	            } else {
	                // Uninstall the old instance before initializing a new one
	                uninstallPlugin(container);
	            }
	        } else {
	            if (!Helpers.isObject(options)) throwError('Illegal options');
	        }
	
	        // Check parameters
	        parameterChecker(attributes);
	
	        pagination.initialize();
	
	        return this;
	    };
	
	    // Instance defaults
	    $.fn[pluginName].defaults = {
	
	        // Data source
	        // Array | String | Function | Object
	        //dataSource: '',
	
	        // String | Function
	        //locator: 'data',
	
	        // Find totalNumber from remote response, the totalNumber will be ignored when totalNumberLocator is specified
	        // Function
	        //totalNumberLocator: function() {},
	
	        // Total entries
	        totalNumber: 0,
	
	        // Default page
	        pageNumber: 1,
	
	        // entries of per page
	        pageSize: 10,
	
	        // Page range (pages on both sides of the current page)
	        pageRange: 2,
	
	        // Whether to display the 'Previous' button
	        showPrevious: true,
	
	        // Whether to display the 'Next' button
	        showNext: true,
	
	        // Whether to display the page buttons
	        showPageNumbers: true,
	
	        showNavigator: false,
	
	        // Whether to display the 'Go' input
	        showGoInput: false,
	
	        // Whether to display the 'Go' button
	        showGoButton: false,
	
	        // Page link
	        pageLink: '',
	
	        // 'Previous' text
	        prevText: '&laquo;',
	
	        // 'Next' text
	        nextText: '&raquo;',
	
	        // Ellipsis text
	        ellipsisText: '...',
	
	        // 'Go' button text
	        goButtonText: 'Go',
	
	        // Additional className for Pagination element
	        //className: '',
	
	        classPrefix: 'paginationjs',
	
	        // Default active class
	        activeClassName: 'active',
	
	        // Default disable class
	        disableClassName: 'disabled',
	
	        //ulClassName: '',
	
	        // Whether to insert inline style
	        inlineStyle: true,
	
	        formatNavigator: '<%= currentPage %> / <%= totalPage %>',
	
	        formatGoInput: '<%= input %>',
	
	        formatGoButton: '<%= button %>',
	
	        // Pagination element's position in the container
	        position: 'bottom',
	
	        // Auto hide previous button when current page is the first page
	        autoHidePrevious: false,
	
	        // Auto hide next button when current page is the last page
	        autoHideNext: false,
	
	        //header: '',
	
	        //footer: '',
	
	        // Aliases for custom pagination parameters
	        //alias: {},
	
	        // Whether to trigger pagination at initialization
	        triggerPagingOnInit: true,
	
	        // Whether to hide pagination when less than one page
	        hideWhenLessThanOnePage: false,
	
	        showFirstOnEllipsisShow: true,
	
	        showLastOnEllipsisShow: true,
	
	        // Pagination callback
	        callback: function() {}
	    };
	
	    // Hook register
	    $.fn[pluginHookMethod] = function(hook, callback) {
	        if (arguments.length < 2) {
	            throwError('Missing argument.');
	        }
	
	        if (!$.isFunction(callback)) {
	            throwError('callback must be a function.');
	        }
	
	        var container = $(this);
	        var paginationData = container.data('pagination');
	
	        if (!paginationData) {
	            container.data('pagination', {});
	            paginationData = container.data('pagination');
	        }
	
	        !paginationData.hooks && (paginationData.hooks = {});
	
	        //paginationData.hooks[hook] = callback;
	        paginationData.hooks[hook] = paginationData.hooks[hook] || [];
	        paginationData.hooks[hook].push(callback);
	
	    };
	
	    // Static method
	    $[pluginName] = function(selector, options) {
	        if (arguments.length < 2) {
	            throwError('Requires two parameters.');
	        }
	
	        var container;
	
	        // 'selector' is a jQuery object
	        if (typeof selector !== 'string' && selector instanceof jQuery) {
	            container = selector;
	        } else {
	            container = $(selector);
	        }
	
	        if (!container.length) return;
	
	        container.pagination(options);
	
	        return container;
	    };
	
	    // ============================================================
	    // helpers
	    // ============================================================
	
	    var Helpers = {};
	
	    // Throw error
	    function throwError(content) {
	        throw new Error('Pagination: ' + content);
	    }
	
	    // Check parameters
	    function parameterChecker(args) {
	        if (!args.dataSource) {
	            throwError('"dataSource" is required.');
	        }
	
	        if (typeof args.dataSource === 'string') {
	            if (args.totalNumberLocator === undefined) {
	                if (args.totalNumber === undefined) {
	                    throwError('"totalNumber" is required.');
	                } else if (!$.isNumeric(args.totalNumber)) {
	                    throwError('"totalNumber" is incorrect. (Number)');
	                }
	            } else {
	                if (!$.isFunction(args.totalNumberLocator)) {
	                    throwError('"totalNumberLocator" should be a Function.');
	                }
	            }
	        } else if (Helpers.isObject(args.dataSource)) {
	            if (typeof args.locator === 'undefined') {
	                throwError('"dataSource" is an Object, please specify "locator".');
	            } else if (typeof args.locator !== 'string' && !$.isFunction(args.locator)) {
	                throwError('' + args.locator + ' is incorrect. (String | Function)');
	            }
	        }
	
	        if (args.formatResult !== undefined && !$.isFunction(args.formatResult)) {
	            throwError('"formatResult" should be a Function.');
	        }
	    }
	
	    // uninstall plugin
	    function uninstallPlugin(target) {
	        var events = ['go', 'previous', 'next', 'disable', 'enable', 'refresh', 'show', 'hide', 'destroy'];
	
	        // off events of old instance
	        $.each(events, function(index, value) {
	            target.off(eventPrefix + value);
	        });
	
	        // reset pagination data
	        target.data('pagination', {});
	
	        // remove old
	        $('.paginationjs', target).remove();
	    }
	
	    // Object type detection
	    function getObjectType(object, tmp) {
	        return ((tmp = typeof(object)) == "object" ? object == null && "null" || Object.prototype.toString.call(object).slice(8, -1) : tmp).toLowerCase();
	    }
	
	    $.each(['Object', 'Array', 'String'], function(index, name) {
	        Helpers['is' + name] = function(object) {
	            return getObjectType(object) === name.toLowerCase();
	        };
	    });
	
	    /*
	     * export via AMD or CommonJS
	     * */
	    if (true) {
	        !(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	            return $;
	        }.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	    }
	
	})(this, window.jQuery);


/***/ })
/******/ ]);